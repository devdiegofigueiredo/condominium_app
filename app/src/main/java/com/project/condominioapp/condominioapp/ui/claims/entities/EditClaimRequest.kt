package com.project.condominioapp.condominioapp.ui.claims.entities

data class EditClaimRequest(val claimId: String, val title: String, val description: String)