package com.project.condominioapp.condominioapp.utils.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.condominioapp.condominioapp.R
import kotlinx.android.synthetic.main.fragment_info.*
import kotlin.reflect.KFunction0

@SuppressLint("ValidFragment")
class InfoFragment(private val labelMessage: String,
                   private val textButton: String,
                   private val onButtonClicked: KFunction0<Unit>)
    : androidx.fragment.app.Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        message.text = labelMessage

        if (textButton.isNotEmpty()) {
            text_button.text = textButton
        }

        text_button.setOnClickListener {
            onButtonClicked()
            activity?.supportFragmentManager?.beginTransaction()?.remove(this@InfoFragment)?.commitAllowingStateLoss()
        }
    }
}