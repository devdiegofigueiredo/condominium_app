package com.project.condominioapp.condominioapp.ui.vacancies.entities

data class CreateVacancieResponse(val statusCode: Int, val vacancie: Vacancie)