package com.project.condominioapp.condominioapp.ui.vacancies

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

class VacanciesPresenter(val view: VacanciesContract.View,
                         val interactor: VacanciesContract.Interactor) :
        VacanciesContract.Presenter,
        VacanciesContract.Presenter.VacanciesCallback,
        VacanciesContract.Presenter.VacancieDeletedCallback,
        VacanciesContract.Presenter.VacancieEditedCallback,
        VacanciesContract.Presenter.UserCallback {

    init {
        view.showProgress()
        interactor.user(this)
    }

    override fun vacancies() {
        interactor.user(this)
        view.showLoading()
    }

    override fun onVancanciesSuccess(vacancies: List<Vacancie>, count: Int) {
        view.addVacancies(vacancies, count)
        view.hideLoading()
        view.hideProgress()
    }

    override fun onVancanciesEmpty(message: String, textButton: String) {
        view.showEmptyView(message, textButton)
        view.hideLoading()
        view.hideProgress()
    }

    override fun onVacanciesError(message: String, textButton: String) {
        view.showErrorView(message, textButton)
        view.hideLoading()
        view.hideProgress()
    }

    override fun deleteVacancie(vacancieId: String) {
        view.showLoading()
        view.hideDeleteVacancieDialog()
        interactor.deleteVacancie(vacancieId, this)
    }

    override fun onVacancieDeletedSuccess(message: String, vacancieId: String) {
        view.hideLoading()
        view.removeVacancieFromList(vacancieId)
        view.showMessage(message)
    }

    override fun onVacancieDeletedError(message: String) {
        view.hideLoading()
        view.showMessage(message)
    }

    override fun editVacancie(vacancieId: String,
                              model: EditText,
                              plate: EditText,
                              vacancieNumber: EditText,
                              apartmentNumber: EditText,
                              apartmentBlock: EditText) {
        if (isFieldsValidateds(model, plate, vacancieNumber, apartmentNumber, apartmentBlock)) {
            view.showLoading()
            interactor.editVacancie(vacancieId,
                    model.text.toString(),
                    plate.text.toString(),
                    vacancieNumber.text.toString(),
                    apartmentNumber.text.toString(),
                    apartmentBlock.text.toString(),
                    this)
        }
    }

    override fun onVacancieEditedSuccess(message: String, vacancie: Vacancie) {
        view.hideLoading()
        view.showMessage(message)
        view.updateVacancie(vacancie)
    }

    override fun onVacancieEditedError(message: String, textButton: String) {
        view.hideLoading()
        view.showMessage(message)
    }

    override fun onUserSuccess(currentUserId: String) {
        view.setupVacancies(currentUserId)
        interactor.vacancies(this)
    }

    private fun isFieldsValidateds(model: EditText,
                                   plate: EditText,
                                   vacancieNumber: EditText,
                                   apartmentNumber: EditText,
                                   apartmentBlock: EditText): Boolean {

        model.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(model)
            return false
        }

        plate.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(plate)
            return false
        }

        vacancieNumber.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(vacancieNumber)
            return false
        }

        apartmentNumber.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(apartmentNumber)
            return false
        }

        apartmentBlock.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(apartmentBlock)
            return false
        }

        return true
    }
}