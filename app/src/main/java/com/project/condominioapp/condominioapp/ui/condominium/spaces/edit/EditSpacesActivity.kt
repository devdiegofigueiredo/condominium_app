package com.project.condominioapp.condominioapp.ui.condominium.spaces.edit

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.EditText
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.condominium.entities.Space
import com.project.condominioapp.condominioapp.ui.condominium.spaces.SpacesActivity
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.formatValue
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_create_space.*
import kotlinx.android.synthetic.main.done.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class EditSpaceActivity : BaseActivity(), EditSpaceContract.View {

    var space: Space? = null
    var condominiumId: String = ""

    companion object {
        const val EXTRA_SPACE = "extra_space"
        const val EXTRA_CONDOMINIUM_ID = "extra_condominium_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_space)
        setupToolbar(getString(R.string.edit_space), true)

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_update_space), this)

        intent?.extras?.getParcelable<Space>(EXTRA_SPACE)?.apply { space = this }
        intent?.extras?.getString(EXTRA_CONDOMINIUM_ID)?.apply { condominiumId = this }

        name.setText(space?.name)
        description.setText(space?.description)
        value.setText(space?.value?.formatValue())

        val interactor = EditSpaceInteractor(this)
        val presenter = EditSpacePresenter(this, interactor)

        done.setOnClickListener {
            space?.let { presenter.editSpace(name, description, value, condominiumId, it._id) }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun finishView(space: Space): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { _: DialogInterface, _: Int ->
            val intent = Intent()
            intent.putExtra(SpacesActivity.RESULT_SPACE, space)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun showErrorField(field: EditText) {
        field.requestFocus()
        field.error = getString(R.string.input_this_field)
    }
}