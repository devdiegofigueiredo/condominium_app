package com.project.condominioapp.condominioapp.ui.claims.entities

data class DeleteClaimResponse(val statusCode: Int)