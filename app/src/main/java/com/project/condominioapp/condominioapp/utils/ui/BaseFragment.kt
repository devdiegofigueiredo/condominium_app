package com.project.condominioapp.condominioapp.utils.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.project.condominioapp.condominioapp.R
import kotlinx.android.synthetic.main.done.*
import kotlin.reflect.KFunction0

@SuppressLint("Registered")
open class BaseFragment : androidx.fragment.app.Fragment() {

    lateinit var loading: AlertDialog

    fun showLoading() {
        loading.takeIf { !it.isShowing }.apply { loading.show() }
    }

    fun hideLoading() {
        loading.takeIf { it.isShowing }.apply { loading.hide() }
    }

    fun showDoneLoading() {
        progress.visibility = View.VISIBLE
        done_label.visibility = View.GONE
    }

    fun hideDoneLoading() {
        progress.visibility = View.GONE
        done_label.visibility = View.VISIBLE
    }

    fun showErrorMessage(title: String) {
        Toast.makeText(activity, title, Toast.LENGTH_LONG).show()
    }

    fun showErrorView(message: String, onInfoButtonClicked: KFunction0<Unit>) {
        val errorFragment = InfoFragment(message, "", onInfoButtonClicked)
        childFragmentManager.beginTransaction().replace(R.id.error_container, errorFragment).commitAllowingStateLoss()
    }

    fun showSimpleDialog(message: String) {
        showSimpleDialog(message, null)
    }

    fun showSimpleDialog(message: String, listener: DialogInterface.OnClickListener?) {
        activity?.apply {
            val dialog = AlertDialog.Builder(this, R.style.AlertDialogTheme)
            dialog.setTitle(getString(R.string.attention))
            dialog.setMessage(message)

            dialog.setNegativeButton(getString(R.string.ok), listener)

            val alertDialog = dialog.create()
            alertDialog.setCancelable(true)
            alertDialog.show()
        }
    }

    fun hideKeyboard() {
        activity?.apply {
            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }
    }
}