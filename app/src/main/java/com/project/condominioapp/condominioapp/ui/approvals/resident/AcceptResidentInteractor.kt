package com.project.condominioapp.condominioapp.ui.approvals.resident

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.approvals.entities.AcceptResidentRequest
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class AcceptResidentInteractor(val context: Context) : AcceptResidentContract.Interactor {

    private lateinit var currentUser: User
    private val compositeDisposable = CompositeDisposable()

    override fun pendencyResidents(callback: AcceptResidentContract.Presenter.ResidentsCallback) {
        val disposable = Single.fromCallable { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                        getPendencyResidents(callback)
                    }
                }, {
                })

        compositeDisposable.add(disposable)
    }

    private fun getPendencyResidents(callback: AcceptResidentContract.Presenter.ResidentsCallback) {
        val request = BaseService.pendecyResidents(currentUser.condominiumId)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            this.user?.takeIf { it.isNotEmpty() }?.apply {
                                callback.onResidentsSuccess(this)
                            } ?: kotlin.run {
                                callback.onResidentsError(context.getString(R.string.anyone_resident_pendency))
                            }
                        } else {
                            callback.onResidentsError(context.getString(R.string.error_pendency_residents))
                        }
                    }
                }, {
                    it.message?.apply { callback.onResidentsError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun acceptResident(userId: String, callback: AcceptResidentContract.Presenter.AcceptCallback) {
        val acceptResidentRequest = AcceptResidentRequest(userId, currentUser.condominiumId)
        val request = BaseService.acceptResident(acceptResidentRequest)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        response.takeIf { it.statusCode == 200 }?.apply {
                            callback.onAcceptSuccess(context.getString(R.string.user_accepted_success), userId)
                        } ?: kotlin.run {
                            callback.onAcceptError(context.getString(R.string.error_accept_user))
                        }
                    }
                }, {
                    it.message?.apply { callback.onAcceptError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun deleteUser(userId: String, firebaseId: String, callback: AcceptResidentContract.Presenter.DeleteCallback) {
        val request = BaseService.deleteResident(userId, currentUser.condominiumId, firebaseId)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        takeIf { this.statusCode == 200 }?.apply {
                            callback.onDeleteSuccess(context.getString(R.string.user_deleted_sucess), userId)
                        } ?: kotlin.run {
                            callback.onDeleteError(context.getString(R.string.error_delete_user))

                        }
                    } ?: kotlin.run {
                        callback.onDeleteError(context.getString(R.string.error_delete_user))
                    }
                }, {
                    it.message?.apply { callback.onDeleteError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}