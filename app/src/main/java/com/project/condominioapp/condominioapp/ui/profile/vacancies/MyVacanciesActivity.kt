package com.project.condominioapp.condominioapp.ui.profile.vacancies

import android.content.Intent
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.profile.vacancies.editable.EditableVacanciesActivity
import kotlinx.android.synthetic.main.activity_my_vacancies.*
import kotlinx.android.synthetic.main.toolbar.*

class MyVacanciesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_vacancies)

        setupToolbar()
        setupVacancies()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_edit, menu)
        menuInflater.inflate(R.menu.menu_add, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }

            R.id.action_edit -> {
                startActivity(Intent(baseContext, EditableVacanciesActivity::class.java))
            }

            R.id.action_add -> {
                startAddVacancieDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupVacancies() {
        vacancies_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
//        vacancies_list.adapter = VacanciesAdapter(null)
    }

    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.my_vacancies)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun startAddVacancieDialog() {
        val contextWrapper = ContextThemeWrapper(this, R.style.AlertDialogTheme)
        val dialog = AlertDialog.Builder(contextWrapper)
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.add_vacancie_dialog, null, false)
        val cardView = view.findViewById<androidx.cardview.widget.CardView>(R.id.main_container)
        cardView.cardElevation = 0f

        val addVacancieDialog = dialog.create()
        addVacancieDialog.setView(view)
        addVacancieDialog.setCancelable(true)
        addVacancieDialog.show()

        val done = view.findViewById<TextView>(R.id.done)
        done.setOnClickListener { addVacancieDialog.dismiss() }
    }
}