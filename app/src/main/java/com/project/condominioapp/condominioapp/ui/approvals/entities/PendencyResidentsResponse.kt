package com.project.condominioapp.condominioapp.ui.approvals.entities

import com.project.condominioapp.condominioapp.model.User

data class PendencyResidentsResponse(val statusCode: Int,
                                     val user: List<User>?)