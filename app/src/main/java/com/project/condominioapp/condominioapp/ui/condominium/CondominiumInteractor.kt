package com.project.condominioapp.condominioapp.ui.condominium

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.repository.CurrentUser
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.condominium.CondominiumContract.Presenter.CondominiumCallback
import com.project.condominioapp.condominioapp.ui.condominium.util.CondominiumSession
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CondominiumInteractor(val context: Context) : CondominiumContract.Interactor {

    private lateinit var user: User
    private val compositeDisposable = CompositeDisposable()

    override fun condominium(callback: CondominiumCallback) {
        CurrentUser.user(onUserSuccess(callback), context)
    }

    fun onUserSuccess(callback: CondominiumCallback) = object : CurrentUser.UserCallback {
        override fun onUserSuccess(user: User) {
            this@CondominiumInteractor.user = user
            requestCondominium(callback)
        }
    }

    private fun requestCondominium(callback: CondominiumCallback) {
        val callCreateUser = BaseService.condominium(user.condominiumId)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onCondominiumSuccess(this.condominium)
                            CondominiumSession.condominium(this.condominium)
                        } else {
                            callback.onCondominiumError(context.getString(R.string.error_get_condominium))
                        }
                    }
                }, {
                    it.message?.apply { callback.onCondominiumError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}