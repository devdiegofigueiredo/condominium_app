package com.project.condominioapp.condominioapp.ui.condominium.spaces.edit

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.ui.condominium.entities.EditSpaceRequest
import com.project.condominioapp.condominioapp.ui.condominium.entities.Space
import com.project.condominioapp.condominioapp.ui.condominium.util.CondominiumSession
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class EditSpaceInteractor(val context: Context) : EditSpaceContract.Interactor {

    private val disposables = CompositeDisposable()

    override fun editSpace(name: String,
                           description: String,
                           value: Double,
                           condominiumId: String,
                           spaceId: String,
                           callback: EditSpaceContract.Presenter.EditSpaceCallback) {
        val editSpaceRequest = EditSpaceRequest(condominiumId, name, description, value, spaceId)
        val request = BaseService.editSpace(editSpaceRequest)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    takeIf { response.statusCode == 200 }?.apply {
                        updateCondominiumSession(response.space)
                        callback.onEditSuccess(context.getString(R.string.space_edit_sucess), response.space)
                    } ?: kotlin.run {
                        callback.onEditError(context.getString(R.string.error_edit_space))
                    }
                }, {
                    it.message?.apply { callback.onEditError(this) }
                })

        disposables.add(disposable)
    }

    private fun updateCondominiumSession(spaceUpdated: Space) {
        CondominiumSession.updateSpace(spaceUpdated)
    }

    override fun onDestroy() {
        disposables.dispose()
    }
}