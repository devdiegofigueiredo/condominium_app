package com.project.condominioapp.condominioapp.ui.vacancies.search

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.vacancies.VacanciesActivity
import com.project.condominioapp.condominioapp.ui.vacancies.edit.EditVacanciesActivity
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_search_vacancies.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class SearchVacanciesActivity : BaseActivity(), SearchVacanciesContract.View {

    private val EDIT_VACANCIE_REQUEST_CODE = 1001

    private lateinit var adapter: SearchVacanciesAdapter
    private lateinit var presenter: SearchVacanciesPresenter
    private lateinit var deleteVacancieDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_vacancies)

        setupToolbar(getString(R.string.search_vacancie), true)

        loading = ProgressDialogUtil.getDialog(getString(R.string.search_vacancies), this)

        val interactor = SearchVacanciesInteractor(this)
        presenter = SearchVacanciesPresenter(this, interactor)

        search.setOnClickListener {
            presenter.vacancies(vacancie_label.text.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        showKeyboard()
    }

    override fun onStop() {
        hideKeyboard()
        super.onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        takeIf { resultCode == RESULT_OK }?.apply {
            when (requestCode) {
                EDIT_VACANCIE_REQUEST_CODE -> {
                    val jsonClaim = data?.extras?.getString(VacanciesActivity.EXTRA_VACANCIE)
                    val gson = Gson()
                    val vacancie = gson.fromJson(jsonClaim, Vacancie::class.java)
                    data?.extras?.getString(VacanciesActivity.EXTRA_USER_ID)?.apply { adapter.updateVacancie(vacancie) }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.takeIf { it.itemId == android.R.id.home }?.apply {
            finish()
            hideKeyboard()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setupVacancies(vacancies: List<Vacancie>) {
        adapter.addVacancies(vacancies)

        information_label.visibility.takeIf { it == View.VISIBLE }.apply { information_label.visibility = View.GONE }
        vacancies_list.visibility.takeIf { it == View.GONE }.apply { vacancies_list.visibility = View.VISIBLE }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun showErrorView(message: String) {
        information_label.text = message
        information_label.visibility.takeIf { it == View.GONE }.apply { information_label.visibility = View.VISIBLE }

        vacancies_list.visibility.takeIf { it == View.VISIBLE }.apply { vacancies_list.visibility = View.GONE }
    }

    override fun closeKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun setupVacanciesList(currentUserId: String) {
        adapter = SearchVacanciesAdapter(this, currentUserId, this::onDeleteClicked, this::onEditClicked)
        vacancies_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        vacancies_list.adapter = adapter
    }

    override fun removeVacancieFromList(vacancieId: String) {
        adapter.removeVacancieFromList(vacancieId)
    }

    private fun onDeleteClicked(vacancieId: String) {
        showDeleteVacancieDialog(vacancieId)
    }

    private fun onEditClicked(vacancieId: String,
                              model: String,
                              plate: String,
                              vacancieNumber: String,
                              apartmentNumber: String,
                              apartmentBlock: String) {
        closeKeyboard()

        val intent = Intent(this, EditVacanciesActivity::class.java)
        intent.putExtra(EditVacanciesActivity.EXTRA_VACANCIE_ID, vacancieId)
        intent.putExtra(EditVacanciesActivity.EXTRA_MODEL, model)
        intent.putExtra(EditVacanciesActivity.EXTRA_PLATE, plate)
        intent.putExtra(EditVacanciesActivity.EXTRA_VACANCIE_NUMBER, vacancieNumber)
        intent.putExtra(EditVacanciesActivity.EXTRA_APARTMENT_NUMBER, apartmentNumber)
        intent.putExtra(EditVacanciesActivity.EXTRA_APARTMENT_BLOCK, apartmentBlock)
        startActivityForResult(intent, EDIT_VACANCIE_REQUEST_CODE)
    }

    private fun showDeleteVacancieDialog(vacancieId: String) {
        closeKeyboard()

        val contextWrapper = ContextThemeWrapper(this, R.style.AlertDialogTheme)
        val dialog = AlertDialog.Builder(contextWrapper)
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.delete_dialog, null, false)

        deleteVacancieDialog = dialog.create()
        deleteVacancieDialog.setView(view)
        deleteVacancieDialog.setCancelable(true)
        deleteVacancieDialog.show()

        val yes = view.findViewById<TextView>(R.id.yes)
        yes.setOnClickListener {
            deleteVacancieDialog.dismiss()
            presenter.deleteVacancie(vacancieId)
            deleteVacancieDialog.dismiss()
        }

        val no = view.findViewById<TextView>(R.id.no)
        no.setOnClickListener { deleteVacancieDialog.dismiss() }
    }
}