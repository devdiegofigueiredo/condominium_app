package com.project.condominioapp.condominioapp.ui.approvals.entities

data class ReplyClaimRequest(val claimId: String, val reply: String)