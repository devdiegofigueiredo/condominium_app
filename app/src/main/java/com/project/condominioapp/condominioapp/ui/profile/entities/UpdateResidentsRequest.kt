package com.project.condominioapp.condominioapp.ui.profile.entities

data class UpdateResidentsRequest(val residents: List<Resident>,
                                  val userId: String,
                                  val condominiumId: String)
