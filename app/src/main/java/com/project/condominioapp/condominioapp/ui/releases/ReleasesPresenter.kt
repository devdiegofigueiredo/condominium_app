package com.project.condominioapp.condominioapp.ui.releases

import com.project.condominioapp.condominioapp.ui.releases.entities.Release

class ReleasesPresenter(val view: ReleasesContract.View,
                        val interactor: ReleasesContract.Interactor) :
        ReleasesContract.Presenter,
        ReleasesContract.Presenter.ReleasesCallback {

    override fun releases() {
        view.showLoading()
        interactor.releases(this)
    }

    override fun onReleasesSuccess(releases: List<Release>) {
        view.hideLoading()
        view.setupReleases(releases)
    }

    override fun onReleasesError(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}