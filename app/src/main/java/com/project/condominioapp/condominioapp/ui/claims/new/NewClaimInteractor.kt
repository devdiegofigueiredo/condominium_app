package com.project.condominioapp.condominioapp.ui.claims.new

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.claims.entities.CreateClaimRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NewClaimInteractor(val context: Context) : NewClaimContract.Interactor {

    private lateinit var currentUser: User
    private val compositeDisposable = CompositeDisposable()

    init {
        getUserInformation()
    }

    override fun createClaim(title: String, description: String, callback: NewClaimContract.Presenter.ClaimCallback) {
        val createClaimRequest = CreateClaimRequest(title, description, currentUser.id, currentUser.condominiumId)
        val callCreateUser = BaseService.createClaim(createClaimRequest)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onCreateClaimSuccess(this.claim, context.getString(R.string.claim_created))
                        } else {
                            callback.onCreateClaimError(context.getString(R.string.error_create_claim))
                        }
                    }
                }, {
                    it.message?.apply { callback.onCreateClaimError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun getUserInformation() {
        val disposable = Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                    }
                }, {
                })

        compositeDisposable.add(disposable)
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}