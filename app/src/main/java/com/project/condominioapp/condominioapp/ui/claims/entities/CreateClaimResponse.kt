package com.project.condominioapp.condominioapp.ui.claims.entities

data class CreateClaimResponse(val statusCode: Int, val claim: Claim)