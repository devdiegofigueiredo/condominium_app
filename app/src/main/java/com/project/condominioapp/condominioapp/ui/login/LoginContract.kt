package com.project.condominioapp.condominioapp.ui.login

interface LoginContract {

    interface View {
        fun startHomeScreen()
        fun showLoading()
        fun hideLoading()
        fun showErrorMessage(message: String)
        fun showPendingAcceptDialog()
    }

    interface Presenter {
        interface LoginCallback {
            fun onLoginSuccess()
            fun onLoginPending()
            fun onLoginError(message: String)
        }

        fun login(email: String, password: String, condominiumId: String)
    }

    interface Interactor {
        fun clearDatabase()
        fun onDestroy()
        fun doFirebaseLogin(email: String, password: String, condominiumId: String, callback: Presenter.LoginCallback)
    }
}