package com.project.condominioapp.condominioapp.ui.condominium.spaces.edit

import android.content.DialogInterface
import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.condominium.entities.Space
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface EditSpaceContract {


    interface View : BaseView {
        fun finishView(space: Space): DialogInterface.OnClickListener
        fun showErrorField(field: EditText)
    }

    interface Presenter {
        interface EditSpaceCallback {
            fun onEditSuccess(message: String, space: Space)
            fun onEditError(message: String)
        }

        fun editSpace(name: EditText, description: EditText, value: EditText, condominiumId: String, spaceId: String)
        fun onDestroy()
    }

    interface Interactor {
        fun editSpace(name: String, description: String, value: Double, condominiumId: String, spaceId: String, callback: Presenter.EditSpaceCallback)
        fun onDestroy()
    }
}