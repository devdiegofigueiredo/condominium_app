package com.project.condominioapp.condominioapp.ui.condominium.entities

data class EditSpaceResponse(val statusCode: Int, val space: Space)