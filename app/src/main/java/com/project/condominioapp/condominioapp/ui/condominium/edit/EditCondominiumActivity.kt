package com.project.condominioapp.condominioapp.ui.condominium.edit

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.EditText
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.condominium.entities.Condominium
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.TextFormatUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_edit_condominium.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class EditCondominiumActivity : BaseActivity(), EditCondominiumContract.View {

    companion object {
        const val EXTRA_CONDOMINIUM = "extra_condominium"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_condominium)

        setupToolbar(getString(R.string.edit_condominium), true)

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_updating_condominium), this)


        val condominium = intent?.extras?.getParcelable<Condominium>(EXTRA_CONDOMINIUM)
        name.setText(condominium?.name)

        condominium?.phone?.takeIf { it.length > 10 }?.apply {
            phone.setText(TextFormatUtil.addTextViewMask(this, "(##)#####-####"))
        } ?: kotlin.run {
            phone.setText(condominium?.phone?.let { TextFormatUtil.addTextViewMask(it, "(##)####-####") })
        }
        phone.addTextChangedListener(TextFormatUtil.phoneMask(phone))

        street.setText(condominium?.street)
        city.setText(condominium?.city)
        state.setText(condominium?.state)

        postal_code.setText(condominium?.postalCode?.let { TextFormatUtil.addTextViewMask(it, "#####-###") })
        postal_code.addTextChangedListener(TextFormatUtil.addMask("#####-###", postal_code))

        val interactor = EditCondominiumInteractor(this)
        val presenter = EditCondominiumPresenter(this, interactor)
        done.setOnClickListener {
            condominium?._id?.apply {
                presenter.editCondominium(name, phone, street, city, state, postal_code, this)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun finishView(condominium: Condominium): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { _: DialogInterface, _: Int ->
            val intent = Intent()
            intent.putExtra(EXTRA_CONDOMINIUM, condominium)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun showErrorField(field: EditText) {
        field.requestFocus()
        field.error = getString(R.string.input_this_field)
    }
}