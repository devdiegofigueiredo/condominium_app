package com.project.condominioapp.condominioapp.ui.claims.entities

data class EditClaimResponse(val statusCode: Int, val claim: Claim)