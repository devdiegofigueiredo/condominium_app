package com.project.condominioapp.condominioapp.model

data class SimpleResponse(val statusCode: Int, val message: String)