package com.project.condominioapp.condominioapp.ui.releases

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ReleasesInteractor(val context: Context) : ReleasesContract.Interactor {

    private lateinit var currentUser: User
    private val disposables = CompositeDisposable()

    override fun releases(callback: ReleasesContract.Presenter.ReleasesCallback) {
        val disposable = Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                        getReleases(callback)
                    }
                }, {
                })

        disposables.add(disposable)
    }

    override fun onDestroy() {
        disposables.dispose()
    }

    private fun getReleases(callback: ReleasesContract.Presenter.ReleasesCallback) {
        val callCreateUser = BaseService.getReleases(currentUser.condominiumId, currentUser.id)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onReleasesSuccess(this.releases)
                        } else {
                            callback.onReleasesError(context.getString(R.string.error_get_vacancies))
                        }
                    }
                }, {
                    it.message?.apply { callback.onReleasesError(this) }
                })

        disposables.add(disposable)
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}