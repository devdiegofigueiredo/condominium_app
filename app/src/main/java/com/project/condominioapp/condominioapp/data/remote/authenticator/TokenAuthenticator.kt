package com.project.condominioapp.condominioapp.data.remote.authenticator

import okhttp3.Interceptor
import okhttp3.Response

class TokenAuthenticator : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val builder = chain.request().newBuilder()

//        FirebaseAuth.getInstance().currentUser?.getIdToken(true)?.addOnCompleteListener({
//            if (it.isSuccessful) {
//                builder.addHeader("idToken", it.result?.token)
//            }
//        })

        builder.addHeader("idToken", "idToken")

        return chain.proceed(builder.build())
    }
}