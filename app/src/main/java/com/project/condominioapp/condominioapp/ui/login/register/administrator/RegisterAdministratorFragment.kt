package com.project.condominioapp.condominioapp.ui.login.register.administrator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.login.register.RegisterActivity
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.TextFormatUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_register_administrator.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class RegisterAdministratorFragment : BaseFragment(), RegisterAdministratorContract.View {

    private lateinit var presenter: RegisterAdministratorPresenter

    companion object {

        @JvmStatic
        fun newInstance(): RegisterAdministratorFragment {
            return RegisterAdministratorFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register_administrator, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.apply {
            val interactor = RegisterAdministratorInteractor(this)
            presenter = RegisterAdministratorPresenter(this@RegisterAdministratorFragment, interactor)

            loading = ProgressDialogUtil.getDialog("Aguarde, estamos criando seu condomínio!", this)

        }

        postal_code.addTextChangedListener(TextFormatUtil.addMask("#####-###", postal_code))
        phone.addTextChangedListener(TextFormatUtil.phoneMask(phone))

        done.setOnClickListener { presenter.createCondominium(condominium_name, email, phone, address, state, city, postal_code) }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun showCreateCondominiumSuccessMessage() {
        showErrorMessage(getString(R.string.condominium_created))
    }

    override fun requestFocusField(editText: EditText) {
        editText.requestFocus()
    }

    override fun showInvalidEmail() {
        email.error = getString(R.string.input_valid_email)
    }

    override fun showInvalidPhone() {
        phone.error = getString(R.string.input_valid_phone)
    }

    override fun showInvalidCondominiumName() {
        condominium_name.error = getString(R.string.input_valid_condominium_name)
    }

    override fun showInvalidAddress() {
        address.error = getString(R.string.input_valid_address)
    }

    override fun showInvalidState() {
        state.error = getString(R.string.input_valid_state)
    }

    override fun showInvalidCity() {
        city.error = getString(R.string.input_valid_city)
    }

    override fun showInvalidPostalCode() {
        postal_code.error = getString(R.string.input_valid_postal_code)
    }

    override fun closeScreen() {
        (activity as RegisterActivity).finish()
    }
}