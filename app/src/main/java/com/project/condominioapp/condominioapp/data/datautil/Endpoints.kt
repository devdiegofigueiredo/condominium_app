package com.project.condominioapp.condominioapp.data.datautil

class Endpoints {

    companion object {
        const val createUser = "user/create"
        const val getCurrentUser = "user/userInfo"
        const val createClaim = "claim/create"
        const val getClaims = "claim/byId"
        const val createVacancie = "vacancie/create"
        const val getVacancies = "vacancie/"
        const val editVacancie = "vacancie/updateById"
        const val deleteVacancie = "vacancie/deleteById"
        const val deleteClaim = "claim/deleteById"
        const val updateClaim = "claim/updateById"
        const val updateResidents = "user/updateResidentsById"
        const val updateFirebaseToken = "user/updateFirebaseToken"
        const val updateUser = "user/updateUser"
        const val searchVacancies = "vacancie/search"
        const val searchReleases = "release/"
        const val createCondominium = "condominium/create"
        const val pendencyUsers = "user/pendencyUsers"
        const val claimsUnreplieds = "claim/byClaimsUnreplied"
        const val addClaimReply = "claim/addClaimReply"
        const val addSolvedClaimReply = "claim/addSolvedClaimReply"
        const val acceptResident = "user/acceptResident"
        const val deleteResident = "user/deleteUser"
        const val condominium = "condominium/byCondominiumId"
        const val updateCondominium = "condominium/updateCondominium"
        const val createSpace = "condominium/addSpace"
        const val deleteSpace = "condominium/removeSpace"
        const val editSpace = "condominium/updateSpace"
    }
}