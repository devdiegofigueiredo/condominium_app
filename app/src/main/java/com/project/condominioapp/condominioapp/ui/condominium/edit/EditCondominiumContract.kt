package com.project.condominioapp.condominioapp.ui.condominium.edit

import android.content.DialogInterface
import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.condominium.entities.Condominium
import com.project.condominioapp.condominioapp.ui.condominium.entities.EditCondominiumRequest
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface EditCondominiumContract {

    interface View : BaseView {
        fun finishView(condominium: Condominium): DialogInterface.OnClickListener
        fun showErrorField(field: EditText)
    }

    interface Presenter {
        interface EditCondominiumCallback {
            fun onEditCondominiumSuccess(message: String, condominium: Condominium)
            fun onEditCondominiumError(message: String)
        }

        fun editCondominium(name: EditText,
                            phone: EditText,
                            street: EditText,
                            city: EditText,
                            state: EditText,
                            postal_code: EditText,
                            condominiumId: String)

        fun onDestroy()
    }

    interface Interactor {
        fun editCondominium(editCondominium: EditCondominiumRequest, callback: Presenter.EditCondominiumCallback)
        fun onDestroy()
    }

}