package com.project.condominioapp.condominioapp.ui.approvals.resident

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.model.User
import kotlin.reflect.KFunction1
import kotlin.reflect.KFunction2

class AcceptResidentAdapter(val acceptResident: KFunction1<String, Unit>,
                            val deleteResident: KFunction2<String, String, Unit>,
                            val context: Context) :
        androidx.recyclerview.widget.RecyclerView.Adapter<AcceptResidentAdapter.ViewHolder>() {

    private var residents = arrayListOf<User>()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.accept_resident_item, parent, false))

    override fun getItemCount(): Int = residents.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val apartmentText = context.getString(R.string.apartment) +
                ": " +
                residents[position].apartment +
                " - " +
                context.getString(R.string.block) +
                ": " +
                residents[position].block.toUpperCase()

        holder.apartment.text = apartmentText
        holder.name.text = residents[position].name
        holder.email.text = residents[position].email

        holder.yes.setOnClickListener { acceptResident(residents[position].id) }
        holder.no.setOnClickListener { deleteResident(residents[position].id, residents[position].firebaseId) }
    }

    fun addResidents(residents: List<User>) {
        this.residents.addAll(residents)
        notifyDataSetChanged()
    }

    fun removeResident(residentId: String) {
        val it = residents.iterator()
        while (it.hasNext()) {
            val claim = it.next()
            claim.takeIf { it.id == residentId }?.apply {
                it.remove()
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val email: TextView = itemView.findViewById(R.id.email)
        val apartment: TextView = itemView.findViewById(R.id.apartment)
        val yes: TextView = itemView.findViewById(R.id.yes)
        val no: TextView = itemView.findViewById(R.id.no)
    }
}