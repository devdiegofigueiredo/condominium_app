package com.project.condominioapp.condominioapp.ui.approvals.entities

class AcceptResidentRequest(val userId: String, val condominiumId: String)