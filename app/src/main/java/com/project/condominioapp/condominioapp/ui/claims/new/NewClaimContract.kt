package com.project.condominioapp.condominioapp.ui.claims.new

import com.project.condominioapp.condominioapp.ui.claims.entities.Claim

interface NewClaimContract {

    interface View {
        fun showIncompleteFieldError()
        fun showMessage(message: String)
        fun closeScreen(claim: Claim)
        fun showLoading()
        fun hideLoading()
    }

    interface Presenter {
        interface ClaimCallback {
            fun onCreateClaimSuccess(claim: Claim, message: String)
            fun onCreateClaimError(message: String)
        }

        fun createClaim(title: String, description: String)
        fun onDestroy()
    }

    interface Interactor {
        fun createClaim(title: String, description: String, callback: Presenter.ClaimCallback)
        fun onDestroy()
    }
}