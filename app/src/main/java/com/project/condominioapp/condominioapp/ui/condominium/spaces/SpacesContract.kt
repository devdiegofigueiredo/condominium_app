package com.project.condominioapp.condominioapp.ui.condominium.spaces

import android.content.DialogInterface
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface SpacesContract {

    interface View : BaseView {
        fun removeSpace(spaceId: String): DialogInterface.OnClickListener
    }

    interface Presenter {
        interface DeleteSpaceCallback {
            fun onDeleteSuccess(message: String, spaceId: String)
            fun onDeleteError(message: String)
        }

        fun deleteSpace(spaceId: String, condominiumId: String)
        fun onDestroy()
    }

    interface Interactor {
        fun deleteSpace(spaceId: String, condominiumId: String, callback: Presenter.DeleteSpaceCallback)
        fun onDestroy()
    }

}