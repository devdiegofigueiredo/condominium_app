package com.project.condominioapp.condominioapp.ui.vacancies

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.vacancies.entities.EditVacancieRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class VacanciesInteractor(val context: Context) : VacanciesContract.Interactor {

    private lateinit var currentUser: User
    private val disposables = CompositeDisposable()
    private var currentPage = 0

    override fun vacancies(callback: VacanciesContract.Presenter.VacanciesCallback) {
        val callCreateUser = BaseService.getVacancies(currentUser.condominiumId)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            this.vacancies?.takeIf { it.isNotEmpty() }?.apply {
                                currentPage++
                                callback.onVancanciesSuccess(this, response.count)
                            } ?: kotlin.run {
                                callback.onVancanciesEmpty(context.getString(R.string.empty_vacancies), context.getString(R.string.create_now))
                            }
                        } else {
                            callback.onVacanciesError(context.getString(R.string.error_get_vacancies), context.getString(R.string.try_again))
                        }
                    }
                }, {
                    it.message?.apply { callback.onVacanciesError(this, context.getString(R.string.try_again)) }
                })

        disposables.add(disposable)
    }

    override fun deleteVacancie(vacancieId: String, callback: VacanciesContract.Presenter.VacancieDeletedCallback) {
        val request = BaseService.deleteVacancie(vacancieId)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onVacancieDeletedSuccess(context.getString(R.string.vacancie_deleted), vacancieId)
                        } else {
                            callback.onVacancieDeletedError("An error was occurred to delete this vacancie. Try again")
                        }
                    }
                }, {
                    it.message?.apply { callback.onVacancieDeletedError(this) }
                })

        disposables.add(disposable)
    }

    override fun editVacancie(vacancieId: String,
                              model: String,
                              plate: String,
                              vacancieNumber: String,
                              apartmentNumber: String,
                              apartmentBlock: String,
                              callback: VacanciesContract.Presenter.VacancieEditedCallback) {
        val editVacancieRequest = EditVacancieRequest(vacancieId, model, plate, vacancieNumber)
        val request = BaseService.editVacancie(editVacancieRequest)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onVacancieEditedSuccess(context.getString(R.string.vacancie_edited), this.vacancie)
                        } else {
                            callback.onVacancieEditedError(context.getString(R.string.error_edit_vacancie), context.getString(R.string.try_again))
                        }
                    }
                }, {
                    it.message?.apply { callback.onVacancieEditedError(this, context.getString(R.string.try_again)) }
                })

        disposables.add(disposable)
    }

    override fun user(callback: VacanciesContract.Presenter.UserCallback) {
        getUserInformation(callback)
    }

    override fun onDestroy() {
        disposables.dispose()
    }

    private fun getUserInformation(callback: VacanciesContract.Presenter.UserCallback) {
        val disposable = Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                        callback.onUserSuccess(this.id)
                    }
                }, {

                })

        disposables.add(disposable)
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}