package com.project.condominioapp.condominioapp.ui.condominium.util

import com.project.condominioapp.condominioapp.ui.condominium.entities.Condominium
import com.project.condominioapp.condominioapp.ui.condominium.entities.Space

class CondominiumSession {

    companion object {

        private lateinit var condominium: Condominium

        fun condominium(): Condominium {
            return condominium
        }

        fun condominium(condominium: Condominium) {
            this.condominium = condominium
        }

        fun updateSpace(spaceUpdated: Space) {
            condominium.spaces?.forEachIndexed { index, space ->
                space._id.takeIf { it == spaceUpdated._id }?.apply {
                    condominium.spaces?.apply {
                        this[index] = spaceUpdated
                    }
                    return
                }
            }
        }

        fun addSpace(space: Space) {
            condominium.spaces?.apply { this.toMutableList().add(space) } ?: kotlin.run {
                val spaces = arrayListOf<Space>()
                spaces.add(space)
                condominium.spaces = spaces
            }
        }

        fun removeSpace(spaceId: String) {
            condominium.spaces?.toMutableList()?.removeAll {
                it._id == spaceId
            }
        }
    }
}