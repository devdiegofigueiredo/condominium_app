package com.project.condominioapp.condominioapp.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.project.condominioapp.condominioapp.ui.profile.entities.Resident


class RoomDataConverter {

    @TypeConverter
    fun fromResidentList(countryLang: List<Resident>?): String? {
        if (countryLang == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<Resident>>() {

        }.type
        return gson.toJson(countryLang, type)
    }

    @TypeConverter
    fun toResidentList(countryLangString: String?): List<Resident>? {
        if (countryLangString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<Resident>>() {

        }.type
        return gson.fromJson(countryLangString, type)
    }
}