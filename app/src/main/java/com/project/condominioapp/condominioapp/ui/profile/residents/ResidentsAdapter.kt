package com.project.condominioapp.condominioapp.ui.profile.residents

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.profile.entities.Resident
import kotlin.reflect.KFunction0

class ResidentsAdapter(val showEmptyResidentMessage: KFunction0<Unit>) : androidx.recyclerview.widget.RecyclerView.Adapter<ResidentsAdapter.ViewHolder>() {

    val residents = arrayListOf<Resident>()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ResidentsAdapter.ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.residents_item, parent, false))

    override fun getItemCount(): Int = residents.size

    override fun onBindViewHolder(holder: ResidentsAdapter.ViewHolder, position: Int) {
        holder.name.text = residents[position].name
        holder.delete.setOnClickListener {
            residents.removeAt(position)
            notifyItemRemoved(position)

            residents.takeIf { it.isEmpty() }?.apply { showEmptyResidentMessage() }
        }
    }

    fun addResident(resident: Resident) {
        residents.add(resident)
        notifyDataSetChanged()
    }

    fun addResidents(residents: List<Resident>) {
        this.residents.addAll(residents)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val delete: ImageButton = itemView.findViewById(R.id.delete)
    }
}