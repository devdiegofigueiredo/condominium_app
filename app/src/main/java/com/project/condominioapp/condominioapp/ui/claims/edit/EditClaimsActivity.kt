package com.project.condominioapp.condominioapp.ui.claims.edit

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.BaseAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.claims.ClaimsActivity
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_new_claim.*
import kotlinx.android.synthetic.main.toolbar.*

class EditClaimsActivity : BaseActivity(), EditClaimsContract.View {

    companion object {
        const val EXTRA_CLAIM_ID = "extra_claim_id"
        const val EXTRA_TITLE = "extra_title"
        const val EXTRA_MESSAGE = "extra_message"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_claim)
        setupToolbar()

        loading = ProgressDialogUtil.getDialog(getString(R.string.edit_claims), this)

        small_description.setText(intent.extras.getString(EXTRA_TITLE))
        problem.setText(intent.extras.getString(EXTRA_MESSAGE))

        val interactor = EditClaimsInteractor(this)
        val presenter = EditClaimsPresenter(this, interactor)

        done.setOnClickListener {
            presenter.editClaim(intent.extras.getString(EXTRA_CLAIM_ID),
                    small_description,
                    problem)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun showErrorField(field: EditText) {
        field.requestFocus()
        field.error = getString(R.string.input_this_field)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun closeScreen(claim: Claim) {
        val gson = Gson()
        val vacancieJson = gson.toJson(claim)
        val intent = Intent()
        intent.putExtra(ClaimsActivity.EXTRA_CLAIM, vacancieJson)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.edit_claim)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}