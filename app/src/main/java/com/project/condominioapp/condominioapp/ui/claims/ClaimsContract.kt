package com.project.condominioapp.condominioapp.ui.claims

import com.project.condominioapp.condominioapp.ui.claims.entities.Claim

interface ClaimsContract {

    interface View {
        fun addClaims(claims: List<Claim>)
        fun showErrorView(message: String, textButton: String)
        fun showLoading()
        fun hideLoading()
        fun removeClaim(claimId: String)
        fun showEmptyView(message: String, textButton: String)
        fun updateDeleteLoadingMessage()
        fun updateGetClaimsLoadingMessage()
        fun showProgress()
        fun hideProgress()
    }

    interface Presenter {
        interface ClaimsCallback {
            fun onClaimsSuccess(claims: List<Claim>)
            fun onClaimsEmpty(message: String, textButton: String)
            fun onClaimsError(message: String, textButton: String)
        }

        interface DeleteClaimCallback {
            fun onDeleteClaimSuccess(claimId: String)
            fun onDeleteClaimError(message: String, textButton: String)
        }

        fun claims(page: Int)
        fun deleteClaim(claimId: String)
        fun onDestroy()
    }

    interface Interactor {
        fun claims(page: Int, callback: Presenter.ClaimsCallback)
        fun deleteClaim(claimId: String, callback: Presenter.DeleteClaimCallback)
        fun onDestroy()
    }
}