package com.project.condominioapp.condominioapp.ui.condominium.entities

import android.os.Parcel
import android.os.Parcelable

data class Condominium(val _id: String,
                       val name: String,
                       val isActive: Boolean,
                       val condominiumId: String,
                       val phone: String,
                       val street: String,
                       val postalCode: String,
                       val city: String,
                       val state: String,
                       var spaces: ArrayList<Space>?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(Space))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(name)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeString(condominiumId)
        parcel.writeString(phone)
        parcel.writeString(street)
        parcel.writeString(postalCode)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeTypedList(spaces)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Condominium> {
        override fun createFromParcel(parcel: Parcel): Condominium {
            return Condominium(parcel)
        }

        override fun newArray(size: Int): Array<Condominium?> {
            return arrayOfNulls(size)
        }
    }
}