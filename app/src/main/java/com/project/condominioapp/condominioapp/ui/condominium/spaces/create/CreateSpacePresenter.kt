package com.project.condominioapp.condominioapp.ui.condominium.spaces.create

import android.widget.EditText
import com.project.condominioapp.condominioapp.utils.convertToDouble

class CreateSpacePresenter(val view: CreateSpaceContract.View,
                           val interactor: CreateSpaceContract.Interactor) :
        CreateSpaceContract.Presenter,
        CreateSpaceContract.Presenter.CreateSpaceCallback {

    override fun createSpace(name: EditText, description: EditText, value: EditText, condominiumId: String) {
        takeIf { isFieldsValidateds(name, description, value) }?.apply {
            view.showDoneLoading()
            interactor.createSpace(name.text.toString(), description.text.toString(), value.text.toString().convertToDouble(), condominiumId, this)
        }
    }

    override fun onCreateSuccess(message: String) {
        view.showSimpleDialog(message, view.finishView())
        view.hideDoneLoading()
    }

    override fun onCreateError(message: String) {
        view.showSimpleDialog(message)
        view.hideDoneLoading()
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }

    private fun isFieldsValidateds(name: EditText, description: EditText, value: EditText): Boolean {

        name.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(name)
            return false
        }

        description.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(description)
            return false
        }

        value.takeIf { it.text.isEmpty() && value.text.toString().convertToDouble() > 0.0 }?.apply {
            view.showErrorField(name)
            return false
        }

        return true
    }
}