package com.project.condominioapp.condominioapp.ui.condominium.spaces.create

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import android.widget.EditText
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.TextFormatUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_create_space.*
import kotlinx.android.synthetic.main.done.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class CreateSpaceActivity : BaseActivity(), CreateSpaceContract.View {

    companion object {
        const val EXTRA_CONDOMINIUM_ID = "extra_condominium_id"
    }

    private lateinit var presenter: CreateSpacePresenter
    private lateinit var condominiumId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_space)
        setupToolbar(getString(R.string.create_space), true)

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_create_space), this)

        intent?.extras?.getString(EXTRA_CONDOMINIUM_ID)?.apply {
            condominiumId = this
        }

        value.addTextChangedListener(TextFormatUtil.moneyMask(value))

        val interactor = CreateSpaceInteractor(this)
        presenter = CreateSpacePresenter(this, interactor)

        done.setOnClickListener {
            presenter.createSpace(name, description, value, condominiumId)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun finishView(): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { _: DialogInterface, _: Int ->
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun showErrorField(field: EditText) {
        field.requestFocus()
        field.error = getString(R.string.input_this_field)
    }
}