package com.project.condominioapp.condominioapp.ui.releases.new

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_create_release.*
import java.util.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class CreateReleaseActivity : BaseActivity(), CreateReleaseContract.View {

    private lateinit var calendarDialog: AlertDialog
    private lateinit var presenter: CreateReleasePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_release)

        setupToolbar(getString(R.string.new_release), true)
        setupCalendar()

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_creating_liberation), this)

        check_box_free_entrance.setOnClickListener {
            check_box_free_entrance.takeIf { it.isChecked }?.apply {
                date_container.visibility = View.GONE
            } ?: kotlin.run { date_container.visibility = View.VISIBLE }
        }

        release_date.setOnClickListener { calendarDialog.show() }

        val interactor = CreateReleaseInteractor(this)
        presenter = CreateReleasePresenter(this, interactor)

        done.setOnClickListener {
            presenter.createRelease(check_box_free_entrance.isChecked, release_date, release_title, releaseds)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun showErrorField(field: EditText) {
        field.error = getString(R.string.input_this_field)
        field.requestFocus()
    }

    override fun closeScreen() {
        finish()
    }

    private fun setupCalendar() {
        val calendar = Calendar.getInstance()

        val view = LayoutInflater.from(this).inflate(R.layout.dialog_date_picker, null, false)
        val datePicker = view.findViewById(R.id.dp_calendar) as DatePicker
        datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), datePickerDialogListener)
        datePicker.minDate = calendar.timeInMillis

        val builder = AlertDialog.Builder(this)
        builder.setView(view)
        builder.setNegativeButton(getString(R.string.cancel)) { _, _ -> calendarDialog.dismiss() }
        builder.setPositiveButton(getString(R.string.ok)) { _, _ -> calendarDialog.dismiss() }

        calendarDialog = builder.create()
    }

    private var datePickerDialogListener: DatePicker.OnDateChangedListener =
            DatePicker.OnDateChangedListener { datePicker, year, monthOfYear, dayOfMonth ->
                release_date.setText(formatZeroDate(dayOfMonth) + "/" + formatZeroDate(monthOfYear + 1) + "/" + year.toString())
            }

    private fun formatZeroDate(value: Int): String {
        takeIf { value < 10 }?.apply {
            return "0$value"
        } ?: kotlin.run {
            return value.toString()
        }

        return ""
    }
}