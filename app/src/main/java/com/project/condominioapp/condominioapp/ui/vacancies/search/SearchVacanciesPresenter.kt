package com.project.condominioapp.condominioapp.ui.vacancies.search

import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

class SearchVacanciesPresenter(val view: SearchVacanciesContract.View,
                               val interactor: SearchVacanciesContract.Interactor) :
        SearchVacanciesContract.Presenter,
        SearchVacanciesContract.Presenter.VacanciesCallback,
        SearchVacanciesContract.Presenter.UserCallback,
        SearchVacanciesContract.Presenter.VacancieDeletedCallback {

    init {
        interactor.user(this)
    }

    override fun vacancies(text: String) {
        view.showLoading()
        interactor.vacancies(text, this)
    }

    override fun onVancanciesSuccess(vacancies: List<Vacancie>) {
        view.hideLoading()
        view.setupVacancies(vacancies)
        view.closeKeyboard()
    }

    override fun onVacanciesError(message: String) {
        view.showErrorView(message)
        view.hideLoading()
        view.closeKeyboard()
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }

    override fun onUserSuccess(currentUserId: String) {
        view.setupVacanciesList(currentUserId)
    }

    override fun deleteVacancie(vacancieId: String) {
        view.showLoading()
        interactor.deleteVacancie(vacancieId, this)
    }

    override fun onVacancieDeletedSuccess(message: String, vacancieId: String) {
        view.hideLoading()
        view.showErrorMessage(message)
        view.removeVacancieFromList(vacancieId)
    }

    override fun onVacancieDeletedError(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }
}