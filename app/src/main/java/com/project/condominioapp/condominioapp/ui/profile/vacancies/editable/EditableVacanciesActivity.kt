package com.project.condominioapp.condominioapp.ui.profile.vacancies.editable

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.project.condominioapp.condominioapp.R
import kotlinx.android.synthetic.main.activity_my_vacancies.*
import kotlinx.android.synthetic.main.toolbar.*

class EditableVacanciesActivity : AppCompatActivity(), EditableVacanciesContract.View {

    private lateinit var presenter: EditableVacanciesPresenter
    private lateinit var adapter: EditableVacanciesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editable_vacancies)

        setupToolbar()
        setupVacancies()

        val interactor = EditableVacanciesInteractor()
        presenter = EditableVacanciesPresenter(this, interactor)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        item.takeIf { it?.itemId == R.id.action_done }?.apply {
            adapter.vacancies?.apply { presenter.saveVacancies(this) }
                    ?: kotlin.run { showVacancieIncompletedMessage() }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_done, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun showVacancieIncompletedMessage() {
        Toast.makeText(this, "Incompleted", Toast.LENGTH_LONG).show()
    }

    private fun setupVacancies() {
        vacancies_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        adapter = EditableVacanciesAdapter(null)
        vacancies_list.adapter = adapter
    }

    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.vacancies)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}