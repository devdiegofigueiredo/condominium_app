package com.project.condominioapp.condominioapp.ui.login

import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.*
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.login.LoginContract.Presenter.LoginCallback
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LoginInteractor(val context: Activity) : LoginContract.Interactor {

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var getLoginToken: AsyncTask<Context, Void, String>
    private lateinit var saveToken: AsyncTask<Context, Void, Boolean>

    private val disposables = CompositeDisposable()

    override fun clearDatabase() {
        AppDatabase.instance(context)?.userDao()?.clear()
    }

    override fun onDestroy() {
        getLoginToken.apply { this.cancel(true) }
        saveToken.apply { this.cancel(true) }
        disposables.dispose()
    }

    override fun doFirebaseLogin(email: String, password: String, condominiumId: String, callback: LoginCallback) {
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(context, callbackFirebaseLogin(condominiumId, callback))
    }

    private fun callbackFirebaseLogin(condominiumId: String, callback: LoginCallback): OnCompleteListener<AuthResult> {
        return OnCompleteListener {
            if (it.isSuccessful) {
                it.result?.user?.uid?.apply { getUserInfo(this, condominiumId, callback) }
            } else {

                if (it.exception is FirebaseAuthInvalidCredentialsException ||
                        it.exception is FirebaseAuthInvalidUserException) {
                    callback.onLoginError(context.getString(R.string.user_not_existent_or_password))
                    return@OnCompleteListener
                }

                if (it.exception is FirebaseAuthUserCollisionException) {
                    callback.onLoginError(context.getString(R.string.email_already_exist))
                    return@OnCompleteListener
                }

                it.exception?.message?.apply {
                    FirebaseAuth.getInstance().signOut()
                    callback.onLoginError(this)
                }
            }
        }
    }

    private fun getUserInfo(id: String, condominiumId: String, callback: LoginCallback) {
        val callCreateUser = BaseService.getUserInfo(id, condominiumId)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        when {
                            this.status == 200 -> {
                                this.user.isPendency.takeIf { it }?.apply {
                                    callback.onLoginPending()
                                    FirebaseAuth.getInstance().signOut()
                                } ?: kotlin.run {
                                    saveUser(this.user, callback)
                                }
                            }
                            this.status == 402 -> {
                                FirebaseAuth.getInstance().signOut()
                                callback.onLoginError(this.message)
                            }
                            else -> {
                                FirebaseAuth.getInstance().signOut()
                                callback.onLoginError(context.getString(R.string.error_retrieve_user))
                            }
                        }
                    }
                }, {
                    FirebaseAuth.getInstance().signOut()
                    callback.onLoginError(context.getString(R.string.error_retrieve_user))
                })

        disposables.add(disposable)
    }

    private fun saveUser(user: User, callback: LoginCallback) {
        val disposable = Observable.fromCallable { }
                .map { saveDatabaseUser(user) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ isSaved ->
                    if (isSaved) {
                        callback.onLoginSuccess()
                    } else {
                        FirebaseAuth.getInstance().signOut()
                        callback.onLoginError(context.getString(R.string.techcnical_problem_occurred))
                    }
                }, {
                    FirebaseAuth.getInstance().signOut()
                    callback.onLoginError(context.getString(R.string.error_create_user))
                })

        disposables.add(disposable)
    }

    private fun saveDatabaseUser(user: User): Boolean {
        return try {
            AppDatabase.instance(context)?.userDao()?.insert(user)
            true
        } catch (e: Exception) {
            false
        }
    }
}