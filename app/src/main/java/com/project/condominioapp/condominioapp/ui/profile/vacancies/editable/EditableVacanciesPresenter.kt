package com.project.condominioapp.condominioapp.ui.profile.vacancies.editable

import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

class EditableVacanciesPresenter(val view: EditableVacanciesContract.View,
                                 val interactor: EditableVacanciesContract.Interactor) :
        EditableVacanciesContract.Presenter {

    override fun saveVacancies(vacancies: List<Vacancie>) {
        takeIf { isNotEmptyFields(vacancies) }?.apply {
            interactor.saveVacancies(vacancies)
        } ?: kotlin.run {
            view.showVacancieIncompletedMessage()
        }
    }

    private fun isNotEmptyFields(vacancies: List<Vacancie>): Boolean {
        vacancies.forEachIndexed { index, vacancie ->
            vacancie.apartmentNumber.takeIf { it.isEmpty() }?.apply { return false }
            vacancie.model.takeIf { it.isEmpty() }?.apply { return false }
            vacancie.user.id.takeIf { it.isEmpty() }?.apply { return false }
            vacancie.plate.takeIf { it.isEmpty() }?.apply { return false }
        }
        return true
    }
}