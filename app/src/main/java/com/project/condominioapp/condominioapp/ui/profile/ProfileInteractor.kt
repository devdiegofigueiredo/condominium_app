package com.project.condominioapp.condominioapp.ui.profile

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.profile.entities.UpdateUserRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ProfileInteractor(val context: Context) : ProfileContract.Interactor {

    private lateinit var currentUser: User

    override fun updateUser(name: String, phone: String, apartment: String, block: String, callback: ProfileContract.Presenter.UpdateUserCallback) {
        val updateUserRequest = UpdateUserRequest(currentUser.id, name, phone, apartment, block)
        val request = BaseService.updateUser(updateUserRequest)
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.status == 200) {
                            saveUser(this.user, callback)
                        } else {
                            callback.onUpdateUserError(context.getString(R.string.error_update_user))
                        }
                    }
                }, {
                    it.message?.apply { callback.onUpdateUserError(this) }
                })
    }

    override fun getUser(callback: ProfileContract.Presenter.GetUserCallback) {
        Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                        callback.onUserSuccess(this)
                    }
                }, {

                })
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }

    private fun saveUser(user: User, callback: ProfileContract.Presenter.UpdateUserCallback) {
        Observable.fromCallable { }
                .map { saveDatabaseUser(user) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ isSaved ->
                    if (isSaved) {
                        callback.onUpdateUserSuccess(context.getString(R.string.user_update_success))
                    } else {
                        callback.onUpdateUserError(context.getString(R.string.techcnical_problem_occurred))
                    }
                }, {
                    callback.onUpdateUserError(context.getString(R.string.error_update_user))
                })
    }

    private fun saveDatabaseUser(user: User): Boolean {
        return try {
            AppDatabase.instance(context)?.userDao()?.insert(user)
            true
        } catch (e: Exception) {
            false
        }
    }
}