package com.project.condominioapp.condominioapp.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.condominium.CondominiumActivity
import com.project.condominioapp.condominioapp.ui.login.LoginActivity
import com.project.condominioapp.condominioapp.ui.profile.ProfileActivity
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : BaseActivity(), SettingsContract.View {

    private lateinit var presenter: SettingsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setupToolbar(getString(R.string.settings), true)

        val interactor = SettingsInteractor(this)
        presenter = SettingsPresenter(this, interactor)

        profile.setOnClickListener { startActivity(Intent(this, ProfileActivity::class.java)) }

        condominium.setOnClickListener { startActivity(Intent(this, CondominiumActivity::class.java)) }

        logout.setOnClickListener {
            presenter.loggoutApp()
        }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun restartApp() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }
}