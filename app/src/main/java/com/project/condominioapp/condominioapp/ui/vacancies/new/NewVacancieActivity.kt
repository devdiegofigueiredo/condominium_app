package com.project.condominioapp.condominioapp.ui.vacancies.new

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.vacancies.VacanciesActivity
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.TextFormatUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_new_vacancies.*
import kotlinx.android.synthetic.main.toolbar.*

class NewVacancieActivity : BaseActivity(), NewVacancieContract.View {

    private lateinit var presenter: NewVacanciePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_vacancies)
        setupToolbar()

        loading = ProgressDialogUtil.getDialog(getString(R.string.create_vacancies), this)

        val interactor = NewVacancieInteractor(this)
        presenter = NewVacanciePresenter(this, interactor)

        plate.addTextChangedListener(TextFormatUtil.addMask("###-####", plate))
        done.setOnClickListener { presenter.createVacancie(model, plate, vacancie_number) }
    }

    override fun onResume() {
        super.onResume()
        showKeyboard()
    }

    override fun onStop() {
        hideKeyboard()
        super.onStop()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.takeIf { it.itemId == android.R.id.home }.apply {
            hideKeyboard()
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showErrorField(field: EditText) {
        field.requestFocus()
        field.error = getString(R.string.input_this_field)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun closeScreen(vacancie: Vacancie, currentUserId: String) {
        val gson = Gson()
        val vacancieJson = gson.toJson(vacancie)
        val intent = Intent()
        intent.putExtra(VacanciesActivity.EXTRA_VACANCIE, vacancieJson)
        intent.putExtra(VacanciesActivity.EXTRA_USER_ID, currentUserId)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.new_vacancie)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}