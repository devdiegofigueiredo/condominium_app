package com.project.condominioapp.condominioapp.data.remote

import com.project.condominioapp.condominioapp.data.datautil.Endpoints
import com.project.condominioapp.condominioapp.model.SimpleResponse
import com.project.condominioapp.condominioapp.ui.approvals.entities.AcceptResidentRequest
import com.project.condominioapp.condominioapp.ui.approvals.entities.PendencyResidentsResponse
import com.project.condominioapp.condominioapp.ui.home.entities.UpdateFirebaseTokenRequest
import com.project.condominioapp.condominioapp.ui.login.domain.entities.CreateUserRequest
import com.project.condominioapp.condominioapp.ui.login.domain.entities.LoginResponse
import com.project.condominioapp.condominioapp.ui.profile.entities.UpdateResidentsRequest
import com.project.condominioapp.condominioapp.ui.profile.entities.UpdateResidentsResponse
import com.project.condominioapp.condominioapp.ui.profile.entities.UpdateUserRequest
import io.reactivex.Observable
import retrofit2.http.*

interface UserService {

    @GET(Endpoints.getCurrentUser)
    fun getUserInfo(@Query(value = "firebaseId") firebaseId: String,
                    @Query(value = "condominiumId") condominiumId: String): Observable<LoginResponse>

    @POST(Endpoints.createUser)
    fun createUser(@Body user: CreateUserRequest): Observable<LoginResponse>

    @PUT(Endpoints.updateResidents)
    fun updateResidents(@Body user: UpdateResidentsRequest): Observable<UpdateResidentsResponse>

    @PUT(Endpoints.updateUser)
    fun updateUser(@Body user: UpdateUserRequest): Observable<LoginResponse>

    @GET(Endpoints.pendencyUsers)
    fun pendencyResident(@Query(value = "condominiumId") condominiumId: String): Observable<PendencyResidentsResponse>

    @PUT(Endpoints.updateFirebaseToken)
    fun updateFirebaseToken(@Body firebaseTokenRequest: UpdateFirebaseTokenRequest): Observable<SimpleResponse>

    @PUT(Endpoints.acceptResident)
    fun acceptResident(@Body acceptResidentRequest: AcceptResidentRequest): Observable<SimpleResponse>

    @DELETE(Endpoints.deleteResident)
    fun deleteResident(@Query(value = "userId") userId: String,
                       @Query(value = "condominiumId") condominiumId: String,
                       @Query(value = "firebaseId") firebaseId: String): Observable<SimpleResponse>
}