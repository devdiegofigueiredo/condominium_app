package com.project.condominioapp.condominioapp.ui.login.register.resident

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.home.entities.UpdateFirebaseTokenRequest
import com.project.condominioapp.condominioapp.ui.login.domain.entities.CreateUserRequest
import com.project.condominioapp.condominioapp.ui.login.register.resident.RegisterContract.Presenter.CreateUserCallback
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RegisterInteractor(private val context: Context) : RegisterContract.Interactor {

    private val disposables = CompositeDisposable()

    override fun createUser(name: String, email: String, password: String, phone: String, condominiumId: String, apartment: String, block: String, callback: CreateUserCallback) {
        val createUserRequest = CreateUserRequest(name, email, phone, condominiumId, apartment, block, password)
        val callCreateUser = BaseService.createUser(createUserRequest)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        when {
                            this.status == 200 -> {
                                callback.onCreateUserSuccess()
                                saveFirebaseToken(this.user)
                            }
                            this.status == 402 -> callback.onCreateUserError(this.message)
                            else -> callback.onCreateUserError(context.getString(R.string.error_create_user) + " Erro na requisição de criação de usuário")
                        }
                    }
                }, {
                    callback.onCreateUserError(context.getString(R.string.error_create_user) + " Erro ao requisitar a criação do usuário")
                })

        disposables.add(disposable)
    }

    private fun saveFirebaseToken(user: User) {
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        return@OnCompleteListener
                    }

                    val token = task.result!!.token

                    token.takeIf { it.isNotEmpty() }?.apply {
                        val updateUserRequest = UpdateFirebaseTokenRequest(user.condominiumId, user.id, token)
                        val request = BaseService.updateFirebaseToken(updateUserRequest)
                        request.subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ response ->
                                    Log.v("FirebaseToken Success", response.statusCode.toString())
                                }, {
                                    Log.v("FirebaseToken Error", it.message)
                                })
                    }
                })
    }

    override fun isValidEmail(email: String): Boolean {
        return if (TextUtils.isEmpty(email)) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }
    }

    override fun onDestroy() {
        disposables.dispose()
    }
}