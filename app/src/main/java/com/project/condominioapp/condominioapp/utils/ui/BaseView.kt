package com.project.condominioapp.condominioapp.utils.ui

import android.content.DialogInterface
import kotlin.reflect.KFunction0

interface BaseView {
    fun showLoading()
    fun hideLoading()
    fun showErrorMessage(message: String)
    fun showErrorView(message: String, onInfoButtonClicked: KFunction0<Unit>)
    fun showSimpleDialog(message: String)
    fun showSimpleDialog(message: String, listener: DialogInterface.OnClickListener?)
    fun showDoneLoading()
    fun hideDoneLoading()
}