package com.project.condominioapp.condominioapp.ui.releases.new

import android.widget.EditText
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface CreateReleaseContract {

    interface View : BaseView {
        fun showErrorField(field: EditText)
        fun closeScreen()
    }

    interface Presenter {
        interface ReleaseCallback {
            fun onCreateReleaseSuccess(message: String)
            fun onCreateReleaseError(message: String)
        }

        fun createRelease(isDateless: Boolean, releaseDate: EditText, releaseTitle: EditText, releaseds: EditText)
        fun onDestroy()
    }

    interface Interactor {
        fun createRelease(isDateless: Boolean,
                          releaseDate: String,
                          releaseTitle: String,
                          releaseds: String,
                          callback: Presenter.ReleaseCallback)

        fun onDestroy()
    }
}