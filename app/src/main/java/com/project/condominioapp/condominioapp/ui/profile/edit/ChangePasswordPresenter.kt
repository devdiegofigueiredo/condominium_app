package com.project.condominioapp.condominioapp.ui.profile.edit

import android.widget.EditText

class ChangePasswordPresenter(val view: ChangePasswordContract.View,
                              val interactor: ChangePasswordContract.Interactor) :
        ChangePasswordContract.Presenter,
        ChangePasswordContract.Presenter.PasswordCallback {

    override fun changePassword(currentPassword: EditText, newPassword: EditText, confirmPassword: EditText) {
        if (isFieldsValidateds(currentPassword, newPassword, confirmPassword)) {
            view.showLoading()
            interactor.updatePassword(currentPassword.text.toString(), newPassword.text.toString(), this)
        }
    }

    override fun onPasswordChangeSuccess(message: String) {
        view.hideLoading()
        view.showMessage(message)
        view.closeScreen()
    }

    override fun onPasswordChangeError(message: String) {
        view.hideLoading()
        view.showMessage(message)
    }

    private fun isFieldsValidateds(currentPassword: EditText, newPassword: EditText, confirmPassword: EditText): Boolean {
        currentPassword.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(currentPassword)
            return false
        }

        newPassword.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(newPassword)
            return false
        }

        newPassword.takeIf { it.text.toString().length < 6 }?.apply {
            view.showIncorrectSizeMessage(newPassword)
            return false
        }

        newPassword.takeIf { it.text.toString() != confirmPassword.text.toString() }?.apply {
            view.showWrongPasswordMessage()
            return false
        }

        confirmPassword.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(confirmPassword)
            return false
        }

        confirmPassword.takeIf { it.text.toString().length < 6 }?.apply {
            view.showIncorrectSizeMessage(confirmPassword)
            return false
        }

        return true
    }
}