package com.project.condominioapp.condominioapp.data.remote

import com.project.condominioapp.condominioapp.data.datautil.Endpoints
import com.project.condominioapp.condominioapp.ui.vacancies.entities.*
import io.reactivex.Observable
import retrofit2.http.*

interface VacancieService {

    @POST(Endpoints.createVacancie)
    fun createVacancie(@Body vacancieRequest: CreateVacancieRequest): Observable<CreateVacancieResponse>

    @GET(Endpoints.getVacancies)
    fun getVacancies(@Query(value = "condominiumId") condominiumId: String): Observable<VacanciesResponse>

    @PUT(Endpoints.editVacancie)
    fun editVacancie(@Body editVacancieRequest: EditVacancieRequest): Observable<EditVacancieResponse>

    @DELETE(Endpoints.deleteVacancie)
    fun deleteVacancie(@Query(value = "vacancieId") condominiumId: String): Observable<DeleteVacancieResponse>

    @GET(Endpoints.searchVacancies)
    fun searchVacancies(@Query(value = "condominiumId") condominiumId: String,
                        @Query(value = "vacancieNumber") vacancieNumber: String): Observable<VacanciesResponse>
}