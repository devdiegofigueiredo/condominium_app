package com.project.condominioapp.condominioapp.ui.vacancies.new

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.vacancies.entities.CreateVacancieRequest
import io.reactivex.Observable

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NewVacancieInteractor(val context: Context) : NewVacancieContract.Interactor {

    private lateinit var currentUser: User
    private val compositeDisposable = CompositeDisposable()

    init {
        getUserInformation()
    }

    override fun createVacancie(model: String,
                                plate: String,
                                vacancieNumber: String,
                                callback: NewVacancieContract.Presenter.CreateVacancieCallback) {

        val createClaimRequest = CreateVacancieRequest(currentUser.condominiumId,
                currentUser.id,
                model,
                plate,
                vacancieNumber,
                currentUser.apartment,
                currentUser.block)

        val request = BaseService.createVacancie(createClaimRequest)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onCreateVacancieSuccess(context.getString(R.string.vacancie_created), this.vacancie, currentUser.id)
                        } else {
                            callback.onCreateVacancieError(context.getString(R.string.error_create_vacancie))
                        }
                    }
                }, {
                    it.message?.apply { callback.onCreateVacancieError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun getUserInformation() {
        val disposable = Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                    }
                }, {

                })
        compositeDisposable.add(disposable)
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }

}