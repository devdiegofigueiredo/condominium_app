package com.project.condominioapp.condominioapp.ui.condominium.spaces

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.condominium.entities.Condominium
import com.project.condominioapp.condominioapp.ui.condominium.entities.Space
import com.project.condominioapp.condominioapp.ui.condominium.spaces.create.CreateSpaceActivity
import com.project.condominioapp.condominioapp.ui.condominium.spaces.create.CreateSpaceActivity.Companion.EXTRA_CONDOMINIUM_ID
import com.project.condominioapp.condominioapp.ui.condominium.spaces.edit.EditSpaceActivity
import com.project.condominioapp.condominioapp.ui.condominium.util.CondominiumSession
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import com.project.condominioapp.condominioapp.utils.ui.InfoFragment
import kotlinx.android.synthetic.main.activity_spaces.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class SpacesActivity : BaseActivity(), SpacesContract.View {

    private lateinit var condominium: Condominium
    private lateinit var deleteSpaceDialog: AlertDialog
    private lateinit var presenter: SpacesPresenter
    private lateinit var adapter: SpacesAdapter

    companion object {
        const val EXTRA_CONDOMINIUM = "exta_condominium"
        const val REQUEST_CREATE_SPACE = 1000
        const val REQUEST_EDIT_SPACE = 1001
        const val RESULT_SPACE = "result_space"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spaces)
        setupToolbar(getString(R.string.space), true)

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_deleting_space), this)
        setupSpaces()

        condominium = CondominiumSession.condominium()

        condominium.spaces?.let {
            it.takeIf { !it.isEmpty() }?.apply {
                adapter.addSpaces(this)
            } ?: kotlin.run { showEmptySpacesView() }
        } ?: kotlin.run { showEmptySpacesView() }


        val interactor = SpacesInteractor(this)
        presenter = SpacesPresenter(this, interactor)

        floating_action_button.setOnClickListener {
            val intent = Intent(baseContext, CreateSpaceActivity::class.java)
            intent.putExtra(EXTRA_CONDOMINIUM_ID, condominium._id)
            startActivityForResult(intent, REQUEST_CREATE_SPACE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        takeIf { resultCode == RESULT_OK }?.apply {
            when (requestCode) {
                REQUEST_CREATE_SPACE -> {
                    condominium = CondominiumSession.condominium()
                    condominium.spaces?.apply { adapter.addSpaces(this.toMutableList()) }

                    space_list.visibility.takeIf { it == View.GONE }?.apply { space_list.visibility = View.VISIBLE }
                }
                REQUEST_EDIT_SPACE -> {
                    val space = data?.extras?.getParcelable<Space>(RESULT_SPACE)
                    space?.apply { adapter.updateSpace(this) }
                }
            }
        }
    }

    override fun removeSpace(spaceId: String): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { _: DialogInterface, _: Int ->
            adapter.removeSpace(spaceId)
        }
    }

    private fun setupSpaces() {
        adapter = SpacesAdapter(this::showDeleteSpaceDialog, this::editSpace, this::showEmptySpacesView)

        space_list.layoutManager = LinearLayoutManager(baseContext)
        space_list.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.takeIf { it.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    private fun showEmptySpacesView() {
        space_list.visibility = View.GONE
        error_container.visibility = View.VISIBLE

        val errorFragment = InfoFragment(getString(R.string.anyone_space_was_created),
                getString(R.string.create_now),
                this::onInfoButtonClicked)
        supportFragmentManager.beginTransaction().replace(R.id.error_container, errorFragment).commitAllowingStateLoss()
    }

    private fun onInfoButtonClicked() {
        val intent = Intent(baseContext, CreateSpaceActivity::class.java)
        intent.putExtra(EXTRA_CONDOMINIUM_ID, condominium._id)
        startActivityForResult(intent, REQUEST_CREATE_SPACE)
    }

    private fun showDeleteSpaceDialog(spaceId: String) {
        val contextWrapper = ContextThemeWrapper(this, R.style.AlertDialogTheme)
        val dialog = AlertDialog.Builder(contextWrapper)
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.delete_dialog, null, false)

        deleteSpaceDialog = dialog.create()
        deleteSpaceDialog.setView(view)
        deleteSpaceDialog.setCancelable(true)
        deleteSpaceDialog.show()

        val yes = view.findViewById<TextView>(R.id.yes)
        yes.setOnClickListener {
            deleteSpaceDialog.dismiss()
            presenter.deleteSpace(spaceId, condominium._id)
        }

        val no = view.findViewById<TextView>(R.id.no)
        no.setOnClickListener { deleteSpaceDialog.dismiss() }
    }

    private fun editSpace(space: Space) {
        val intent = Intent(baseContext, EditSpaceActivity::class.java)
        intent.putExtra(EditSpaceActivity.EXTRA_CONDOMINIUM_ID, condominium._id)
        intent.putExtra(EditSpaceActivity.EXTRA_SPACE, space)
        startActivityForResult(intent, REQUEST_EDIT_SPACE)
    }
}