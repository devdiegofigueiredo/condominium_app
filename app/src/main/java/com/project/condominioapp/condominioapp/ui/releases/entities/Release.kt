package com.project.condominioapp.condominioapp.ui.releases.entities

data class Release(val id: String, val title: String, val names: String, val date: String)