package com.project.condominioapp.condominioapp.ui.profile.edit

import android.content.Context
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.project.condominioapp.condominioapp.R

class ChangePasswordInteractor(val context: Context) : ChangePasswordContract.Interactor {

    override fun updatePassword(currentPassword: String, newPassword: String, callback: ChangePasswordContract.Presenter.PasswordCallback) {
        val user = FirebaseAuth.getInstance().currentUser

        user?.email?.apply {
            val credential = EmailAuthProvider.getCredential(this, currentPassword)

            user.reauthenticate(credential).addOnCompleteListener {
                if (it.isSuccessful) {
                    user.updatePassword(newPassword).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            callback.onPasswordChangeSuccess(context.getString(R.string.password_change_success))
                        } else {
                            callback.onPasswordChangeError(context.getString(R.string.password_change_error))
                        }
                    }
                } else {
                    callback.onPasswordChangeError(context.getString(R.string.password_change_error))
                }
            }
        }
    }
}