package com.project.condominioapp.condominioapp.ui.vacancies.edit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.vacancies.VacanciesActivity
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import kotlinx.android.synthetic.main.activity_new_vacancies.*
import kotlinx.android.synthetic.main.toolbar.*

class EditVacanciesActivity : AppCompatActivity(), EditVacanciesContract.View {

    companion object {
        const val EXTRA_VACANCIE_ID = "extra_vacancie_id"
        const val EXTRA_MODEL = "extra_model"
        const val EXTRA_PLATE = "extra_plate"
        const val EXTRA_VACANCIE_NUMBER = "extra_vacancie_number"
        const val EXTRA_APARTMENT_NUMBER = "extra_apartment_number"
        const val EXTRA_APARTMENT_BLOCK = "extra_apartment_block"
    }

    private lateinit var loading: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_vacancies)
        setupToolbar()

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_update_vacancie), this)

        val interactor = EditVacanciesInteractor(this)
        val presenter = EditVacanciesPresenter(this, interactor)

        setupVacancie()

        val vacancieId = intent.extras.getString(EXTRA_VACANCIE_ID)
        done.setOnClickListener {
            presenter.editVacancie(vacancieId, model, plate, vacancie_number)
            closeKeyboard()
        }
    }

    private fun setupVacancie() {
        model.setText(intent.extras.getString(EXTRA_MODEL))
        plate.setText(intent.extras.getString(EXTRA_PLATE))
        vacancie_number.setText(intent.extras.getString(EXTRA_VACANCIE_NUMBER))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.takeIf { it.itemId == android.R.id.home }.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun showLoading() {
        loading.takeIf { !it.isShowing }.apply { loading.show() }
    }

    override fun hideLoading() {
        loading.takeIf { it.isShowing }.apply { loading.hide() }
    }

    override fun showErrorField(field: EditText) {
        field.requestFocus()
        field.error = getString(R.string.input_this_field)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun closeScreen(vacancie: Vacancie, currentUserId: String) {
        val gson = Gson()
        val vacancieJson = gson.toJson(vacancie)
        val intent = Intent()
        intent.putExtra(VacanciesActivity.EXTRA_VACANCIE, vacancieJson)
        intent.putExtra(VacanciesActivity.EXTRA_USER_ID, currentUserId)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.new_vacancie)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun closeKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}