package com.project.condominioapp.condominioapp.ui.home

import com.project.condominioapp.condominioapp.model.User

class HomePresenter(val view: HomeContract.View,
                    val interactor: HomeContract.Interactor) :
        HomeContract.Presenter,
        HomeContract.Presenter.UserCallback {

    override fun currentUser() {
        interactor.currentUser(this)
    }

    override fun onUserSuccess(user: User) {
        user.takeIf { it.isAdmin }?.apply {
            view.setupAdminFunctions()
        }
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}