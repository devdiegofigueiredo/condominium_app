package com.project.condominioapp.condominioapp.ui.login.register.resident

import android.widget.EditText

class RegisterPresenter(val view: RegisterContract.View,
                        val interactor: RegisterContract.Interactor) :
        RegisterContract.Presenter,
        RegisterContract.Presenter.CreateUserCallback {

    override fun createUser(name: String,
                            email: String,
                            password: String,
                            condominiumId: String,
                            phone: String,
                            apartment: String,
                            block: String) {
        view.showLoading()
        interactor.createUser(name, email, password, phone, condominiumId, apartment, block, this)

    }

    override fun onCreateUserSuccess() {
        view.showPendingAcceptDialog()
        view.hideLoading()
    }

    override fun onCreateUserError(message: String) {
        view.showMessage(message)
        view.hideLoading()
    }

    override fun isValidateFieldsToCreateUser(nameField: EditText,
                                              emailField: EditText,
                                              passwordField: EditText,
                                              condominiumId: EditText,
                                              phone: EditText,
                                              apartment: EditText,
                                              block: EditText): Boolean {
        if (nameField.text.trim().length <= 1) {
            view.showNameInvalidError()
            view.requestFocusField(nameField)
            return false
        }

        if (!interactor.isValidEmail(emailField.text.toString())) {
            view.showEmailInvalidError()
            view.requestFocusField(emailField)
            return false
        }

        if (passwordField.text.trim().length < 6) {
            view.showPasswordInvalidError()
            view.requestFocusField(passwordField)
            return false
        }

        if (condominiumId.text.trim().length < 3) {
            view.showCondominiumIdInvalidError()
            view.requestFocusField(condominiumId)
            return false
        }

        if (phone.text.trim().length < 10) {
            view.showPhoneInvalidError()
            view.requestFocusField(phone)
            return false
        }

        if (apartment.text.trim().length <= 1) {
            view.showApartmentInvalidError()
            view.requestFocusField(apartment)
            return false
        }

        if (block.text.trim().isEmpty()) {
            view.showBlockInvalidError()
            view.requestFocusField(block)
            return false
        }

        return true
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}