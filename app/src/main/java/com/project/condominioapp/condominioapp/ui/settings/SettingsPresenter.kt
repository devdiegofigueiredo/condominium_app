package com.project.condominioapp.condominioapp.ui.settings

class SettingsPresenter(val view: SettingsContract.View,
                        val interactor: SettingsContract.Interactor) :
        SettingsContract.Presenter,
        SettingsContract.Presenter.LoggoutCallback {

    override fun onDestroy() {
        interactor.onDestroy()
    }

    override fun loggoutApp() {
        interactor.loggoutApp(this)
    }

    override fun onLoggoutSuccess() {
        view.restartApp()
    }
}