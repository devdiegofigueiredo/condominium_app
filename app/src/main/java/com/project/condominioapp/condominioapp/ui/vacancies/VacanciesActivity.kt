package com.project.condominioapp.condominioapp.ui.vacancies

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.vacancies.edit.EditVacanciesActivity
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie
import com.project.condominioapp.condominioapp.ui.vacancies.new.NewVacancieActivity
import com.project.condominioapp.condominioapp.ui.vacancies.search.SearchVacanciesActivity
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import com.project.condominioapp.condominioapp.utils.ui.InfoFragment
import kotlinx.android.synthetic.main.activity_vacancies.*
import kotlinx.android.synthetic.main.toolbar.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class VacanciesActivity : BaseActivity(), VacanciesContract.View {

    private val NEW_VACANCIE_REQUEST_CODE = 1000
    private val EDIT_VACANCIE_REQUEST_CODE = 1001

    companion object {
        const val EXTRA_VACANCIE = "extra_vacancie"
        const val EXTRA_USER_ID = "extra_user_id"
    }

    private lateinit var presenter: VacanciesPresenter
    private lateinit var deleteVacancieDialog: AlertDialog
    private lateinit var adapter: VacanciesAdapter

    private var isEmptyVacancies = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vacancies)
        setupToolbar()

        loading = ProgressDialogUtil.getDialog(getString(R.string.fetch_vacancies), this)

        val interactor = VacanciesInteractor(this)
        presenter = VacanciesPresenter(this, interactor)

        floating_action_button.setOnClickListener {
            startActivityForResult(Intent(this, NewVacancieActivity::class.java), NEW_VACANCIE_REQUEST_CODE)
        }
    }


    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        takeIf { resultCode == RESULT_OK }?.apply {
            when (requestCode) {

                NEW_VACANCIE_REQUEST_CODE -> {
                    vacancies_list.visibility = View.VISIBLE
                    isEmptyVacancies = false

                    val jsonClaim = data?.extras?.getString(EXTRA_VACANCIE)
                    val gson = Gson()
                    val vacancie = gson.fromJson(jsonClaim, Vacancie::class.java)
                    data?.extras?.getString(EXTRA_USER_ID)?.apply { adapter.addVacancie(vacancie) }

                }

                EDIT_VACANCIE_REQUEST_CODE -> {
                    val jsonClaim = data?.extras?.getString(EXTRA_VACANCIE)
                    val gson = Gson()
                    val vacancie = gson.fromJson(jsonClaim, Vacancie::class.java)
                    data?.extras?.getString(EXTRA_USER_ID)?.apply { adapter.updateVacancie(vacancie) }
                }
            }
        } ?: kotlin.run {
            takeIf { isEmptyVacancies }?.apply {
                showEmptyView(getString(R.string.empty_vacancies), getString(R.string.create_now))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.takeIf { it.itemId == android.R.id.home }?.apply { finish() }
        item.takeIf { it.itemId == R.id.action_search }?.apply {
            startActivity(Intent(baseContext, SearchVacanciesActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun addVacancies(vacancies: List<Vacancie>, count: Int) {
        adapter.addVacancies(vacancies, count)
    }

    override fun setupVacancies(currentUserId: String) {
        vacancies_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        adapter = VacanciesAdapter(currentUserId,
                this::onDeleteClicked,
                this::onEditClicked, this,
                this::onClaimsEmpty,
                this::onLoadMore)
        vacancies_list.adapter = adapter
    }

    override fun showErrorView(message: String, textButton: String) {
        vacancies_list.visibility = View.GONE

        val errorFragment = InfoFragment(message, textButton, this::onInfoButtonClicked)
        supportFragmentManager.beginTransaction().replace(R.id.error_container, errorFragment).commitAllowingStateLoss()
    }

    override fun showEmptyView(message: String, textButton: String) {
        isEmptyVacancies = true
        vacancies_list.visibility = View.GONE

        val errorFragment = InfoFragment(message, textButton, this::onCreateNowClicked)
        supportFragmentManager.beginTransaction().replace(R.id.error_container, errorFragment).commitAllowingStateLoss()
    }

    override fun showErrorField(field: EditText) {
        field.error = getString(R.string.input_this_field)
        field.requestFocus()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun hideDeleteVacancieDialog() {
        deleteVacancieDialog.takeIf { it.isShowing }?.apply { this.dismiss() }
    }

    override fun removeVacancieFromList(vacancieId: String) {
        adapter.removeVacancieFromList(vacancieId)
    }

    override fun updateVacancie(vacancie: Vacancie) {
        adapter.updateVacancie(vacancie)
    }

    override fun showDeleteDialog() {
        loading = ProgressDialogUtil.getDialog(getString(R.string.delete_vacancies), this)
    }

    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.vacancies)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun updateDeleteLoadingMessage() {
        loading = ProgressDialogUtil.getDialog(getString(R.string.deleting_vacancie), this)
    }


    override fun updateGetVacanciesLoadingMessage() {
        loading = ProgressDialogUtil.getDialog(getString(R.string.fetch_vacancies), this)
    }

    private fun onCreateNowClicked() {
        startActivityForResult(Intent(this, NewVacancieActivity::class.java), NEW_VACANCIE_REQUEST_CODE)
    }

    private fun onInfoButtonClicked() {
        vacancies_list.visibility = View.VISIBLE
        presenter.vacancies()
    }

    private fun onDeleteClicked(vacancieId: String) {
        showDeleteVacancieDialog(vacancieId)
    }

    private fun onEditClicked(vacancieId: String,
                              model: String,
                              plate: String,
                              vacancieNumber: String,
                              apartmentNumber: String,
                              apartmentBlock: String) {
        val intent = Intent(this, EditVacanciesActivity::class.java)
        intent.putExtra(EditVacanciesActivity.EXTRA_VACANCIE_ID, vacancieId)
        intent.putExtra(EditVacanciesActivity.EXTRA_MODEL, model)
        intent.putExtra(EditVacanciesActivity.EXTRA_PLATE, plate)
        intent.putExtra(EditVacanciesActivity.EXTRA_VACANCIE_NUMBER, vacancieNumber)
        intent.putExtra(EditVacanciesActivity.EXTRA_APARTMENT_NUMBER, apartmentNumber)
        intent.putExtra(EditVacanciesActivity.EXTRA_APARTMENT_BLOCK, apartmentBlock)
        startActivityForResult(intent, EDIT_VACANCIE_REQUEST_CODE)
    }

    private fun showDeleteVacancieDialog(vacancieId: String) {
        val contextWrapper = ContextThemeWrapper(this, R.style.AlertDialogTheme)
        val dialog = AlertDialog.Builder(contextWrapper)
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.delete_dialog, null, false)

        deleteVacancieDialog = dialog.create()
        deleteVacancieDialog.setView(view)
        deleteVacancieDialog.setCancelable(true)
        deleteVacancieDialog.show()

        val yes = view.findViewById<TextView>(R.id.yes)
        yes.setOnClickListener {
            deleteVacancieDialog.dismiss()
            presenter.deleteVacancie(vacancieId)
        }

        val no = view.findViewById<TextView>(R.id.no)
        no.setOnClickListener { deleteVacancieDialog.dismiss() }
    }

    private fun onClaimsEmpty() {
        isEmptyVacancies = true
        showEmptyView(getString(R.string.empty_claims), getString(R.string.create_now))
    }

    private fun onLoadMore() {

    }
}