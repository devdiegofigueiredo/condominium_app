package com.project.condominioapp.condominioapp.data.local.dao

import androidx.room.*
import com.project.condominioapp.condominioapp.model.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    fun getUser(): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Query("DELETE FROM user")
    fun clear()

    @Delete
    fun delete(user: User)
}