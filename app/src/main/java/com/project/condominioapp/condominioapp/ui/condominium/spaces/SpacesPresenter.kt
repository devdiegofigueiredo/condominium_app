package com.project.condominioapp.condominioapp.ui.condominium.spaces

class SpacesPresenter(val view: SpacesContract.View,
                      val interactor: SpacesContract.Interactor) :
        SpacesContract.Presenter,
        SpacesContract.Presenter.DeleteSpaceCallback {

    override fun onDeleteSuccess(message: String, spaceId: String) {
        view.hideLoading()
        view.showSimpleDialog(message, view.removeSpace(spaceId))
    }

    override fun onDeleteError(message: String) {
        view.showSimpleDialog(message)
        view.hideLoading()
    }

    override fun deleteSpace(spaceId: String, condominiumId: String) {
        view.showLoading()
        interactor.deleteSpace(spaceId, condominiumId, this)
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}