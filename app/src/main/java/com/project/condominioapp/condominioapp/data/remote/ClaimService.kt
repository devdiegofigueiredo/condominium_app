package com.project.condominioapp.condominioapp.data.remote

import com.project.condominioapp.condominioapp.data.datautil.Endpoints
import com.project.condominioapp.condominioapp.model.SimpleResponse
import com.project.condominioapp.condominioapp.ui.approvals.entities.ReplyClaimRequest
import com.project.condominioapp.condominioapp.ui.claims.entities.*
import io.reactivex.Observable
import retrofit2.http.*

interface ClaimService {

    @POST(Endpoints.createClaim)
    fun createClaim(@Body claimRequest: CreateClaimRequest): Observable<CreateClaimResponse>

    @GET(Endpoints.getClaims)
    fun getClaims(@Query(value = "condominiumId") condominiumId: String,
                  @Query(value = "ownerId") userId: String,
                  @Query(value = "page") page: String): Observable<ClaimsResponse>

    @DELETE(Endpoints.deleteClaim)
    fun deleteClaim(@Query(value = "claimId") condominiumId: String): Observable<DeleteClaimResponse>

    @PUT(Endpoints.updateClaim)
    fun editClaim(@Body editClaimRequest: EditClaimRequest): Observable<EditClaimResponse>

    @GET(Endpoints.claimsUnreplieds)
    fun claimsUnreplieds(@Query(value = "condominiumId") condominiumId: String): Observable<ClaimsResponse>

    @PUT(Endpoints.addClaimReply)
    fun replyClaim(@Body replyClaimRequest: ReplyClaimRequest): Observable<SimpleResponse>

    @PUT(Endpoints.addSolvedClaimReply)
    fun replySolvedClaim(@Body replyClaimRequest: ReplyClaimRequest): Observable<SimpleResponse>

}