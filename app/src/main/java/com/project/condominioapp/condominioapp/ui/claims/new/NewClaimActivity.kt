package com.project.condominioapp.condominioapp.ui.claims.new

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.claims.ClaimsActivity
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import kotlinx.android.synthetic.main.activity_new_claim.*
import kotlinx.android.synthetic.main.toolbar.*

class NewClaimActivity : AppCompatActivity(), NewClaimContract.View {

    private lateinit var progress: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_claim)
        setupToolbar()

        val interactor = NewClaimInteractor(this)
        val presenter = NewClaimPresenter(this, interactor)

        progress = ProgressDialogUtil.getDialog(getString(R.string.creating_claim), this)

        problem_container.setOnClickListener { problem.requestFocus() }
        done.setOnClickListener { presenter.createClaim(small_description.text.toString(), problem.text.toString()) }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun closeScreen(claim: Claim) {
        val gson = Gson()
        val claimJson = gson.toJson(claim)
        val intent = Intent()
        intent.putExtra(ClaimsActivity.EXTRA_CLAIM, claimJson)
        setResult(RESULT_OK, intent)
        finish()
    }

    override fun showIncompleteFieldError() {
        Toast.makeText(this, getString(R.string.input_fields_create_claim), Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progress.show()
    }

    override fun hideLoading() {
        progress.dismiss()
    }


    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.claim)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}