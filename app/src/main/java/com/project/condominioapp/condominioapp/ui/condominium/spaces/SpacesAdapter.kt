package com.project.condominioapp.condominioapp.ui.condominium.spaces

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.condominium.entities.Space
import com.project.condominioapp.condominioapp.utils.formatValue
import kotlin.reflect.KFunction0
import kotlin.reflect.KFunction1

class SpacesAdapter(val showDeleteDialog: KFunction1<String, Unit>,
                    val editSpace: KFunction1<Space, Unit>,
                    val onSpacesEmpty: KFunction0<Unit>) : RecyclerView.Adapter<SpacesAdapter.ViewHolder>() {

    var spaces = arrayListOf<Space>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.spaces_item, parent, false))

    override fun getItemCount(): Int = spaces.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = spaces[position].name
        holder.description.text = spaces[position].description
        holder.value.text = spaces[position].value.formatValue()
        holder.delete.setOnClickListener { showDeleteDialog(spaces[position]._id) }
        holder.edit.setOnClickListener { editSpace(spaces[position]) }
    }

    fun updateSpace(spaceUpdated: Space) {
        spaces.forEachIndexed { index, space ->
            space._id.takeIf { it == spaceUpdated._id }?.apply {
                spaces[index] = spaceUpdated
                notifyDataSetChanged()
                return
            }
        }
    }

    fun addSpaces(spaces: List<Space>) {
        this.spaces.clear()
        this.spaces.addAll(spaces)
        notifyDataSetChanged()
    }

    fun removeSpace(spaceId: String) {
        spaces.removeAll {
            it._id == spaceId
        }
        notifyDataSetChanged()

        spaces.size.takeIf { it == 0 }?.apply { onSpacesEmpty() }
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.title)
        val description: TextView = itemView.findViewById(R.id.description)
        val value: TextView = itemView.findViewById(R.id.value)
        val edit: ImageButton = itemView.findViewById(R.id.edit)
        val delete: ImageButton = itemView.findViewById(R.id.delete)
    }
}