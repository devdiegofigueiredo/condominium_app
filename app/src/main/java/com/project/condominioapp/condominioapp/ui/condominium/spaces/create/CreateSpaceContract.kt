package com.project.condominioapp.condominioapp.ui.condominium.spaces.create

import android.content.DialogInterface
import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.condominium.entities.Space
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface CreateSpaceContract {

    interface View : BaseView {
        fun finishView(): DialogInterface.OnClickListener
        fun showErrorField(field: EditText)
    }

    interface Presenter {
        interface CreateSpaceCallback {
            fun onCreateSuccess(message: String)
            fun onCreateError(message: String)
        }

        fun createSpace(name: EditText, description: EditText, value: EditText, condominiumId: String)
        fun onDestroy()
    }

    interface Interactor {
        fun createSpace(name: String, description: String, value: Double, condominiumId: String, callback: Presenter.CreateSpaceCallback)
        fun onDestroy()
    }
}