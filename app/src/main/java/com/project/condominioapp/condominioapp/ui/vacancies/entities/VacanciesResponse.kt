package com.project.condominioapp.condominioapp.ui.vacancies.entities

data class VacanciesResponse(val statusCode: Int, val vacancies: List<Vacancie>?, val count: Int, val error: String)