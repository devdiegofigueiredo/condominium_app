package com.project.condominioapp.condominioapp.data.local.dao

import android.annotation.SuppressLint
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.utils.RoomDataConverter


@Database(entities = (arrayOf(User::class)), version = 1)
@TypeConverters(RoomDataConverter::class)
abstract class AppDatabase : RoomDatabase() {

    companion object {

        @SuppressLint("StaticFieldLeak")
        var database: AppDatabase? = null

        fun instance(context: Context): AppDatabase? {
            if (database == null) {
                database = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "user")
                        .build()
            }
            return database
        }

        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE 'user'" + " ADD COLUMN 'city' STRING" + " ADD COLUMN 'state' STRING")
            }
        }
    }

    abstract fun userDao(): UserDao
}