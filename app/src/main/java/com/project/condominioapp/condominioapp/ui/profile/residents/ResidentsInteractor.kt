package com.project.condominioapp.condominioapp.ui.profile.residents

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.profile.entities.Resident
import com.project.condominioapp.condominioapp.ui.profile.entities.UpdateResidentsRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ResidentsInteractor(val context: Context) : ResidentsContract.Interactor {

    private lateinit var currentUser: User
    private val disposables = CompositeDisposable()

    override fun updateResidents(residents: List<Resident>, callback: ResidentsContract.Presenter.ResidentsCallback) {
        val updateResidentsRequest = UpdateResidentsRequest(residents, currentUser.id, currentUser.condominiumId)
        val request = BaseService.updateResidents(updateResidentsRequest)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onResidentsSuccess(context.getString(R.string.residents_updated))
                        } else {
                            callback.onResidentsError(context.getString(R.string.error_update_residents))
                        }
                    }
                }, {
                    it.message?.apply { callback.onResidentsError(this) }
                })

        disposables.add(disposable)
    }

    override fun getCurrentUser(callback: ResidentsContract.Presenter.UserCallback) {
        val disposable = Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                        getUserInformation(callback)
                    }
                }, {

                })

        disposables.add(disposable)
    }

    private fun getUserInformation(callback: ResidentsContract.Presenter.UserCallback) {
        val request = BaseService.getUserInfo(currentUser.firebaseId, currentUser.condominiumId)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.status == 200) {
                            currentUser = this.user
                            this.user.residents?.apply { callback.onUserSuccess(this) }
                        } else {
                            callback.onUserError(context.getString(R.string.error_retrieve_user))
                        }
                    }
                }, {
                    it.message?.apply { callback.onUserError(this) }
                })

        disposables.add(disposable)
    }

    override fun onDestroy() {
        disposables.dispose()
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}