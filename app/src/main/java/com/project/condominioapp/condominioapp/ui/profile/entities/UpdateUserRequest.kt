package com.project.condominioapp.condominioapp.ui.profile.entities

data class UpdateUserRequest(
        val userId: String,
        val name: String,
        val phone: String,
        val apartment: String,
        val block: String
)