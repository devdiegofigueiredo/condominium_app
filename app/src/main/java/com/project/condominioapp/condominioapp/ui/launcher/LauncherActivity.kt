package com.project.condominioapp.condominioapp.ui.launcher

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.project.condominioapp.condominioapp.ui.home.HomeActivity
import com.project.condominioapp.condominioapp.ui.login.LoginActivity

class LauncherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            val intent = Intent(baseContext, LoginActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            startHomeScreen()
        }
    }

    private fun startHomeScreen() {
        startActivity(Intent(baseContext, HomeActivity::class.java))
        finish()
    }
}