package com.project.condominioapp.condominioapp.ui.vacancies

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie
import com.project.condominioapp.condominioapp.utils.TextFormatUtil
import kotlin.reflect.KFunction0
import kotlin.reflect.KFunction1
import kotlin.reflect.KFunction6

class VacanciesAdapter(private val currentUserId: String,
                       private val onDeleteClicked: KFunction1<String, Unit>,
                       private val onEditClicked: KFunction6<String, String, String, String, String, String, Unit>,
                       private val context: Context,
                       private val onClaimsEmpty: KFunction0<Unit>,
                       private val onLoadMore: KFunction0<Unit>) :
        androidx.recyclerview.widget.RecyclerView.Adapter<VacanciesAdapter.ViewHolder>() {

    private var vacancies = arrayListOf<Vacancie>()
    private var limit = 0

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.vacancies_item, parent, false))

    override fun getItemCount(): Int = vacancies.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.model.text = context.getString(R.string.model).plus(" ").plus(vacancies[position].model)
        holder.plate.text = context.getString(R.string.plate).plus(" ").plus(TextFormatUtil.addPlateMask(vacancies[position].plate.toUpperCase()))
        holder.vacancieNumber.text = context.getString(R.string.vacancie).plus(": ").plus(vacancies[position].vacancieNumber)
        holder.apartmentNumber.text = context.getString(R.string.apartment).plus(": ").plus(vacancies[position].apartmentNumber)
        holder.apartmentBlock.text = context.getString(R.string.block).plus(": ").plus(vacancies[position].apartmentBlock)

        vacancies[position].takeIf { it.user.id == currentUserId }?.apply {
            holder.edit.visibility = View.VISIBLE
            holder.delete.visibility = View.VISIBLE
            holder.edit.setOnClickListener {
                editVacancie(vacancies[position].id,
                        vacancies[position].model,
                        vacancies[position].plate,
                        vacancies[position].vacancieNumber,
                        vacancies[position].apartmentNumber,
                        vacancies[position].apartmentBlock)
            }

            holder.delete.setOnClickListener { deleteVacancie(vacancies[position].id) }
        } ?: kotlin.run {
            holder.edit.visibility = View.GONE
            holder.delete.visibility = View.GONE
        }

        position.takeIf { it == vacancies.size - 5 && vacancies.size < limit }?.apply {
            onLoadMore()
        }
    }

    private fun editVacancie(vacancieId: String,
                             model: String,
                             plate: String,
                             vacancieNumber: String,
                             apartmentNumber: String,
                             apartmentBlock: String) {
        onEditClicked(vacancieId, model, plate, vacancieNumber, apartmentNumber, apartmentBlock)
    }

    private fun deleteVacancie(vacancieId: String) {
        onDeleteClicked(vacancieId)
    }

    fun removeVacancieFromList(vacancieId: String) {
        vacancies.removeAll {
            it.id == vacancieId
        }

        notifyDataSetChanged()
        vacancies.size.takeIf { it == 0 }?.apply { onClaimsEmpty() }
    }

    fun updateVacancie(vacancieUpdated: Vacancie) {
        vacancies.forEachIndexed { index, vacancie ->
            vacancie.id.takeIf { it == vacancieUpdated.id }?.apply {
                vacancies[index] = vacancieUpdated
                notifyDataSetChanged()
            }
        }
    }

    fun addVacancies(vacancies: List<Vacancie>, count: Int) {
        this.vacancies.addAll(vacancies)
        this.limit = count
        notifyDataSetChanged()
    }

    fun addVacancie(vacancie: Vacancie) {
        vacancies.add(vacancie)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val model: TextView = itemView.findViewById(R.id.model)
        val plate: TextView = itemView.findViewById(R.id.plate)
        val vacancieNumber: TextView = itemView.findViewById(R.id.vacancieNumber)
        val apartmentNumber: TextView = itemView.findViewById(R.id.apartmentNumber)
        val apartmentBlock: TextView = itemView.findViewById(R.id.apartmentBlock)
        val edit: ImageButton = itemView.findViewById(R.id.ic_edit)
        val delete: ImageButton = itemView.findViewById(R.id.ic_remove)
    }
}