package com.project.condominioapp.condominioapp.ui.claims.entities

import com.google.gson.annotations.SerializedName
import com.project.condominioapp.condominioapp.model.User

data class Claim(@SerializedName("_id") val id: String,
                 val title: String,
                 val description: String,
                 val user: User,
                 val date: String,
                 val isReplied: Boolean,
                 val reply: String,
                 val repliedDate: String)