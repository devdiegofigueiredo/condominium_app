package com.project.condominioapp.condominioapp.ui.login.register.administrator

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.login.register.entities.CreateCondominiumRequest

interface RegisterAdministratorContract {

    interface View {
        fun requestFocusField(editText: EditText)
        fun showLoading()
        fun hideLoading()
        fun showErrorMessage(message: String)
        fun showInvalidEmail()
        fun showInvalidPhone()
        fun showInvalidCondominiumName()
        fun showInvalidAddress()
        fun showInvalidState()
        fun showInvalidCity()
        fun showInvalidPostalCode()
        fun showCreateCondominiumSuccessMessage()
        fun closeScreen()
    }

    interface Presenter {
        interface CreateCondominiumCallback {
            fun onCreateCondominiumSuccess(message: String)
            fun onCreateCondominiumError(message: String)
        }

        fun createCondominium(condominiumName: EditText,
                              email: EditText,
                              phone: EditText,
                              address: EditText,
                              state: EditText,
                              city: EditText,
                              postalCode: EditText)

        fun onDestroy()
    }

    interface Interactor {
        fun createCondominium(registerAdministratorRequest: CreateCondominiumRequest,
                              callback: Presenter.CreateCondominiumCallback)

        fun onDestroy()
    }

}