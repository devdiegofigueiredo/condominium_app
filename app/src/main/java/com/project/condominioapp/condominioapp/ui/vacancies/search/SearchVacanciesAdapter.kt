package com.project.condominioapp.condominioapp.ui.vacancies.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie
import kotlin.reflect.KFunction1
import kotlin.reflect.KFunction6

class SearchVacanciesAdapter(val context: Context,
                             private val currentUserId: String,
                             private val onDeleteClicked: KFunction1<String, Unit>,
                             private val onEditClicked: KFunction6<String, String, String, String, String, String, Unit>) :
        androidx.recyclerview.widget.RecyclerView.Adapter<SearchVacanciesAdapter.ViewHolder>() {

    private var vacancies = arrayListOf<Vacancie>()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.vacancies_item, parent, false))

    override fun getItemCount(): Int = vacancies.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.model.text = context.getString(R.string.model).plus(" ").plus(vacancies[position].model)
        holder.plate.text = context.getString(R.string.plate).plus(" ").plus(vacancies[position].plate)
        holder.vacancieNumber.text = context.getString(R.string.vacancie).plus(": ").plus(vacancies[position].vacancieNumber)
        holder.apartmentNumber.text = context.getString(R.string.apartment).plus(": ").plus(vacancies[position].apartmentNumber)
        holder.apartmentBlock.text = context.getString(R.string.block).plus(": ").plus(vacancies[position].apartmentBlock)

        vacancies[position].takeIf { it.user.id == currentUserId }?.apply {
            holder.edit.visibility = View.VISIBLE
            holder.delete.visibility = View.VISIBLE
            holder.edit.setOnClickListener {
                onEditClicked(vacancies[position].id, model, plate, vacancieNumber, apartmentNumber, apartmentBlock)
            }

            holder.delete.setOnClickListener { onDeleteClicked(vacancies[position].id) }
        } ?: kotlin.run {
            holder.edit.visibility = View.GONE
            holder.delete.visibility = View.GONE
        }
    }

    fun updateVacancie(vacancieUpdated: Vacancie) {
        vacancies.forEachIndexed { index, vacancie ->
            vacancie.id.takeIf { it == vacancieUpdated.id }?.apply {
                vacancies[index] = vacancieUpdated
                notifyDataSetChanged()
            }
        }
    }

    fun removeVacancieFromList(vacancieId: String) {
        vacancies.removeAll {
            it.id == vacancieId
        }
        notifyDataSetChanged()
    }

    fun addVacancies(vacancies: List<Vacancie>) {
        this.vacancies.clear()
        this.vacancies.addAll(vacancies)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val model: TextView = itemView.findViewById(R.id.model)
        val plate: TextView = itemView.findViewById(R.id.plate)
        val vacancieNumber: TextView = itemView.findViewById(R.id.vacancieNumber)
        val apartmentNumber: TextView = itemView.findViewById(R.id.apartmentNumber)
        val apartmentBlock: TextView = itemView.findViewById(R.id.apartmentBlock)
        val edit: ImageButton = itemView.findViewById(R.id.ic_edit)
        val delete: ImageButton = itemView.findViewById(R.id.ic_remove)
    }
}