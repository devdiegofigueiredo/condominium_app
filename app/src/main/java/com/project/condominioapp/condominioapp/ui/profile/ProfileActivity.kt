package com.project.condominioapp.condominioapp.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.profile.edit.ChangePasswordActivity
import com.project.condominioapp.condominioapp.ui.profile.residents.ResidentsActivity
import com.project.condominioapp.condominioapp.ui.profile.vacancies.MyVacanciesActivity
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.TextFormatUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_profile.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class ProfileActivity : BaseActivity(), ProfileContract.View {

    private lateinit var presenter: ProfilePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        setupToolbar(getString(R.string.my_profile), true)

        loading = ProgressDialogUtil.getDialog(getString(R.string.update_user), this)

        vacancie.setOnClickListener { startActivity(Intent(this, MyVacanciesActivity::class.java)) }
        password.setOnClickListener { startActivity(Intent(this, ChangePasswordActivity::class.java)) }
        residents.setOnClickListener { startActivity(Intent(this, ResidentsActivity::class.java)) }

        phone.addTextChangedListener(TextFormatUtil.phoneMask(phone))

        val interactor = ProfileInteractor(this)
        presenter = ProfilePresenter(this, interactor)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_done, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        item.takeIf { it?.itemId == R.id.action_done }?.apply { presenter.updateUser(name, phone, apartment, block) }
        return super.onOptionsItemSelected(item)
    }

    override fun showNameInvalidError() {
        name.error = getString(R.string.input_valid_name)
    }

    override fun showPhoneInvalidError() {
        phone.error = getString(R.string.input_valid_phone)
    }

    override fun showApartmentInvalidError() {
        apartment.error = getString(R.string.input_valid_apartment)
    }

    override fun showBlockInvalidError() {
        block.error = getString(R.string.input_valid_block)
    }

    override fun requestFieldFocus(field: EditText) {
        field.requestFocus()
    }

    override fun setupUserInformation(user: User) {
        name.setText(user.name)
        email.setText(user.email)
        phone.setText(user.phone)
        apartment.setText(user.apartment)
        block.setText(user.block)
    }
}