package com.project.condominioapp.condominioapp.ui.claims.edit

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim

interface EditClaimsContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun closeScreen(claim: Claim)
        fun showErrorField(field: EditText)
        fun showMessage(message: String)
    }

    interface Presenter {

        interface EditClaimCallback {
            fun onEditClaimSuccess(message: String, claim: Claim)
            fun onEditClaimError(message: String, textButton: String)
        }

        fun editClaim(claimId: String, title: EditText, message: EditText)
    }

    interface Interactor {
        fun editClaim(claimId: String, title: String, message: String, callback: Presenter.EditClaimCallback)
    }
}