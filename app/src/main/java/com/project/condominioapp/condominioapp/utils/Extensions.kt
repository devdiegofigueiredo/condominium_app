package com.project.condominioapp.condominioapp.utils


fun Double.formatValue() = "R$ ".plus(String.format("%.2f", this))

fun String.convertToDouble() = this.clearMoneyFormat().toDouble() / 100

fun String.clearMoneyFormat() = this.replace(Regex("[R$,.]"), "")