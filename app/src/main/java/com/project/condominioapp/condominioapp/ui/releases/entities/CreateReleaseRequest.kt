package com.project.condominioapp.condominioapp.ui.releases.entities

data class CreateReleaseRequest(val isDateless: Boolean, val releaseDate: String, val releaseTitle: String, val releaseds: String)