package com.project.condominioapp.condominioapp.ui.claims.new

import com.project.condominioapp.condominioapp.ui.claims.entities.Claim

class NewClaimPresenter(val view: NewClaimContract.View,
                        val interactor: NewClaimContract.Interactor) :
        NewClaimContract.Presenter,
        NewClaimContract.Presenter.ClaimCallback {

    override fun createClaim(title: String, description: String) {
        title.takeIf { it.isEmpty() }?.apply {
            view.showIncompleteFieldError()
            return
        }

        description.takeIf { it.isEmpty() }?.apply {
            view.showIncompleteFieldError()
            return
        }

        view.showLoading()
        interactor.createClaim(title, description, this)
    }

    override fun onCreateClaimSuccess(claim: Claim, message: String) {
        view.showMessage(message)
        view.closeScreen(claim)
        view.hideLoading()
    }

    override fun onCreateClaimError(message: String) {
        view.showMessage(message)
        view.hideLoading()
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}