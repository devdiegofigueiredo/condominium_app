package com.project.condominioapp.condominioapp.ui.vacancies.edit

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

class EditVacanciesPresenter(val view: EditVacanciesContract.View,
                             val interactor: EditVacanciesContract.Interactor) :
        EditVacanciesContract.Presenter,
        EditVacanciesContract.Presenter.EditVacancieCallback {

    override fun editVacancie(vacancieId: String, model: EditText, plate: EditText, vacancieNumber: EditText) {
        if (isFieldsValidateds(model, plate, vacancieNumber)) {
            view.showLoading()
            interactor.editVacancie(vacancieId,
                    model.text.toString(),
                    plate.text.toString(),
                    vacancieNumber.text.toString(),
                    this)
        }
    }

    override fun onEditVacancieSuccess(message: String, vacancie: Vacancie, currentUserId: String) {
        view.hideLoading()
        view.showMessage(message)
        view.closeScreen(vacancie, currentUserId)
    }

    override fun onEditVacancieError(message: String) {
        view.hideLoading()
        view.showMessage(message)
    }

    private fun isFieldsValidateds(model: EditText,
                                   plate: EditText,
                                   vacancieNumber: EditText): Boolean {

        model.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(model)
            return false
        }

        plate.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(plate)
            return false
        }

        vacancieNumber.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(vacancieNumber)
            return false
        }

        return true
    }
}