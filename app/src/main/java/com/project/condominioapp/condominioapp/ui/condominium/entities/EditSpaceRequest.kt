package com.project.condominioapp.condominioapp.ui.condominium.entities

data class EditSpaceRequest(val condominiumId: String,
                            val name: String,
                            val description: String,
                            val value: Double,
                            val spaceId: String)