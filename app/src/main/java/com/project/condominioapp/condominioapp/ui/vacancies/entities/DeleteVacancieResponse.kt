package com.project.condominioapp.condominioapp.ui.vacancies.entities

data class DeleteVacancieResponse(val statusCode: Int,
                                  val vacancie: Vacancie)