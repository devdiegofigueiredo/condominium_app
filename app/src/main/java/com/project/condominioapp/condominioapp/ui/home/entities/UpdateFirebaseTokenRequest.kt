package com.project.condominioapp.condominioapp.ui.home.entities

data class UpdateFirebaseTokenRequest(val condominiumId: String,
                                      val userId: String,
                                      val firebaseToken: String)