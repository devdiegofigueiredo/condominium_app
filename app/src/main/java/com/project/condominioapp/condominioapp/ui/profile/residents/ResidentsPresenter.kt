package com.project.condominioapp.condominioapp.ui.profile.residents

import com.project.condominioapp.condominioapp.ui.profile.entities.Resident

class ResidentsPresenter(val view: ResidentsContract.View,
                         val interactor: ResidentsContract.Interactor) :
        ResidentsContract.Presenter,
        ResidentsContract.Presenter.ResidentsCallback,
        ResidentsContract.Presenter.UserCallback {

    init {
        view.showLoading()
        interactor.getCurrentUser(this)
    }

    override fun updateResidents(residents: List<Resident>) {
        view.changeLoadingMessageToUpdate()
        view.showLoading()
        interactor.updateResidents(residents, this)
    }

    override fun onResidentsSuccess(message: String) {
        view.showErrorMessage(message)
        view.hideLoading()
        view.closeScreen()
    }

    override fun onResidentsError(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }

    override fun onUserSuccess(residents: List<Resident>) {
        view.hideLoading()
        view.setupResidents(residents)
    }

    override fun onUserError(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }
}