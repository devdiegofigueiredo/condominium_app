package com.project.condominioapp.condominioapp.ui.claims

import com.project.condominioapp.condominioapp.ui.claims.entities.Claim

class ClaimsPresenter(val view: ClaimsContract.View,
                      val interactor: ClaimsContract.Interactor) :
        ClaimsContract.Presenter,
        ClaimsContract.Presenter.ClaimsCallback,
        ClaimsContract.Presenter.DeleteClaimCallback {

    init {
        view.showProgress()
        interactor.claims(0, this)
    }

    override fun claims(page: Int) {
        view.updateGetClaimsLoadingMessage()
        view.showLoading()
        interactor.claims(page, this)
    }

    override fun onClaimsSuccess(claims: List<Claim>) {
        view.addClaims(claims)
        view.hideProgress()
        view.hideLoading()
    }

    override fun onClaimsEmpty(message: String, textButton: String) {
        view.showEmptyView(message, textButton)
        view.hideProgress()
        view.hideLoading()
    }

    override fun onClaimsError(message: String, textButton: String) {
        view.showErrorView(message, textButton)
        view.hideProgress()
        view.hideLoading()
    }

    override fun deleteClaim(claimId: String) {
        view.updateDeleteLoadingMessage()
        view.showLoading()
        interactor.deleteClaim(claimId, this)
    }

    override fun onDeleteClaimSuccess(claimId: String) {
        view.removeClaim(claimId)
        view.hideLoading()
    }

    override fun onDeleteClaimError(message: String, textButton: String) {
        view.showErrorView(message, textButton)
        view.hideLoading()
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}