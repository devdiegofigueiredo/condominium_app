package com.project.condominioapp.condominioapp.ui.approvals.resident

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_accept.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class AcceptResidentFragment : BaseFragment(), AcceptResidentContract.View {

    private lateinit var presenter: AcceptResidentPresenter
    private lateinit var adapter: AcceptResidentAdapter

    companion object {
        @JvmStatic
        fun newInstance(): AcceptResidentFragment {
            return AcceptResidentFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_accept, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.baseContext?.apply {
            val interactor = AcceptResidentInteractor(this)
            presenter = AcceptResidentPresenter(this@AcceptResidentFragment, interactor)
            presenter.pendencyResidents()

            adapter = AcceptResidentAdapter(this@AcceptResidentFragment::acceptResident,
                    this@AcceptResidentFragment::deleteResident,
                    this)

            accept_residents_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
            accept_residents_list.adapter = adapter
        }

        activity?.apply {
            loading = ProgressDialogUtil.getDialog(getString(R.string.wait_update_user), this)
        }
    }

    override fun addResidents(residents: List<User>) {
        adapter.addResidents(residents)
    }

    private fun acceptResident(userId: String) {
        presenter.acceptResident(userId)
    }

    private fun deleteResident(userId: String, firebaseId: String) {
        presenter.deleteResident(userId, firebaseId)
    }

    override fun emptyResidents() {
        empty_user_message.visibility = View.VISIBLE
        accept_residents_list.visibility = View.GONE
    }

    override fun showLayoutLoading() {
        progress.visibility = View.VISIBLE
    }

    override fun hideLayoutLoading() {
        progress.visibility = View.GONE
    }

    override fun removeResidentFromList(residentId: String) {
        adapter.removeResident(residentId)
    }

    override fun removeUserFromList(userId: String) {
        adapter.removeResident(userId)
    }

    override fun createErrorView(message: String) {
        empty_user_message.text = message
        empty_user_message.visibility = View.VISIBLE

        accept_residents_list.visibility = View.GONE
    }
}