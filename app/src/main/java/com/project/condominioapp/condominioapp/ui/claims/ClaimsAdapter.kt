package com.project.condominioapp.condominioapp.ui.claims

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim
import com.project.condominioapp.condominioapp.utils.DateUtil
import kotlin.reflect.KFunction0
import kotlin.reflect.KFunction1
import kotlin.reflect.KFunction3

class ClaimsAdapter(private val onClaimSelected: KFunction1<Claim, Unit>,
                    private val onEditClaimClicked: KFunction3<String, String, String, Unit>,
                    private val onEditDeleteClicked: KFunction1<String, Unit>,
                    private val onClaimsEmpty: KFunction0<Unit>,
                    private val onLoadMore: KFunction1<Int, Unit>) :
        androidx.recyclerview.widget.RecyclerView.Adapter<ClaimsAdapter.ViewHolder>() {

    private val claims = arrayListOf<Claim>()
    private val limit = 0
    private var page = 0

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ClaimsAdapter.ViewHolder =
            ClaimsAdapter.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.claims_item, parent, false))

    override fun getItemCount(): Int = claims.size

    override fun onBindViewHolder(holder: ClaimsAdapter.ViewHolder, position: Int) {
        holder.mainContainer.setOnClickListener { onClaimSelected(claims[position]) }
        holder.title.text = claims[position].title
        holder.smallDescription.text = claims[position].description
        holder.createdDate.text = DateUtil.convertStringToDate(claims[position].date)

        holder.delete.setOnClickListener { onEditDeleteClicked(claims[position].id) }
        holder.edit.setOnClickListener {
            onEditClaimClicked(claims[position].id,
                    claims[position].title,
                    claims[position].description)
        }

        claims[position].isReplied.takeIf { it }?.apply { holder.editableContainer.visibility = View.GONE }
                ?: kotlin.run { holder.editableContainer.visibility = View.VISIBLE }

        position.takeIf { it == limit - 5 }?.apply {
            onLoadMore(page++)
        }

        claims[position].takeIf { it.isReplied }?.apply { holder.positiveIcon.visibility = View.VISIBLE }
    }

    fun removeClaim(claimId: String) {
        claims.removeAll {
            it.id == claimId
        }
        notifyDataSetChanged()

        claims.size.takeIf { it == 0 }?.apply { onClaimsEmpty() }
    }

    fun updateClaim(claimUpdated: Claim) {
        claims.forEachIndexed { index, claim ->
            claim.id.takeIf { it == claimUpdated.id }?.apply {
                claims[index] = claimUpdated
                notifyDataSetChanged()
            }
        }
    }

    fun addClaim(claim: Claim) {
        claims.add(claim)
        notifyDataSetChanged()
    }

    fun addClaims(claims: List<Claim>) {
        this.claims.addAll(claims)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val mainContainer: androidx.cardview.widget.CardView = itemView.findViewById(R.id.main_container)
        val editableContainer: LinearLayout = itemView.findViewById(R.id.editable_container)
        val title: TextView = itemView.findViewById(R.id.title)
        val smallDescription: TextView = itemView.findViewById(R.id.small_description)
        val createdDate: TextView = itemView.findViewById(R.id.created_date)
        val edit: LinearLayout = itemView.findViewById(R.id.edit)
        val delete: LinearLayout = itemView.findViewById(R.id.delete)
        val positiveIcon: ImageView = itemView.findViewById(R.id.positive)
    }
}