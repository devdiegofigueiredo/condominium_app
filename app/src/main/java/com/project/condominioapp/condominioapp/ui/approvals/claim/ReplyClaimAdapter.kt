package com.project.condominioapp.condominioapp.ui.approvals.claim

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim
import kotlin.reflect.KFunction1

class ReplyClaimAdapter(val replyClaim: KFunction1<String, Unit>,
                        val solvedClaim: KFunction1<String, Unit>,
                        val context: Context) :
        androidx.recyclerview.widget.RecyclerView.Adapter<ReplyClaimAdapter.ViewHolder>() {

    val claims = arrayListOf<Claim>()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.reply_claim_item, parent, false))

    override fun getItemCount(): Int = claims.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = claims[position].title
        holder.description.text = claims[position].description
        holder.userName.text = claims[position].user.name
        holder.reply.setOnClickListener { replyClaim(claims[position].id) }
        holder.solved.setOnClickListener { solvedClaim(claims[position].id) }
    }

    fun addClaims(claims: List<Claim>) {
        this.claims.addAll(claims)
        notifyDataSetChanged()
    }

    fun removeClaim(claimId: String) {
        val it = claims.
                iterator()
        while (it.hasNext()) {
            val claim = it.next()
            claim.takeIf { it.id == claimId }?.apply {
                it.remove()
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.title)
        val description: TextView = itemView.findViewById(R.id.description)
        val userName: TextView = itemView.findViewById(R.id.name)
        val reply: TextView = itemView.findViewById(R.id.reply)
        val solved: TextView = itemView.findViewById(R.id.solved)
    }
}