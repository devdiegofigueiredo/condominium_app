package com.project.condominioapp.condominioapp.ui.condominium.spaces.edit

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.condominium.entities.Space
import com.project.condominioapp.condominioapp.utils.convertToDouble

class EditSpacePresenter(val view: EditSpaceContract.View,
                         val interactor: EditSpaceContract.Interactor) :
        EditSpaceContract.Presenter,
        EditSpaceContract.Presenter.EditSpaceCallback {

    override fun editSpace(name: EditText, description: EditText, value: EditText, condominiumId: String, spaceId: String) {
        takeIf { isFieldsValidateds(name, description, value) }?.apply {
            view.showDoneLoading()
            interactor.editSpace(name.text.toString(), description.text.toString(), value.text.toString().convertToDouble(), condominiumId, spaceId, this)
        }
    }

    override fun onEditSuccess(message: String, space: Space) {
        view.showSimpleDialog(message, view.finishView(space))
        view.hideDoneLoading()
    }

    override fun onEditError(message: String) {
        view.showSimpleDialog(message)
        view.hideDoneLoading()
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }

    private fun isFieldsValidateds(name: EditText, description: EditText, value: EditText): Boolean {

        name.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(name)
            return false
        }

        description.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(description)
            return false
        }

        value.takeIf { it.text.isEmpty() && value.text.toString().convertToDouble() > 0.0 }?.apply {
            view.showErrorField(name)
            return false
        }

        return true
    }
}