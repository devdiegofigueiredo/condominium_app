package com.project.condominioapp.condominioapp.ui.releases.new

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.releases.entities.CreateReleaseRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CreateReleaseInteractor(val context: Context) : CreateReleaseContract.Interactor {

    private lateinit var currentUser: User
    private val disposables = CompositeDisposable()

    init {
        val disposable = Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                    }
                }, {
                })

        disposables.add(disposable)
    }

    override fun createRelease(isDateless: Boolean, releaseDate: String, releaseTitle: String, releaseds: String, callback: CreateReleaseContract.Presenter.ReleaseCallback) {
        val body = CreateReleaseRequest(isDateless, releaseDate, releaseTitle, releaseds)
        val request = BaseService.createReleases(body)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onCreateReleaseSuccess(context.getString(R.string.release_created))
                        } else {
                            callback.onCreateReleaseError(context.getString(R.string.error_create_release))
                        }
                    }
                }, {
                    it.message?.apply { callback.onCreateReleaseError(this) }
                })

        disposables.add(disposable)
    }

    override fun onDestroy() {
        disposables.dispose()
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}