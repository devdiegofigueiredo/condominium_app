package com.project.condominioapp.condominioapp.ui.releases.new

import android.widget.EditText

class CreateReleasePresenter(val view: CreateReleaseContract.View,
                             val interactor: CreateReleaseContract.Interactor) :
        CreateReleaseContract.Presenter,
        CreateReleaseContract.Presenter.ReleaseCallback {

    override fun createRelease(isDateless: Boolean, releaseDate: EditText, releaseTitle: EditText, releaseds: EditText) {
        view.showLoading()
        takeIf { isValidatedFields(releaseDate, releaseTitle, releaseds, isDateless) }?.apply {
            interactor.createRelease(isDateless,
                    releaseDate.text.toString(),
                    releaseTitle.text.toString(),
                    releaseds.text.toString(),
                    this@CreateReleasePresenter)
        }
    }

    private fun isValidatedFields(releaseDate: EditText, releaseTitle: EditText, releaseds: EditText, dateless: Boolean): Boolean {

        releaseDate.takeIf { it.text.isEmpty() && !dateless }?.apply {
            view.showErrorField(releaseDate)
            return false
        }

        releaseTitle.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(releaseTitle)
            return false
        }

        releaseds.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(releaseds)
            return false
        }

        return true
    }

    override fun onCreateReleaseSuccess(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
        view.closeScreen()
    }

    override fun onCreateReleaseError(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}