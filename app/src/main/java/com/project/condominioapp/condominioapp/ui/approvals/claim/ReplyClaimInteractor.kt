package com.project.condominioapp.condominioapp.ui.approvals.claim

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.repository.CurrentUser
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.approvals.entities.ReplyClaimRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ReplyClaimInteractor(val context: Context) : ReplyClaimContract.Interactor {

    private val compositeDisposable = CompositeDisposable()

    override fun replySolvedClaim(claimId: String, callback: ReplyClaimContract.Presenter.CallbackReplySolved) {
        val replyClaimRequest = ReplyClaimRequest(claimId, "")
        val request = BaseService.replySolvedClaim(replyClaimRequest)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    takeIf { response.statusCode == 200 }?.apply {
                        callback.onReplySolvedSuccess(context.getString(R.string.reply_success), claimId)
                    } ?: kotlin.run {
                        callback.onReplySolvedError(context.getString(R.string.error_reply_solved))
                    }
                }, {
                    it.message?.apply { callback.onReplySolvedError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun claimsUnreplieds(callback: ReplyClaimContract.Presenter.CallbackClaims) {
        CurrentUser.user(userCallback(callback), context)
    }

    override fun replyClaim(claimId: String, reply: String, callback: ReplyClaimContract.Presenter.CallbackReply) {
        val replyClaimRequest = ReplyClaimRequest(claimId, reply)
        val request = BaseService.replyClaim(replyClaimRequest)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    takeIf { response.statusCode == 200 }?.apply {
                        callback.onReplySuccess(context.getString(R.string.reply_success), claimId)
                    } ?: kotlin.run {
                        callback.onReplyError(context.getString(R.string.error_reply_solved))
                    }
                }, {
                    it.message?.apply { callback.onReplyError(this) }
                })

        compositeDisposable.add(disposable)
    }

    private fun userCallback(callback: ReplyClaimContract.Presenter.CallbackClaims) = object : CurrentUser.UserCallback {

        override fun onUserSuccess(user: User) {
            val request = BaseService.claimsUnreplieds(user.condominiumId)
            val disposable = request.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ response ->
                        takeIf { response.statusCode == 200 }?.apply {
                            response.claims?.apply {
                                this.takeIf { it.isNotEmpty() }?.apply {
                                    callback.onClaimsSuccess(this)
                                } ?: kotlin.run {
                                    callback.onClaimsEmpty()
                                }
                            }
                        } ?: kotlin.run {
                            callback.onClaimsError(context.getString(R.string.error_get_claims))
                        }
                    }, {
                        it.message?.apply { callback.onClaimsError(this) }
                    })

            compositeDisposable.add(disposable)
        }
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}