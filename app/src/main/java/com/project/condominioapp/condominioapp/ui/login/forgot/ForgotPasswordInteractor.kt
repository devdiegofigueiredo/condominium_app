package com.project.condominioapp.condominioapp.ui.login.forgot

import android.content.Context
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.project.condominioapp.condominioapp.R

class ForgotPasswordInteractor(val context: Context) : ForgotPasswordContract.Interactor {

    private val firebaseAuth = FirebaseAuth.getInstance()

    override fun retrievePassword(email: String, callback: ForgotPasswordContract.Presenter.RetrievePasswordCallback) {
        firebaseAuth.setLanguageCode("PT-BR")
        firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(callbackResetPasswords(callback))
    }

    private fun callbackResetPasswords(callback: ForgotPasswordContract.Presenter.RetrievePasswordCallback): OnCompleteListener<Void> {
        return OnCompleteListener {
            if (it.isSuccessful) {
                callback.onPasswordSentSuccess(context.getString(R.string.forgot_password_sent))
            } else {
                it.exception?.message?.apply {
                    callback.onPasswordSentError(this)
                }
            }
        }
    }
}