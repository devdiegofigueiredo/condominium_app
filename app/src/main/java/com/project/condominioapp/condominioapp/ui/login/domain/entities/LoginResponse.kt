package com.project.condominioapp.condominioapp.ui.login.domain.entities

import com.google.gson.annotations.SerializedName
import com.project.condominioapp.condominioapp.model.User

data class LoginResponse(@SerializedName("statusCode") val status: Int,
                         val user: User,
                         val message: String)