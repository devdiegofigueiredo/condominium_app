package com.project.condominioapp.condominioapp.ui.condominium

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.repository.CurrentUser
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.condominium.edit.EditCondominiumActivity
import com.project.condominioapp.condominioapp.ui.condominium.entities.Condominium
import com.project.condominioapp.condominioapp.ui.condominium.spaces.SpacesActivity
import com.project.condominioapp.condominioapp.ui.condominium.spaces.SpacesActivity.Companion.EXTRA_CONDOMINIUM
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_condominium.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class CondominiumActivity : CondominiumContract.View, BaseActivity() {

    private var EDIT_CONDOMINIUM_REQUEST: Int = 1000
    private var condominium: Condominium? = null

    private lateinit var presenter: CondominiumPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_condominium)
        setupToolbar(getString(R.string.condominium), true)

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_search_condominium), this)

        val interactor = CondominiumInteractor(this)
        presenter = CondominiumPresenter(this, interactor)
        presenter.condominium()

        spaces.setOnClickListener {
            val intent = Intent(baseContext, SpacesActivity::class.java)
            intent.putExtra(EXTRA_CONDOMINIUM, condominium)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }

        CurrentUser.user(object : CurrentUser.UserCallback {
            override fun onUserSuccess(user: User) {
                item.takeIf { it?.itemId == R.id.action_edit }?.apply {
                    user.isAdmin.takeIf { it }?.apply {
                        condominium?.apply {
                            val intent = Intent(baseContext, EditCondominiumActivity::class.java)
                            intent.putExtra(EditCondominiumActivity.EXTRA_CONDOMINIUM, condominium)
                            startActivityForResult(intent, EDIT_CONDOMINIUM_REQUEST)
                        }
                    }
                }
            }
        }, this)

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_edit, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                takeIf { requestCode == EDIT_CONDOMINIUM_REQUEST }?.apply {

                    data?.apply {
                        val condominium = this.getParcelableExtra<Condominium>(EditCondominiumActivity.EXTRA_CONDOMINIUM)
                        setupCondominium(condominium)
                    }
                }
            }
        }
    }

    override fun setupCondominium(condominium: Condominium) {
        this.condominium = condominium

        name.text = getString(R.string.name).plus(": ").plus(condominium.name)
        phone.text = getString(R.string.phone).plus(": ").plus(condominium.phone)
        street.text = getString(R.string.address).plus(": ").plus(condominium.street)
        postal_code.text = getString(R.string.postal_code).plus(": ").plus(condominium.postalCode)
        city.text = condominium.city.plus(", ").plus(condominium.state)

        condominium_id.text = getString(R.string.condominium_id).plus(": ").plus(condominium.condominiumId)
    }
}