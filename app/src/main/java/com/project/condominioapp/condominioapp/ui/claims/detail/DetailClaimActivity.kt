package com.project.condominioapp.condominioapp.ui.claims.detail

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim
import com.project.condominioapp.condominioapp.utils.DateUtil
import kotlinx.android.synthetic.main.activity_detail_claim.*
import kotlinx.android.synthetic.main.toolbar.*

class DetailClaimActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_CLAIM = "extra_claim"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_claim)
        setupToolbar()

        val jsonClaim = intent?.extras?.getString(EXTRA_CLAIM)
        val claim = Gson().fromJson(jsonClaim, Claim::class.java)
        claim_title.text = claim.title
        description.text = claim.description
        created_date.text = DateUtil.convertStringToDate(claim.date)

        claim.takeIf { it.isReplied }?.apply {
            answer_container.visibility = View.VISIBLE
            admin_reply.text = reply
            reply_date.text = getString(R.string.reply_date) + DateUtil.convertStringToDate(repliedDate)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.detail_claim)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}