package com.project.condominioapp.condominioapp.ui.vacancies.entities

data class EditVacancieResponse(val statusCode: Int,
                                val vacancie: Vacancie)