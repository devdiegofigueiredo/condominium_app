package com.project.condominioapp.condominioapp.ui.approvals.claim

import android.content.Context
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim
import com.project.condominioapp.condominioapp.utils.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_accept2.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class ReplyClaimFragment : BaseFragment(), ReplyClaimContract.View {

    private lateinit var presenter: ReplyClaimPresenter
    private lateinit var adapter: ReplyClaimAdapter

    companion object {
        @JvmStatic
        fun newInstance(): ReplyClaimFragment {
            return ReplyClaimFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_accept, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.baseContext?.apply {
            val interactor = ReplyClaimInteractor(this)

            presenter = ReplyClaimPresenter(this@ReplyClaimFragment, interactor)
            presenter.claimNotAnswered()

            adapter = ReplyClaimAdapter(this@ReplyClaimFragment::replyClaim, this@ReplyClaimFragment::solvedClaim, this)
            accept_residents_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
            accept_residents_list.adapter = adapter
        }
    }

    override fun addClaims(claims: List<Claim>) {
        adapter.addClaims(claims)
    }

    private fun replyClaim(claimId: String) {
        activity?.apply {
            val contextWrapper = ContextThemeWrapper(this, R.style.AlertDialogTheme)
            val dialog = AlertDialog.Builder(contextWrapper)
            val inflater = LayoutInflater.from(this)
            val view = inflater.inflate(R.layout.reply_claim_dialog, null, false)

            val alertDialog = dialog.create()
            alertDialog.setView(view)
            alertDialog.setCancelable(true)
            alertDialog.setOnDismissListener {
                hideKeyboard()
            }
            alertDialog.show()

            val answer = view.findViewById<EditText>(R.id.answer)
            answer.requestFocus()
            showKeyboard()

            val dialogDone = view.findViewById<TextView>(R.id.done)
            dialogDone.setOnClickListener {
                presenter.replyClaim(claimId, answer.text.toString())
                alertDialog.dismiss()
            }
        }
    }

    private fun showKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    override fun onEmptyClaims() {
        empty_user_message.text = "Não há nenhuma reclamação aguardando resposta."
        empty_user_message.visibility = View.VISIBLE
        accept_residents_list.visibility = View.GONE
    }

    override fun removeClaim(claimId: String) {
        adapter.removeClaim(claimId)
    }

    private fun solvedClaim(claimId: String) {
        activity?.apply {
            val dialog = AlertDialog.Builder(this, R.style.AlertDialogTheme)

            dialog.setTitle("Alerta!")
            dialog.setMessage("Deseja informar que essa reclamção está resolvida?")

            dialog.setPositiveButton(getString(R.string.yes)) { _, _ -> presenter.replySolvedClaim(claimId) }
            dialog.setNegativeButton(getString(R.string.no), null)

            val alertDialog = dialog.create()
            alertDialog.setCancelable(true)
            alertDialog.show()
        }
    }

    override fun showLayoutLoading() {
        progress.visibility = View.VISIBLE
    }

    override fun hideLayoutLoading() {
        progress.visibility = View.GONE
    }

    override fun createErrorView(message: String) {
        error_container.visibility = View.VISIBLE
        showErrorView(message, this::onInfoButtonClicked)
    }

    private fun onInfoButtonClicked() {
        error_container.visibility = View.GONE
        presenter.claimNotAnswered()
    }
}