package com.project.condominioapp.condominioapp.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.approvals.ApprovalsActivity
import com.project.condominioapp.condominioapp.ui.claims.ClaimsActivity
import com.project.condominioapp.condominioapp.ui.releases.ReleasesActivity
import com.project.condominioapp.condominioapp.ui.settings.SettingsActivity
import com.project.condominioapp.condominioapp.ui.vacancies.VacanciesActivity
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), HomeContract.View {

    private lateinit var presenter: HomePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setupToolbar(getString(R.string.app_name), false)

        vacancies.setOnClickListener { startActivity(Intent(this, VacanciesActivity::class.java)) }

        claims.setOnClickListener { startActivity(Intent(this, ClaimsActivity::class.java)) }

        releases.setOnClickListener { startActivity(Intent(this, ReleasesActivity::class.java)) }

        approvals.setOnClickListener { startActivity(Intent(this, ApprovalsActivity::class.java)) }

        val interactor = HomeInteractor(this)
        presenter = HomePresenter(this, interactor)
        presenter.currentUser()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_settings, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item.takeIf { it.itemId == R.id.action_settings }?.apply { startActivity(Intent(baseContext, SettingsActivity::class.java)) }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun setupAdminFunctions() {
        approvals.visibility = View.VISIBLE
    }
}