package com.project.condominioapp.condominioapp.data.local

import android.app.Activity
import android.content.Context

class SharedPreferences {

    companion object {

        const val USER_TOKEN = "user_token"

        fun saveLoggedUser(activity: Activity, token: String) {
            val sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE)
            with(sharedPreferences.edit()) {
                putString(USER_TOKEN, token)
                apply()
            }
        }

        fun getLoggedUser(activity: Activity): String? {
            val sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE) ?: return ""
            return sharedPreferences.getString(USER_TOKEN, "")
        }

        fun removeLoggedUser(activity: Activity) {
            val sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE)
            with(sharedPreferences.edit()) {
                remove(USER_TOKEN)
                apply()
            }
        }
    }
}