package com.project.condominioapp.condominioapp.ui.vacancies.search

import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

interface SearchVacanciesContract {

    interface View {
        fun setupVacancies(vacancies: List<Vacancie>)
        fun setupVacanciesList(currentUserId: String)
        fun showLoading()
        fun hideLoading()
        fun showErrorView(message: String)
        fun showErrorMessage(message: String)
        fun closeKeyboard()
        fun removeVacancieFromList(vacancieId: String)
    }

    interface Presenter {
        interface VacanciesCallback {
            fun onVancanciesSuccess(vacancies: List<Vacancie>)
            fun onVacanciesError(message: String)
        }

        interface UserCallback {
            fun onUserSuccess(currentUserId: String)
        }

        interface VacancieDeletedCallback {
            fun onVacancieDeletedSuccess(message: String, vacancieId: String)
            fun onVacancieDeletedError(message: String)
        }

        fun vacancies(text: String)
        fun onDestroy()
        fun deleteVacancie(vacancieId: String)
    }


    interface Interactor {
        fun vacancies(text: String, callback: Presenter.VacanciesCallback)
        fun user(callback: Presenter.UserCallback)
        fun deleteVacancie(vacancieId: String, callback: Presenter.VacancieDeletedCallback)
        fun onDestroy()
    }
}