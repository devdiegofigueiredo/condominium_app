package com.project.condominioapp.condominioapp.ui.vacancies.edit

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

interface EditVacanciesContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun showErrorField(field: EditText)
        fun showMessage(message: String)
        fun closeScreen(vacancie: Vacancie, currentUserId: String)
    }

    interface Presenter {
        interface EditVacancieCallback {
            fun onEditVacancieSuccess(message: String, vacancie: Vacancie, currentUserId: String)
            fun onEditVacancieError(message: String)
        }

        fun editVacancie(vacancieId: String,
                         model: EditText,
                         plate: EditText,
                         vacancieNumber: EditText)
    }

    interface Interactor {
        fun editVacancie(vacancieId: String,
                         model: String,
                         plate: String,
                         vacancieNumber: String,
                         callback: Presenter.EditVacancieCallback)
    }
}