package com.project.condominioapp.condominioapp.ui.vacancies

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface VacanciesContract {

    interface View : BaseView {
        fun addVacancies(vacancies: List<Vacancie>, count: Int)
        fun setupVacancies(currentUserId: String)
        fun showErrorView(message: String, textButton: String)
        fun showEmptyView(message: String, textButton: String)
        fun showErrorField(field: EditText)
        fun showMessage(message: String)
        fun hideDeleteVacancieDialog()
        fun removeVacancieFromList(vacancieId: String)
        fun updateVacancie(vacancie: Vacancie)
        fun showDeleteDialog()
        fun updateDeleteLoadingMessage()
        fun updateGetVacanciesLoadingMessage()
        fun showProgress()
        fun hideProgress()
    }

    interface Presenter {
        interface VacanciesCallback {
            fun onVancanciesSuccess(vacancies: List<Vacancie>, count: Int)
            fun onVancanciesEmpty(message: String, textButton: String)
            fun onVacanciesError(message: String, textButton: String)
        }

        interface VacancieDeletedCallback {
            fun onVacancieDeletedSuccess(message: String, vacancieId: String)
            fun onVacancieDeletedError(message: String)
        }

        interface VacancieEditedCallback {
            fun onVacancieEditedSuccess(message: String, vacancie: Vacancie)
            fun onVacancieEditedError(message: String, textButton: String)
        }

        interface UserCallback {
            fun onUserSuccess(currentUserId: String)
        }

        fun vacancies()
        fun deleteVacancie(vacancieId: String)
        fun editVacancie(vacancieId: String,
                         model: EditText,
                         plate: EditText,
                         vacancieNumber: EditText,
                         apartmentNumber: EditText,
                         apartmentBlock: EditText)
    }

    interface Interactor {
        fun vacancies(callback: Presenter.VacanciesCallback)
        fun deleteVacancie(vacancieId: String, callback: Presenter.VacancieDeletedCallback)
        fun editVacancie(vacancieId: String,
                         model: String,
                         plate: String,
                         vacancieNumber: String,
                         apartmentNumber: String,
                         apartmentBlock: String,
                         callback: Presenter.VacancieEditedCallback)

        fun user(callback: Presenter.UserCallback)
        fun onDestroy()
    }
}