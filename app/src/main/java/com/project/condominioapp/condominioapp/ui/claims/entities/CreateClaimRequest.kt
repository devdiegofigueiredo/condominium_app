package com.project.condominioapp.condominioapp.ui.claims.entities

data class CreateClaimRequest(val title: String, val description: String, val ownerId: String, val condominiumId: String)