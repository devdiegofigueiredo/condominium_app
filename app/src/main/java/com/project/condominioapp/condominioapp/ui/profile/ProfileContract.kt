package com.project.condominioapp.condominioapp.ui.profile

import android.widget.EditText
import com.project.condominioapp.condominioapp.model.User

interface ProfileContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun showErrorMessage(message: String)
        fun setupUserInformation(user: User)
        fun requestFieldFocus(field: EditText)
        fun showNameInvalidError()
        fun showPhoneInvalidError()
        fun showApartmentInvalidError()
        fun showBlockInvalidError()
    }

    interface Presenter {
        interface UpdateUserCallback {
            fun onUpdateUserSuccess(message: String)
            fun onUpdateUserError(message: String)
        }

        interface GetUserCallback {
            fun onUserSuccess(user: User)
        }

        fun updateUser(name: EditText, phone: EditText, apartment: EditText, block: EditText)
    }

    interface Interactor {
        fun updateUser(name: String, phone: String, apartment: String, block: String, callback: Presenter.UpdateUserCallback)
        fun getUser(callback: Presenter.GetUserCallback)
    }
}