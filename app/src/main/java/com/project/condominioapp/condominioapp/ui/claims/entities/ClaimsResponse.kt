package com.project.condominioapp.condominioapp.ui.claims.entities

data class ClaimsResponse(val statusCode: Int, val count: Int, var claims: List<Claim>?)