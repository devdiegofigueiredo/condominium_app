package com.project.condominioapp.condominioapp.ui.vacancies.search

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SearchVacanciesInteractor(val context: Context) : SearchVacanciesContract.Interactor {

    private val disposables = CompositeDisposable()
    private lateinit var currentUser: User

    override fun vacancies(text: String, callback: SearchVacanciesContract.Presenter.VacanciesCallback) {
        val callCreateUser = BaseService.searchVacancies(currentUser.condominiumId, text)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            this.vacancies?.takeIf { it.isNotEmpty() }?.apply {
                                callback.onVancanciesSuccess(this)
                            } ?: kotlin.run {
                                callback.onVacanciesError(context.getString(R.string.empty_vacancies_searched))
                            }
                        } else {
                            callback.onVacanciesError(context.getString(R.string.error_search_vacancies))
                        }
                    }
                }, {
                    it.message?.apply { callback.onVacanciesError(this) }
                })

        disposables.add(disposable)
    }

    override fun user(callback: SearchVacanciesContract.Presenter.UserCallback) {
        getUserInformation(callback)
    }

    override fun onDestroy() {
        disposables.dispose()
    }

    override fun deleteVacancie(vacancieId: String, callback: SearchVacanciesContract.Presenter.VacancieDeletedCallback) {
        val request = BaseService.deleteVacancie(vacancieId)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onVacancieDeletedSuccess(context.getString(R.string.vacancie_deleted), vacancieId)
                        } else {
                            callback.onVacancieDeletedError(context.getString(R.string.error_delete_vacancie))
                        }
                    }
                }, {
                    it.message?.apply { callback.onVacancieDeletedError(this) }
                })

        disposables.add(disposable)
    }

    private fun getUserInformation(callback: SearchVacanciesContract.Presenter.UserCallback) {
        val disposable = Single.fromCallable { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                        callback.onUserSuccess(this.id)
                    }
                }, {
                })

        disposables.add(disposable)
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}