package com.project.condominioapp.condominioapp.ui.releases.entities

data class ReleasesResponse(val statusCode: Int, val releases: List<Release>)