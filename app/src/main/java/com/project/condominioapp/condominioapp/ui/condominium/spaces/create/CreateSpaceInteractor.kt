package com.project.condominioapp.condominioapp.ui.condominium.spaces.create

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.ui.condominium.entities.CreateSpaceRequest
import com.project.condominioapp.condominioapp.ui.condominium.util.CondominiumSession
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CreateSpaceInteractor(val context: Context) : CreateSpaceContract.Interactor {

    private val disposables = CompositeDisposable()

    override fun createSpace(name: String, description: String, value: Double, condominiumId: String, callback: CreateSpaceContract.Presenter.CreateSpaceCallback) {
        val createSpaceRequest = CreateSpaceRequest(condominiumId, name, description, value)
        val request = BaseService.createSpace(createSpaceRequest)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        takeIf { this.statusCode == 200 }?.apply {
                            CondominiumSession.condominium(this.condominium)
                            callback.onCreateSuccess(context.getString(R.string.space_create_sucessfully))
                        } ?: kotlin.run {
                            callback.onCreateError(context.getString(R.string.error_create_space))
                        }
                    } ?: kotlin.run {
                        callback.onCreateError(context.getString(R.string.error_create_space))
                    }
                }, {
                    it.message?.apply { callback.onCreateError(this) }
                })

        disposables.add(disposable)
    }

    override fun onDestroy() {
        disposables.dispose()
    }
}