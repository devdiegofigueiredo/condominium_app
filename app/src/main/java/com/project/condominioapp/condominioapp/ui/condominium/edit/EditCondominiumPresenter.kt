package com.project.condominioapp.condominioapp.ui.condominium.edit

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.condominium.entities.Condominium
import com.project.condominioapp.condominioapp.ui.condominium.entities.EditCondominiumRequest

class EditCondominiumPresenter(val view: EditCondominiumContract.View,
                               val interactor: EditCondominiumContract.Interactor) :
        EditCondominiumContract.Presenter,
        EditCondominiumContract.Presenter.EditCondominiumCallback {

    override fun editCondominium(name: EditText,
                                 phone: EditText,
                                 street: EditText,
                                 city: EditText,
                                 state: EditText,
                                 postal_code: EditText,
                                 condominiumId: String) {
        if (isFieldsValidateds(name, phone, street, city, state, postal_code)) {

            val editCondominium = EditCondominiumRequest(name.text.toString(),
                    phone.text.toString(),
                    street.text.toString(),
                    city.text.toString(),
                    state.text.toString(),
                    postal_code.text.toString(),
                    condominiumId)

            view.showLoading()
            interactor.editCondominium(editCondominium, this)
        }
    }

    override fun onEditCondominiumSuccess(message: String, condominium: Condominium) {
        view.showSimpleDialog(message, view.finishView(condominium))
        view.hideLoading()
    }

    override fun onEditCondominiumError(message: String) {
        view.showSimpleDialog(message)
        view.hideLoading()
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }

    private fun isFieldsValidateds(name: EditText,
                                   phone: EditText,
                                   street: EditText,
                                   city: EditText,
                                   state: EditText,
                                   postal_code: EditText): Boolean {

        name.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(this)
            return false
        }

        phone.takeIf { it.text.isEmpty() || it.text.trim().length < 10 }?.apply {
            view.showErrorField(this)
            return false
        }

        street.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(this)
            return false
        }

        city.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(this)
            return false
        }

        state.takeIf { it.text.isEmpty() && it.text.trim().length < 2 }?.apply {
            view.showErrorField(this)
            return false
        }

        postal_code.takeIf { it.text.isEmpty() || it.text.trim().length < 9 }?.apply {
            view.showErrorField(this)
            return false
        }

        return true
    }

}