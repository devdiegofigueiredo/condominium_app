package com.project.condominioapp.condominioapp.ui.profile

import android.widget.EditText
import com.project.condominioapp.condominioapp.model.User

class ProfilePresenter(val view: ProfileContract.View,
                       val interactor: ProfileContract.Interactor) :
        ProfileContract.Presenter,
        ProfileContract.Presenter.UpdateUserCallback,
        ProfileContract.Presenter.GetUserCallback {

    init {
        interactor.getUser(this)
    }

    override fun updateUser(name: EditText, phone: EditText, apartment: EditText, block: EditText) {
        takeIf { isFieldsValidateds(name, phone, apartment, block) }?.apply {
            view.showLoading()
            interactor.updateUser(name.text.toString(), phone.text.toString(), apartment.text.toString(), block.text.toString(), this@ProfilePresenter)
        }
    }

    override fun onUpdateUserSuccess(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }

    override fun onUpdateUserError(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }

    override fun onUserSuccess(user: User) {
        view.setupUserInformation(user)
    }


    private fun isFieldsValidateds(name: EditText, phone: EditText, apartment: EditText, block: EditText): Boolean {
        name.takeIf { it.text.isEmpty() }?.apply {
            view.showNameInvalidError()
            view.requestFieldFocus(name)
            return false
        }

        if (phone.text.trim().length < 10) {
            view.showPhoneInvalidError()
            view.requestFieldFocus(phone)
            return false
        }

        apartment.takeIf { it.text.isEmpty() }?.apply {
            view.showApartmentInvalidError()
            return false
        }

        block.takeIf { it.text.isEmpty() }?.apply {
            view.showBlockInvalidError()
            view.requestFieldFocus(block)
            return false
        }

        return true
    }
}