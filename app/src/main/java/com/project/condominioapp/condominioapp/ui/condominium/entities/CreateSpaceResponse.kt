package com.project.condominioapp.condominioapp.ui.condominium.entities

data class CreateSpaceResponse(val statusCode: Int, val condominium: Condominium)