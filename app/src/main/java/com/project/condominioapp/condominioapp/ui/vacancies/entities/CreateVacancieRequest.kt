package com.project.condominioapp.condominioapp.ui.vacancies.entities

data class CreateVacancieRequest(val condominiumId: String,
                                 val ownerId: String,
                                 val model: String,
                                 val plate: String,
                                 val vacancieNumber: String,
                                 val apartmentNumber: String,
                                 val apartmentBlock: String)