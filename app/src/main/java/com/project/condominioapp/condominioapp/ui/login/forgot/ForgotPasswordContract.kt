package com.project.condominioapp.condominioapp.ui.login.forgot

interface ForgotPasswordContract {

    interface View {
        fun showMessage(message: String)
        fun showLoading()
        fun hideLoading()
    }

    interface Presenter {

        interface RetrievePasswordCallback {
            fun onPasswordSentSuccess(message: String)
            fun onPasswordSentError(message: String)
        }

        fun retrievePassword(email: String)
    }

    interface Interactor {
        fun retrievePassword(email: String, callback: Presenter.RetrievePasswordCallback)
    }
}