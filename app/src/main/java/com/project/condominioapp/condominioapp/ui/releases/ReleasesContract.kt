package com.project.condominioapp.condominioapp.ui.releases

import com.project.condominioapp.condominioapp.ui.releases.entities.Release
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface ReleasesContract {

    interface View : BaseView {
        fun setupReleases(releases: List<Release>)
    }

    interface Presenter {
        interface ReleasesCallback {
            fun onReleasesSuccess(releases: List<Release>)
            fun onReleasesError(message: String)
        }

        fun releases()
        fun onDestroy()
    }

    interface Interactor {
        fun releases(callback: Presenter.ReleasesCallback)
        fun onDestroy()
    }
}