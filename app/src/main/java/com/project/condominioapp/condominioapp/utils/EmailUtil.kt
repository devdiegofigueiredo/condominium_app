package com.project.condominioapp.condominioapp.utils

import android.text.TextUtils

class EmailUtil {

    companion object {
        fun isValidEmail(email: String): Boolean {
            return if (TextUtils.isEmpty(email)) {
                false
            } else {
                android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
            }
        }
    }
}