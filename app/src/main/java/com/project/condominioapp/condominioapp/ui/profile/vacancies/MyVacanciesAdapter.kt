package com.project.condominioapp.condominioapp.ui.profile.vacancies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

class MyVacanciesAdapter(val vacancies: List<Vacancie>?) : androidx.recyclerview.widget.RecyclerView.Adapter<MyVacanciesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.vacancies_item, parent, false))

    override fun getItemCount(): Int = 10

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)
}