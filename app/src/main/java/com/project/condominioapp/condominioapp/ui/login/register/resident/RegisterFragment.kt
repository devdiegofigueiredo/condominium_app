package com.project.condominioapp.condominioapp.ui.login.register.resident

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.login.LoginActivity
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.TextFormatUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : BaseFragment(), RegisterContract.View {

    private val REQUEST_PHONE_PERMISSION = 1000
    private lateinit var presenter: RegisterPresenter


    companion object {
        @JvmStatic
        fun newInstance(): RegisterFragment {
            return RegisterFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun
            onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.apply {
            loading = ProgressDialogUtil.getDialog(getString(R.string.wait_create_user), this)

            val interactor = RegisterInteractor(this)
            presenter = RegisterPresenter(this@RegisterFragment, interactor)
        }

        phone.addTextChangedListener(TextFormatUtil.phoneMask(phone))
        done.setOnClickListener { createUser() }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun requestPhonePermission() {
        activity?.apply {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), REQUEST_PHONE_PERMISSION)
        }
    }

    override fun requestFocusField(editText: EditText) {
        editText.requestFocus()
    }

    override fun showMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    override fun showPendingAcceptDialog() {
        activity?.apply { showSimpleDialog(getString(R.string.user_pending), onDialogClicked()) }
    }

    override fun showNameInvalidError() {
        name.error = getString(R.string.input_valid_name)
    }

    override fun showEmailInvalidError() {
        email.error = getString(R.string.input_valid_email)
    }

    override fun showPasswordInvalidError() {
        password.error = getString(R.string.input_valid_password)
    }

    override fun showCondominiumIdInvalidError() {
        condominium_id.error = getString(R.string.input_valid_condominium_id)
    }

    override fun showPhoneInvalidError() {
        phone.error = getString(R.string.input_valid_phone)
    }

    override fun showApartmentInvalidError() {
        apartment.error = getString(R.string.input_valid_apartment)
    }

    override fun showBlockInvalidError() {
        block.error = getString(R.string.input_valid_block)
    }

    private fun createUser() {
        if (presenter.isValidateFieldsToCreateUser(name, email, password, condominium_id, phone, apartment, block)) {
            presenter.createUser(name.text.toString(),
                    email.text.toString(),
                    password.text.toString(),
                    condominium_id.text.toString(),
                    phone.text.toString(),
                    apartment.text.toString(),
                    block.text.toString())
        }
    }

    private fun onDialogClicked(): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { _: DialogInterface, _: Int ->
            activity?.apply {
                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }
    }
}