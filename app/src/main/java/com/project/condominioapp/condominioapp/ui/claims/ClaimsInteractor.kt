package com.project.condominioapp.condominioapp.ui.claims

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ClaimsInteractor(val context: Context) : ClaimsContract.Interactor {

    private lateinit var currentUser: User
    private val compositeDisposable = CompositeDisposable()

    override fun claims(page: Int, callback: ClaimsContract.Presenter.ClaimsCallback) {
        getUserInformation(page, callback)
    }

    private fun getUserInformation(page: Int, callback: ClaimsContract.Presenter.ClaimsCallback) {
        val disposable = Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                        getClaims(page, callback)
                    }
                }, {

                })

        compositeDisposable.add(disposable)
    }

    private fun getClaims(page: Int, callback: ClaimsContract.Presenter.ClaimsCallback) {
        val callCreateUser = BaseService.getClaims(currentUser.condominiumId, currentUser.id, page.toString())
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            this.claims?.takeIf { it.isNotEmpty() }?.apply {
                                callback.onClaimsSuccess(this)
                            } ?: kotlin.run {
                                callback.onClaimsEmpty(context.getString(R.string.empty_claims), context.getString(R.string.create_now))
                            }
                        } else {
                            callback.onClaimsError(context.getString(R.string.error_get_claims), context.getString(R.string.try_again))
                        }
                    }
                }, {
                    it.message?.apply { callback.onClaimsError(this, context.getString(R.string.try_again)) }
                })

        compositeDisposable.add(disposable)
    }

    override fun deleteClaim(claimId: String, callback: ClaimsContract.Presenter.DeleteClaimCallback) {
        val request = BaseService.deleteClaim(claimId)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        takeIf { this.statusCode == 200 }?.apply {
                            callback.onDeleteClaimSuccess(claimId)
                        } ?: kotlin.run {
                            callback.onDeleteClaimError(context.getString(R.string.error_delete_claim), context.getString(R.string.try_again))

                        }
                    } ?: kotlin.run {
                        callback.onDeleteClaimError(context.getString(R.string.error_delete_claim), context.getString(R.string.try_again))
                    }
                }, {
                    it.message?.apply { callback.onDeleteClaimError(this, context.getString(R.string.try_again)) }
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}