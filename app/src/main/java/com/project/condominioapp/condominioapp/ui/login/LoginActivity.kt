package com.project.condominioapp.condominioapp.ui.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.home.HomeActivity
import com.project.condominioapp.condominioapp.ui.login.forgot.ForgotPasswordActivity
import com.project.condominioapp.condominioapp.ui.login.register.RegisterActivity
import com.project.condominioapp.condominioapp.utils.EmailUtil
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.login_activity.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class LoginActivity : BaseActivity(), LoginContract.View {

    private lateinit var btLogin: TextView
    private lateinit var btRegister: TextView
    private lateinit var btForgotPassword: TextView
    private lateinit var presenter: LoginPresenter

    private val passwordTransformationMethod = PasswordTransformationMethod()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        btRegister = bt_register
        btRegister.setOnClickListener { startNewActivity<RegisterActivity>() }

        btForgotPassword = bt_forgot_password
        btForgotPassword.setOnClickListener {
            startNewActivity<ForgotPasswordActivity>()
        }

        btLogin = bt_login
        btLogin.setOnClickListener {
            if (isValidatedLoginFields(login.text.toString(), password.text.toString())) {
                presenter.login(login.text.toString(), password.text.toString(), condominium_id.text.toString())
                closeKeyboard()
            }
        }

        val interactor = LoginInteractor(this)
        presenter = LoginPresenter(this, interactor)

        loading = ProgressDialogUtil.getDialog(this)

        password_visibility.setOnTouchListener(View.OnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    password.transformationMethod = null
                    password_visibility.setImageDrawable(ContextCompat.getDrawable(baseContext, R.drawable.ic_visibility_on))
                    return@OnTouchListener true
                }
                MotionEvent.ACTION_UP -> {
                    password_visibility.setImageDrawable(ContextCompat.getDrawable(baseContext, R.drawable.ic_visibility_off))
                    password.transformationMethod = passwordTransformationMethod
                    password.setSelection(password.length())
                    return@OnTouchListener true
                }
            }
            false
        })
    }

    override fun showPendingAcceptDialog() {
        showSimpleDialog(getString(R.string.user_pending_accept))
    }

    override fun startHomeScreen() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    private fun isValidatedLoginFields(email: String, passwordText: String): Boolean {

        if (email.isEmpty()) {
            login.error = getString(R.string.input_email)
            login.requestFocus()
            return false
        }

        if (!EmailUtil.isValidEmail(email)) {
            login.error = getString(R.string.input_valid_email)
            login.requestFocus()
            return false
        }

        if (passwordText.isEmpty()) {
            password.error = getString(R.string.input_password)
            password.requestFocus()
            return false
        }

        if (passwordText.length < 6) {
            password.error = getString(R.string.password_must_six_charracters)
            password.requestFocus()
            return false
        }

        return true
    }

    private fun closeKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private inline fun <reified T> startNewActivity() {
        val intent = Intent(this, T::class.java)
        startActivity(intent)
    }
}