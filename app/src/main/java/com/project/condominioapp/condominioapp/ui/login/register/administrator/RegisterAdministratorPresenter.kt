package com.project.condominioapp.condominioapp.ui.login.register.administrator

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.login.register.entities.CreateCondominiumRequest
import com.project.condominioapp.condominioapp.utils.EmailUtil

class RegisterAdministratorPresenter(val view: RegisterAdministratorContract.View,
                                     val interactor: RegisterAdministratorContract.Interactor) :
        RegisterAdministratorContract.Presenter,
        RegisterAdministratorContract.Presenter.CreateCondominiumCallback {

    override fun createCondominium(condominiumName: EditText,
                                   email: EditText,
                                   phone: EditText,
                                   address: EditText,
                                   state: EditText,
                                   city: EditText,
                                   postalCode: EditText) {
        if (isFieldsValidates(condominiumName, email, phone, address, state, city, postalCode)) {
            val createCondominiumRequest = CreateCondominiumRequest(condominiumName.text.toString(),
                    email.text.toString(),
                    phone.text.toString(),
                    address.text.toString(),
                    state.text.toString(),
                    city.text.toString(),
                    postalCode.text.toString())
            view.showLoading()
            interactor.createCondominium(createCondominiumRequest, this)
        }
    }

    override fun onCreateCondominiumSuccess(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
        view.closeScreen()
    }

    override fun onCreateCondominiumError(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }

    private fun isFieldsValidates(condominiumName: EditText,
                                  email: EditText,
                                  phone: EditText,
                                  address: EditText,
                                  state: EditText,
                                  city: EditText,
                                  postalCode: EditText): Boolean {
        if (condominiumName.text.trim().length <= 1) {
            view.showInvalidCondominiumName()
            view.requestFocusField(condominiumName)
            return false
        }
        if (email.text.trim().length < 3 || !EmailUtil.isValidEmail(email.text.toString())) {
            view.showInvalidEmail()
            view.requestFocusField(email)
            return false
        }

        if (phone.text.trim().length <= 10) {
            view.showInvalidPhone()
            view.requestFocusField(phone)
            return false
        }

        if (address.text.trim().length <= 1) {
            view.showInvalidAddress()
            view.requestFocusField(address)
            return false
        }

        if (state.text.trim().length <= 1) {
            view.showInvalidState()
            view.requestFocusField(state)
            return false
        }

        if (city.text.trim().length <= 1) {
            view.showInvalidCity()
            view.requestFocusField(city)
            return false
        }

        if (postalCode.text.trim().length < 9) {
            view.showInvalidPostalCode()
            view.requestFocusField(postalCode)
            return false
        }

        return true
    }
}