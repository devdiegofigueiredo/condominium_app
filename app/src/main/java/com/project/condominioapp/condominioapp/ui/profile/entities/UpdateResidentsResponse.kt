package com.project.condominioapp.condominioapp.ui.profile.entities

import com.project.condominioapp.condominioapp.model.User

data class UpdateResidentsResponse(val statusCode: Int, val user: User)
