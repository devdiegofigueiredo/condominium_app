package com.project.condominioapp.condominioapp.ui.approvals.claim

import com.project.condominioapp.condominioapp.ui.claims.entities.Claim

class ReplyClaimPresenter(val view: ReplyClaimContract.View,
                          val interactor: ReplyClaimContract.Interactor) :
        ReplyClaimContract.Presenter,
        ReplyClaimContract.Presenter.CallbackClaims,
        ReplyClaimContract.Presenter.CallbackReply,
        ReplyClaimContract.Presenter.CallbackReplySolved {

    override fun onClaimsSuccess(claims: List<Claim>) {
        view.hideLayoutLoading()
        view.addClaims(claims)
    }

    override fun onClaimsError(message: String) {
        view.hideLayoutLoading()
        view.createErrorView(message)
    }

    override fun onReplySuccess(message: String, claimId: String) {
        view.hideLayoutLoading()
        view.removeClaim(claimId)
        view.showErrorMessage(message)
    }

    override fun onClaimsEmpty() {
        view.hideLayoutLoading()
        view.onEmptyClaims()
    }

    override fun onReplyError(message: String) {
        view.hideLayoutLoading()
        view.showErrorMessage(message)
    }

    override fun claimNotAnswered() {
        interactor.claimsUnreplieds(this)
        view.showLayoutLoading()
    }

    override fun replyClaim(claimId: String, reply: String) {
        interactor.replyClaim(claimId, reply, this)
        view.showLayoutLoading()
    }

    override fun onReplySolvedSuccess(message: String, claimId: String) {
        view.hideLayoutLoading()
        view.removeClaim(claimId)
        view.showErrorMessage(message)
    }

    override fun onReplySolvedError(message: String) {
        view.hideLayoutLoading()
        view.showErrorMessage(message)
    }

    override fun replySolvedClaim(claimId: String) {
        view.showLayoutLoading()
        interactor.replySolvedClaim(claimId, this)
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}