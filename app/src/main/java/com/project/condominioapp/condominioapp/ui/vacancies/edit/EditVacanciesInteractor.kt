package com.project.condominioapp.condominioapp.ui.vacancies.edit

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.vacancies.entities.EditVacancieRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EditVacanciesInteractor(val context: Context) : EditVacanciesContract.Interactor {

    private lateinit var currentUser: User

    init {
        getUserInformation()
    }

    override fun editVacancie(vacancieId: String, model: String, plate: String, vacancieNumber: String, callback: EditVacanciesContract.Presenter.EditVacancieCallback) {

        val editVacancieRequest = EditVacancieRequest(vacancieId,
                model,
                plate,
                vacancieNumber)

        val request = BaseService.editVacancie(editVacancieRequest)
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onEditVacancieSuccess(context.getString(R.string.vacancie_created), this.vacancie, currentUser.id)
                        } else {
                            callback.onEditVacancieError(context.getString(R.string.error_create_claim))
                        }
                    }
                }, {
                    it.message?.apply { callback.onEditVacancieError(this) }
                })
    }

    private fun getUserInformation() {
        Observable.fromCallable { }
                .map { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        currentUser = this
                    }
                }, {

                })
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }
}