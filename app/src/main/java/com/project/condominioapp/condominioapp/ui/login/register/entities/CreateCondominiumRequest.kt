package com.project.condominioapp.condominioapp.ui.login.register.entities

data class CreateCondominiumRequest(val name: String,
                                    val email: String,
                                    val phone: String,
                                    val street: String,
                                    val state: String,
                                    val city: String,
                                    val postalCode: String)