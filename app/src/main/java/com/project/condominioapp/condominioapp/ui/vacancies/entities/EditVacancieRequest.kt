package com.project.condominioapp.condominioapp.ui.vacancies.entities

data class EditVacancieRequest(val vacancieId: String,
                               val model: String,
                               val plate: String,
                               val vacancieNumber: String)