package com.project.condominioapp.condominioapp.ui.condominium.entities

import android.os.Parcel
import android.os.Parcelable

data class Space(val _id: String, val name: String, val description: String, val value: Double) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readDouble()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeDouble(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Space> {
        override fun createFromParcel(parcel: Parcel): Space {
            return Space(parcel)
        }

        override fun newArray(size: Int): Array<Space?> {
            return arrayOfNulls(size)
        }
    }


}