package com.project.condominioapp.condominioapp.data.remote

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.project.condominioapp.condominioapp.BuildConfig
import com.project.condominioapp.condominioapp.data.remote.authenticator.TokenAuthenticator
import com.project.condominioapp.condominioapp.model.SimpleResponse
import com.project.condominioapp.condominioapp.ui.approvals.entities.AcceptResidentRequest
import com.project.condominioapp.condominioapp.ui.approvals.entities.PendencyResidentsResponse
import com.project.condominioapp.condominioapp.ui.approvals.entities.ReplyClaimRequest
import com.project.condominioapp.condominioapp.ui.claims.entities.*
import com.project.condominioapp.condominioapp.ui.condominium.entities.*
import com.project.condominioapp.condominioapp.ui.home.entities.UpdateFirebaseTokenRequest
import com.project.condominioapp.condominioapp.ui.login.domain.entities.CreateUserRequest
import com.project.condominioapp.condominioapp.ui.login.domain.entities.LoginResponse
import com.project.condominioapp.condominioapp.ui.login.register.entities.CreateCondominiumRequest
import com.project.condominioapp.condominioapp.ui.profile.entities.UpdateResidentsRequest
import com.project.condominioapp.condominioapp.ui.profile.entities.UpdateResidentsResponse
import com.project.condominioapp.condominioapp.ui.profile.entities.UpdateUserRequest
import com.project.condominioapp.condominioapp.ui.releases.entities.CreateReleaseRequest
import com.project.condominioapp.condominioapp.ui.releases.entities.ReleasesResponse
import com.project.condominioapp.condominioapp.ui.vacancies.entities.*
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class BaseService {

    companion object {

        private fun getRetrofitInstance(baseUrl: String): Retrofit {
            val httpClient = OkHttpClient.Builder()
            httpClient.retryOnConnectionFailure(true)
            httpClient.addNetworkInterceptor(TokenAuthenticator())
            return Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build()
        }

        private fun instance(baseUrl: String): Retrofit {
            val instance: Retrofit by lazy { getRetrofitInstance(baseUrl) }
            return instance
        }

        fun getUserInfo(firebaseId: String, condominiumId: String): Observable<LoginResponse> {
            return instance(BuildConfig.base_url).create(UserService::class.java).getUserInfo(firebaseId, condominiumId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun createUser(user: CreateUserRequest): Observable<LoginResponse> {
            return instance(BuildConfig.base_url).create(UserService::class.java).createUser(user).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun createClaim(user: CreateClaimRequest): Observable<CreateClaimResponse> {
            return instance(BuildConfig.base_url).create(ClaimService::class.java).createClaim(user).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun getClaims(condominiumId: String, userId: String, page: String): Observable<ClaimsResponse> {
            return instance(BuildConfig.base_url).create(ClaimService::class.java).getClaims(condominiumId, userId, page).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun createVacancie(vacancieRequest: CreateVacancieRequest): Observable<CreateVacancieResponse> {
            return instance(BuildConfig.base_url).create(VacancieService::class.java).createVacancie(vacancieRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun getVacancies(condominiumId: String): Observable<VacanciesResponse> {
            return instance(BuildConfig.base_url).create(VacancieService::class.java).getVacancies(condominiumId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun editVacancie(editVacancieRequest: EditVacancieRequest): Observable<EditVacancieResponse> {
            return instance(BuildConfig.base_url).create(VacancieService::class.java).editVacancie(editVacancieRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun deleteVacancie(condominiumId: String): Observable<DeleteVacancieResponse> {
            return instance(BuildConfig.base_url).create(VacancieService::class.java).deleteVacancie(condominiumId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun deleteClaim(claimId: String): Observable<DeleteClaimResponse> {
            return instance(BuildConfig.base_url).create(ClaimService::class.java).deleteClaim(claimId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun editClaim(editClaimRequest: EditClaimRequest): Observable<EditClaimResponse> {
            return instance(BuildConfig.base_url).create(ClaimService::class.java).editClaim(editClaimRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun updateResidents(updateResidentsRequest: UpdateResidentsRequest): Observable<UpdateResidentsResponse> {
            return instance(BuildConfig.base_url).create(UserService::class.java).updateResidents(updateResidentsRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun updateUser(updateUserRequest: UpdateUserRequest): Observable<LoginResponse> {
            return instance(BuildConfig.base_url).create(UserService::class.java).updateUser(updateUserRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun searchVacancies(condominiumId: String, vacancieNumber: String): Observable<VacanciesResponse> {
            return instance(BuildConfig.base_url).create(VacancieService::class.java).searchVacancies(condominiumId, vacancieNumber).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun getReleases(condominiumId: String, userId: String): Observable<ReleasesResponse> {
            return instance(BuildConfig.base_url).create(ReleaseService::class.java).getReleases(condominiumId, userId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun createReleases(body: CreateReleaseRequest): Observable<SimpleResponse> {
            return instance(BuildConfig.base_url).create(ReleaseService::class.java).createRelease(body).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun createCondominium(body: CreateCondominiumRequest): Observable<SimpleResponse> {
            return instance(BuildConfig.base_url).create(CondominiumService::class.java).createCondominium(body).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun pendecyResidents(condominiumId: String): Observable<PendencyResidentsResponse> {
            return instance(BuildConfig.base_url).create(UserService::class.java).pendencyResident(condominiumId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun updateFirebaseToken(updateFirebaseTokenRequest: UpdateFirebaseTokenRequest): Observable<SimpleResponse> {
            return instance(BuildConfig.base_url).create(UserService::class.java).updateFirebaseToken(updateFirebaseTokenRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun claimsUnreplieds(condominiumId: String): Observable<ClaimsResponse> {
            return instance(BuildConfig.base_url).create(ClaimService::class.java).claimsUnreplieds(condominiumId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun replyClaim(replyClaimRequest: ReplyClaimRequest): Observable<SimpleResponse> {
            return instance(BuildConfig.base_url).create(ClaimService::class.java).replyClaim(replyClaimRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun replySolvedClaim(replyClaimRequest: ReplyClaimRequest): Observable<SimpleResponse> {
            return instance(BuildConfig.base_url).create(ClaimService::class.java).replySolvedClaim(replyClaimRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun acceptResident(acceptResidentRequest: AcceptResidentRequest): Observable<SimpleResponse> {
            return instance(BuildConfig.base_url).create(UserService::class.java).acceptResident(acceptResidentRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun deleteResident(userId: String, condominiumId: String, firebaseId: String): Observable<SimpleResponse> {
            return instance(BuildConfig.base_url).create(UserService::class.java).deleteResident(userId, condominiumId, firebaseId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun condominium(condominiumId: String): Observable<CondominiumResponse> {
            return instance(BuildConfig.base_url).create(CondominiumService::class.java).condominium(condominiumId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun updateCondominium(editCondominiumRequest: EditCondominiumRequest): Observable<CondominiumResponse> {
            return instance(BuildConfig.base_url).create(CondominiumService::class.java).updateCondominium(editCondominiumRequest).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun createSpace(body: CreateSpaceRequest): Observable<CreateSpaceResponse> {
            return instance(BuildConfig.base_url).create(CondominiumService::class.java).createSpace(body).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun deleteSpace(spaceId: String, condominiumId: String): Observable<SimpleResponse> {
            return instance(BuildConfig.base_url).create(CondominiumService::class.java).deleteSpace(spaceId, condominiumId).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }

        fun editSpace(body: EditSpaceRequest): Observable<EditSpaceResponse> {
            return instance(BuildConfig.base_url).create(CondominiumService::class.java).editSpace(body).flatMap { response ->
                Observable.fromCallable { response }
            }.map { response -> response }
        }
    }
}