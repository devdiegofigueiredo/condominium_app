package com.project.condominioapp.condominioapp.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.project.condominioapp.condominioapp.ui.profile.entities.Resident

@Entity
data class User(@SerializedName("_id") @PrimaryKey val id: String,
                val firebaseId: String,
                val name: String,
                val email: String,
                val phone: String,
                val condominiumId: String,
                val apartment: String,
                val block: String,
                val isPendency: Boolean,
                val isAdmin: Boolean) {

    @Ignore
    var residents: List<Resident>? = null
}