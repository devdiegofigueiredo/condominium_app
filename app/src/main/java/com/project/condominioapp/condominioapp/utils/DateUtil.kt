package com.project.condominioapp.condominioapp.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateUtil {

    companion object {

        fun getCurrentStringDate(): String {
            return getDateByCalendar(Calendar.getInstance())
        }

        private fun addZeroIfNecessary(value: Int): String {
            return if (value < 10) {
                "0$value"
            } else {
                value.toString()
            }
        }

        private fun getDateByCalendar(calendar: Calendar): String {
            return addZeroIfNecessary(calendar.get(Calendar.DAY_OF_MONTH)) +
                    "/" +
                    addZeroIfNecessary(calendar.get(Calendar.MONTH) + 1) +
                    "/" +
                    calendar.get(Calendar.YEAR)
        }

        fun convertStringToDate(dateString: String): String {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale("pt", "BR"))

            try {
                val date = dateFormat.parse(dateString)
                val calendar = Calendar.getInstance()
                calendar.time = date
                return getDateByCalendar(calendar)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return dateString
        }
    }
}