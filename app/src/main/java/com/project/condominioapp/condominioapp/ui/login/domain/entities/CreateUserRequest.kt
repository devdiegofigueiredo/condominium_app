package com.project.condominioapp.condominioapp.ui.login.domain.entities

data class CreateUserRequest(val name: String,
                             val email: String,
                             val phone: String,
                             val condominiumId: String,
                             val apartment: String,
                             val block: String,
                             val password: String)