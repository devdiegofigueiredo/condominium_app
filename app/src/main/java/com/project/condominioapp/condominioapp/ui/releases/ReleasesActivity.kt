package com.project.condominioapp.condominioapp.ui.releases

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.releases.entities.Release
import com.project.condominioapp.condominioapp.ui.releases.new.CreateReleaseActivity
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_releases.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class ReleasesActivity : BaseActivity(), ReleasesContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_releases)
        setupToolbar(getString(R.string.my_releases), true)

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_search_releases), this)

        floating_action_button.setOnClickListener { startActivity(Intent(this, CreateReleaseActivity::class.java)) }

        val interactor = ReleasesInteractor(this)
        val presenter = ReleasesPresenter(this, interactor)
        presenter.releases()
    }

    override fun setupReleases(releases: List<Release>) {
        val adapter = ReleasesAdapter(releases)
        releases_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        releases_list.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }
}