package com.project.condominioapp.condominioapp.ui.condominium.spaces

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.remote.BaseService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SpacesInteractor(val context: Context) : SpacesContract.Interactor {

    private val compositeDisposable = CompositeDisposable()

    override fun deleteSpace(spaceId: String, condominiumId: String, callback: SpacesContract.Presenter.DeleteSpaceCallback) {
        val callCreateUser = BaseService.deleteSpace(spaceId, condominiumId)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onDeleteSuccess(context.getString(R.string.delete_space_success), spaceId)
                        } else {
                            callback.onDeleteError(context.getString(R.string.error_delete_condominium))
                        }
                    }
                }, {
                    it.message?.apply { callback.onDeleteError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}