package com.project.condominioapp.condominioapp.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.text.NumberFormat

class TextFormatUtil {

    companion object {
        var phoneMask = "(##)####-####"
        var celMask = "(##)#####-####"
        var isUpdating = false
        var oldValue = ""

        fun phoneMask(editText: EditText) = object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                val value = s.toString().replace(Regex("""[()-]"""), "")
                var maskAux = ""
                if (isUpdating) {
                    oldValue = value
                    isUpdating = false
                    return
                }

                val mask = if (value.length > 10) {
                    celMask
                } else {
                    phoneMask
                }

                var i = 0
                for (m in mask.toCharArray()) {
                    if (m != '#' && value.length > oldValue.length || m != '#' && value.length < oldValue.length && value.length !== i) {
                        maskAux += m
                        continue
                    }
                    try {
                        maskAux += value.get(i)
                    } catch (e: Exception) {
                        break
                    }
                    i++
                }

                isUpdating = true
                editText.setText(maskAux)
                editText.setSelection(maskAux.length)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        fun addMask(format: String, editText: EditText) = object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                val value = s.toString().replace(Regex("""[()-]"""), "")
                var maskAux = ""
                if (isUpdating) {
                    oldValue = value
                    isUpdating = false
                    return
                }

                var i = 0
                for (m in format.toCharArray()) {
                    if (m != '#' && value.length > oldValue.length || m != '#' && value.length < oldValue.length && value.length !== i) {
                        maskAux += m
                        continue
                    }
                    try {
                        maskAux += value.get(i)
                    } catch (e: Exception) {
                        break
                    }
                    i++
                }

                isUpdating = true
                editText.setText(maskAux)
                editText.setSelection(maskAux.length)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        fun addTextViewMask(textToFormat: String, mask: String): String {
            var formattedText = ""
            val cleanedText = textToFormat.replace("[()-]".toRegex(), "")

            var i = 0

            for (m in mask.toCharArray()) {
                if (m != '#') {
                    formattedText += m
                    continue
                }

                try {
                    formattedText += cleanedText[i]
                } catch (e: Exception) {
                    break
                }

                i++
            }
            return formattedText
        }

        fun addPlateMask(text: String): String {
            val mask = if (text.length > 7) {
                "########"
            } else {
                "###-####"
            }

            var formattedText = ""
            var i = 0

            for (m in mask.toCharArray()) {
                if (m != '#') {
                    formattedText += m
                    continue
                }

                try {
                    formattedText += text[i]
                } catch (e: Exception) {
                    break
                }

                i++
            }
            return formattedText
        }

        fun moneyMask(editText: EditText) = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence?, start: Int, before: Int, count: Int) {
                editText.removeTextChangedListener(this)

                val cleanString = charSequence.toString().clearMoneyFormat().trim()

                val formatted = NumberFormat.getCurrencyInstance().format((cleanString.toDouble() / 100))

                editText.setText(formatted)
                editText.setSelection(formatted.length)

                editText.addTextChangedListener(this)
            }
        }
    }
}