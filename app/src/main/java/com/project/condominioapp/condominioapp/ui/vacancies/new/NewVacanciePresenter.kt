package com.project.condominioapp.condominioapp.ui.vacancies.new

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

class NewVacanciePresenter(val view: NewVacancieContract.View,
                           val interactor: NewVacancieContract.Interactor) :
        NewVacancieContract.Presenter,
        NewVacancieContract.Presenter.CreateVacancieCallback {

    override fun createVacancie(model: EditText,
                                plate: EditText,
                                vacancieNumber: EditText) {
        if (isFieldsValidateds(model, plate, vacancieNumber)) {
            view.showLoading()
            interactor.createVacancie(model.text.toString(),
                    plate.text.toString(),
                    vacancieNumber.text.toString(),
                    this)
        }
    }

    override fun onCreateVacancieSuccess(message: String, vacancie: Vacancie, currentUserId: String) {
        view.hideLoading()
        view.showMessage(message)
        view.closeScreen(vacancie, currentUserId)
    }

    override fun onCreateVacancieError(message: String) {
        view.hideLoading()
        view.showMessage(message)
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }

    private fun isFieldsValidateds(model: EditText,
                                   plate: EditText,
                                   vacancieNumber: EditText): Boolean {

        model.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(model)
            return false
        }

        plate.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(plate)
            return false
        }

        vacancieNumber.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(vacancieNumber)
            return false
        }

        return true
    }
}