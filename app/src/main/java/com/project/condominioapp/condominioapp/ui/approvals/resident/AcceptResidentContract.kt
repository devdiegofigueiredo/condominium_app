package com.project.condominioapp.condominioapp.ui.approvals.resident

import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface AcceptResidentContract {

    interface View : BaseView {
        fun addResidents(residents: List<User>)
        fun emptyResidents()
        fun showLayoutLoading()
        fun hideLayoutLoading()
        fun removeResidentFromList(residentId: String)
        fun removeUserFromList(userId: String)
        fun createErrorView(message: String)
    }

    interface Presenter {
        interface AcceptCallback {
            fun onAcceptSuccess(message: String, residentId: String)
            fun onAcceptError(message: String)
        }

        interface ResidentsCallback {
            fun onResidentsSuccess(residents: List<User>)
            fun onResidentsEmpty()
            fun onResidentsError(message: String)
        }

        interface DeleteCallback {
            fun onDeleteSuccess(message: String, userId: String)
            fun onDeleteError(message: String)
        }

        fun acceptResident(userId: String)
        fun deleteResident(userId: String, firebaseId: String)
        fun pendencyResidents()
        fun onDestroy()
    }

    interface Interactor {
        fun acceptResident(userId: String, callback: Presenter.AcceptCallback)
        fun deleteUser(userId: String, firebaseId: String, callback: Presenter.DeleteCallback)
        fun pendencyResidents(callback: Presenter.ResidentsCallback)
        fun onDestroy()
    }
}