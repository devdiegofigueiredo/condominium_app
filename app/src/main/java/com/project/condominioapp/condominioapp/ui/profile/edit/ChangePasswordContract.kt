package com.project.condominioapp.condominioapp.ui.profile.edit

import android.widget.EditText

interface ChangePasswordContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun closeScreen()
        fun showMessage(message: String)
        fun showErrorField(field: EditText)
        fun showIncorrectSizeMessage(field: EditText)
        fun showWrongPasswordMessage()
    }

    interface Presenter {

        interface PasswordCallback {
            fun onPasswordChangeSuccess(message: String)
            fun onPasswordChangeError(message: String)
        }

        fun changePassword(currentPassword: EditText, newPassword: EditText, confirmPassword: EditText)
    }

    interface Interactor {
        fun updatePassword(currentPassword: String, newPassword: String, callback: Presenter.PasswordCallback)
    }
}