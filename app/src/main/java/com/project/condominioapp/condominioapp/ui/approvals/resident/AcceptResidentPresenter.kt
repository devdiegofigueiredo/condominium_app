package com.project.condominioapp.condominioapp.ui.approvals.resident

import com.project.condominioapp.condominioapp.model.User

class AcceptResidentPresenter(val view: AcceptResidentContract.View,
                              val interactor: AcceptResidentContract.Interactor) :
        AcceptResidentContract.Presenter,
        AcceptResidentContract.Presenter.AcceptCallback,
        AcceptResidentContract.Presenter.ResidentsCallback,
        AcceptResidentContract.Presenter.DeleteCallback {

    override fun pendencyResidents() {
        view.showLayoutLoading()
        interactor.pendencyResidents(this)
    }

    override fun onResidentsSuccess(residents: List<User>) {
        view.addResidents(residents)
        view.hideLayoutLoading()
    }

    override fun onResidentsEmpty() {
        view.emptyResidents()
    }

    override fun onResidentsError(message: String) {
        view.hideLayoutLoading()
        view.createErrorView(message)
    }

    override fun acceptResident(userId: String) {
        interactor.acceptResident(userId, this)
    }

    override fun onAcceptSuccess(message: String, residentId: String) {
        view.showErrorMessage(message)
        view.removeResidentFromList(residentId)
        view.hideLoading()
    }

    override fun onAcceptError(message: String) {
        view.showErrorMessage(message)
        view.hideLoading()
    }

    override fun deleteResident(userId: String, firebaseId: String) {
        view.showLoading()
        interactor.deleteUser(userId, firebaseId, this)
    }

    override fun onDeleteSuccess(message: String, userId: String) {
        view.showErrorMessage(message)
        view.removeUserFromList(userId)
        view.hideLoading()
    }

    override fun onDeleteError(message: String) {
        view.showErrorMessage(message)
        view.hideLoading()
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }
}