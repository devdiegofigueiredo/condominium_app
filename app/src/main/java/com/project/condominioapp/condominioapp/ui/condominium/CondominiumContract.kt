package com.project.condominioapp.condominioapp.ui.condominium

import com.project.condominioapp.condominioapp.ui.condominium.entities.Condominium
import com.project.condominioapp.condominioapp.utils.ui.BaseView

interface CondominiumContract {

    interface View : BaseView {
        fun setupCondominium(condominium: Condominium)
    }

    interface Presenter {
        interface CondominiumCallback {
            fun onCondominiumSuccess(condominium: Condominium)
            fun onCondominiumError(message: String)
        }

        fun condominium()
        fun onDestroy()

    }

    interface Interactor {
        fun condominium(callback: Presenter.CondominiumCallback)
        fun onDestroy()
    }

}