package com.project.condominioapp.condominioapp.ui.claims

import android.content.Intent
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.claims.detail.DetailClaimActivity
import com.project.condominioapp.condominioapp.ui.claims.edit.EditClaimsActivity
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim
import com.project.condominioapp.condominioapp.ui.claims.new.NewClaimActivity
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import com.project.condominioapp.condominioapp.utils.ui.InfoFragment
import kotlinx.android.synthetic.main.activity_claims.*
import kotlinx.android.synthetic.main.toolbar.*

class ClaimsActivity : BaseActivity(), ClaimsContract.View {

    companion object {
        const val EXTRA_CLAIM = "extra_claim"
    }

    private val EDIT_CLAIM_REQUEST_CODE = 1000
    private val NEW_CLAIM_REQUEST_CODE = 1001

    private var isEmptyVacancies = false

    private lateinit var presenter: ClaimsPresenter
    private lateinit var deleteClaimDialog: AlertDialog
    private lateinit var adapter: ClaimsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_claims)

        setupToolbar()

        loading = ProgressDialogUtil.getDialog(getString(R.string.fetching_claims), this)

        val interactor = ClaimsInteractor(this)
        presenter = ClaimsPresenter(this, interactor)

        setupClaims()

        floating_action_button.setOnClickListener { onFloatingActionButtonClicked() }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun updateGetClaimsLoadingMessage() {
        loading = ProgressDialogUtil.getDialog(getString(R.string.fetching_claims), this)
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        takeIf { resultCode == RESULT_OK }?.apply {
            when (requestCode) {
                NEW_CLAIM_REQUEST_CODE -> {
                    isEmptyVacancies = false

                    val jsonClaim = data?.extras?.getString(EXTRA_CLAIM)
                    val gson = Gson()
                    val claim = gson.fromJson(jsonClaim, Claim::class.java)
                    adapter.addClaim(claim)

                    claims_list.visibility = View.VISIBLE
                }

                EDIT_CLAIM_REQUEST_CODE -> {
                    val jsonClaim = data?.extras?.getString(EXTRA_CLAIM)
                    val gson = Gson()
                    val claim = gson.fromJson(jsonClaim, Claim::class.java)
                    adapter.updateClaim(claim)
                }
            }
        } ?: kotlin.run {
            takeIf { isEmptyVacancies }?.apply {
                showEmptyView(getString(R.string.empty_claims), getString(R.string.create_now))
            }
        }
    }

    override fun updateDeleteLoadingMessage() {
        loading = ProgressDialogUtil.getDialog(getString(R.string.deleting_claim), this)
    }

    private fun onFloatingActionButtonClicked() {
        startActivityForResult(Intent(this, NewClaimActivity::class.java), NEW_CLAIM_REQUEST_CODE)
    }

    private fun setupClaims() {
        claims_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        adapter = ClaimsAdapter(this::onClaimSelected,
                this::onEditClaimClicked,
                this::showDeleteClaimDialog,
                this::onClaimsEmpty,
                this::onLoadMore)

        claims_list.adapter = adapter
        claims_list.addOnScrollListener(onScrollListener)
    }

    override fun addClaims(claims: List<Claim>) {
        adapter.addClaims(claims)
    }

    override fun showErrorView(message: String, textButton: String) {
        claims_list.visibility = View.GONE

        val errorFragment = InfoFragment(message, textButton, this::onInfoButtonClicked)
        supportFragmentManager.beginTransaction().replace(R.id.error_container, errorFragment).commitAllowingStateLoss()
    }

    override fun showEmptyView(message: String, textButton: String) {
        isEmptyVacancies = true
        claims_list.visibility = View.GONE

        val errorFragment = InfoFragment(message, textButton, this::onCreateNowClicked)
        supportFragmentManager.beginTransaction().replace(R.id.error_container, errorFragment).commitAllowingStateLoss()
    }

    override fun removeClaim(claimId: String) {
        adapter.removeClaim(claimId)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        generic_toolbar.title = getString(R.string.claims)
        setSupportActionBar(generic_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private val onScrollListener = object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (dy > 0 && floating_action_button.visibility == View.VISIBLE) {
                floating_action_button.hide()
            } else if (dy < 0 && floating_action_button.visibility != View.VISIBLE) {
                floating_action_button.show()
            }
        }
    }

    private fun onClaimSelected(claim: Claim) {
        val gson = Gson()
        val textJson = gson.toJson(claim)

        val intent = Intent(this, DetailClaimActivity::class.java)
        intent.putExtra(DetailClaimActivity.EXTRA_CLAIM, textJson)
        startActivity(intent)
    }

    private fun onInfoButtonClicked() {
        claims_list.visibility = View.VISIBLE
        presenter.claims(0)
    }

    private fun onEditClaimClicked(claimId: String, title: String, message: String) {
        val intent = Intent(this, EditClaimsActivity::class.java)
        intent.putExtra(EditClaimsActivity.EXTRA_CLAIM_ID, claimId)
        intent.putExtra(EditClaimsActivity.EXTRA_TITLE, title)
        intent.putExtra(EditClaimsActivity.EXTRA_MESSAGE, message)
        startActivityForResult(intent, EDIT_CLAIM_REQUEST_CODE)
    }

    private fun showDeleteClaimDialog(claimId: String) {
        val contextWrapper = ContextThemeWrapper(this, R.style.AlertDialogTheme)
        val dialog = AlertDialog.Builder(contextWrapper)
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.delete_dialog, null, false)

        val labelMessasge = view.findViewById<TextView>(R.id.message)
        labelMessasge.text = "Deseja deletar essa vaga?"

        deleteClaimDialog = dialog.create()
        deleteClaimDialog.setView(view)
        deleteClaimDialog.setCancelable(true)
        deleteClaimDialog.show()

        val yes = view.findViewById<TextView>(R.id.yes)
        yes.setOnClickListener {
            deleteClaimDialog.dismiss()
            presenter.deleteClaim(claimId)
        }

        val no = view.findViewById<TextView>(R.id.no)
        no.setOnClickListener { deleteClaimDialog.dismiss() }
    }

    private fun onCreateNowClicked() {
        startActivityForResult(Intent(this, NewClaimActivity::class.java), NEW_CLAIM_REQUEST_CODE)
    }

    private fun onClaimsEmpty() {
        isEmptyVacancies = true
        showEmptyView(getString(R.string.empty_claims), getString(R.string.create_now))
    }

    private fun onLoadMore(page: Int) {
        presenter.claims(page)
    }
}