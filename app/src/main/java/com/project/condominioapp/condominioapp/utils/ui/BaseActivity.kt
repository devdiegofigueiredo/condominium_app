package com.project.condominioapp.condominioapp.utils.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.project.condominioapp.condominioapp.R
import kotlinx.android.synthetic.main.done.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlin.reflect.KFunction0

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    lateinit var loading: AlertDialog

    fun showLoading() {
        loading.takeIf { !it.isShowing }.apply { loading.show() }
    }

    fun hideLoading() {
        loading.takeIf { it.isShowing }.apply { loading.hide() }
    }

    fun showDoneLoading() {
        done.isEnabled = false
        progress.visibility = View.VISIBLE
        done_label.visibility = View.GONE
    }

    fun hideDoneLoading() {
        done.isEnabled = true
        progress.visibility = View.GONE
        done_label.visibility = View.VISIBLE
    }

    fun showErrorMessage(title: String) {
        Toast.makeText(this, title, Toast.LENGTH_LONG).show()
    }

    fun setupToolbar(title: String, hasBackButton: Boolean) {
        generic_toolbar.title = title
        setSupportActionBar(generic_toolbar)

        takeIf { hasBackButton }?.apply { supportActionBar?.setDisplayHomeAsUpEnabled(true) }
    }

    fun showErrorView(message: String, onInfoButtonClicked: KFunction0<Unit>) {
        val errorFragment = InfoFragment(message, "", onInfoButtonClicked)
        supportFragmentManager?.beginTransaction()?.replace(R.id.error_container, errorFragment)?.commitAllowingStateLoss()
    }

    fun showSimpleDialog(message: String) {
        showSimpleDialog(message, null)
    }

    fun showSimpleDialog(message: String, listener: DialogInterface.OnClickListener?) {
        val dialog = AlertDialog.Builder(this@BaseActivity, R.style.AlertDialogTheme)

        dialog.setTitle("Atenção!")
        dialog.setMessage(message)

        dialog.setNegativeButton(getString(R.string.ok), listener)

        val alertDialog = dialog.create()
        alertDialog.setCancelable(true)
        alertDialog.show()
    }

    fun showKeyboard() {
        val inputMethodManager = baseContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun hideKeyboard() {
        val inputMethodManager = baseContext.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(window.decorView.rootView.windowToken, 0)
    }
}