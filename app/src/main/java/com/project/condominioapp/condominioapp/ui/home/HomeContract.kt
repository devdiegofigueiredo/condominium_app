package com.project.condominioapp.condominioapp.ui.home

import com.project.condominioapp.condominioapp.model.User

interface HomeContract {

    interface View {
        fun setupAdminFunctions()
    }

    interface Presenter {
        interface UserCallback {
            fun onUserSuccess(user: User)
        }

        fun currentUser()
        fun onDestroy()
    }

    interface Interactor {

        fun onDestroy()
        fun currentUser(callback: Presenter.UserCallback)
    }
}