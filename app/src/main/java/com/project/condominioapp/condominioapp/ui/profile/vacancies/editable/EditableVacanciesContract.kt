package com.project.condominioapp.condominioapp.ui.profile.vacancies.editable

import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

interface EditableVacanciesContract {

    interface View {
        fun showVacancieIncompletedMessage()
    }

    interface Presenter {
        interface VacanciesCallback {
            fun onVacanciesSuccess()
            fun onVacanciesError()
        }

        fun saveVacancies(vacancies: List<Vacancie>)
    }

    interface Interactor {
        fun saveVacancies(vacancies: List<Vacancie>)
    }
}