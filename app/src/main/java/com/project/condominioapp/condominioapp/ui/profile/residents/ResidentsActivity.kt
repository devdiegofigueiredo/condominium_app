package com.project.condominioapp.condominioapp.ui.profile.residents

import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.profile.entities.Resident
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_residents.*

@Suppress("DIFFERENT_NAMES_FOR_THE_SAME_PARAMETER_IN_SUPERTYPES")
class ResidentsActivity : BaseActivity(), ResidentsContract.View {

    private lateinit var presenter: ResidentsPresenter
    private lateinit var adapter: ResidentsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_residents)

        loading = ProgressDialogUtil.getDialog(getString(R.string.fetch_residents), this)

        setupToolbar(getString(R.string.residents), true)
        setupResidentsList()

        val interactor = ResidentsInteractor(this)
        presenter = ResidentsPresenter(this, interactor)

        floating_action_button.setOnClickListener { showAddResidentDialog() }

        done.setOnClickListener {
            adapter.residents.takeIf { it.isNotEmpty() }?.apply {
                presenter.updateResidents(adapter.residents)
            } ?: kotlin.run {
                showSaveEmptyDialog()
            }
        }
    }

    private fun showAddResidentDialog() {
        val contextWrapper = ContextThemeWrapper(this, R.style.AlertDialogTheme)
        val dialog = AlertDialog.Builder(contextWrapper)
        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.add_resident_dialog, null, false)

        val alertDialog = dialog.create()
        alertDialog.setView(view)
        alertDialog.setCancelable(true)
        alertDialog.show()

        val name = view.findViewById<EditText>(R.id.name)

        val dialogDone = view.findViewById<TextView>(R.id.done)
        dialogDone.setOnClickListener {
            name.text.toString().takeIf { it.isEmpty() }?.apply {
                name.error = (getString(R.string.input_valid_name))
                return@setOnClickListener
            }

            val resident = Resident(name.text.toString())
            adapter.addResident(resident)

            anyone_added.takeIf { it.visibility == View.VISIBLE }.apply {
                anyone_added.visibility = View.GONE
            }

            alertDialog.dismiss()
        }
    }

    private fun setupResidentsList() {
        adapter = ResidentsAdapter(this::showEmptyResidentMessage)

        residents_list.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        residents_list.adapter = adapter
    }

    private fun showEmptyResidentMessage() {
        anyone_added.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun setupResidents(residents: List<Resident>) {
        residents.takeIf { it.isEmpty() }?.apply {
            showEmptyResidentMessage()
        }

        adapter.addResidents(residents)
    }

    override fun changeLoadingMessageToUpdate() {
        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_update_residents), this)
    }

    override fun closeScreen() {
        finish()
    }

    private fun showSaveEmptyDialog() {
        val dialog = AlertDialog.Builder(this, R.style.AlertDialogTheme)

        dialog.setTitle("Alerta!")
        dialog.setMessage("Se você salvar uma lista vazia, poderá excluir os moradores adicionados anteriormente! Deseja prosseguir?")

        dialog.setPositiveButton(getString(R.string.yes)) { _, _ -> presenter.updateResidents(arrayListOf()) }
        dialog.setNegativeButton(getString(R.string.no), null)

        val alertDialog = dialog.create()
        alertDialog.setCancelable(true)
        alertDialog.show()
    }
}