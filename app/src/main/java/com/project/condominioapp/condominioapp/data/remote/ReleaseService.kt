package com.project.condominioapp.condominioapp.data.remote

import com.project.condominioapp.condominioapp.data.datautil.Endpoints
import com.project.condominioapp.condominioapp.model.SimpleResponse
import com.project.condominioapp.condominioapp.ui.releases.entities.CreateReleaseRequest
import com.project.condominioapp.condominioapp.ui.releases.entities.ReleasesResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Query

interface ReleaseService {

    @GET(Endpoints.searchReleases)
    fun getReleases(@Query(value = "condominiumId") condominiumId: String,
                    @Query(value = "ownerId") ownerId: String): Observable<ReleasesResponse>

    @GET(Endpoints.searchReleases)
    fun createRelease(@Body body: CreateReleaseRequest): Observable<SimpleResponse>

}