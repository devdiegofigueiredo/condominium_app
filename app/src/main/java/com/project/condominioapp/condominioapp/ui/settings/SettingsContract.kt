package com.project.condominioapp.condominioapp.ui.settings

interface SettingsContract {

    interface View {
        fun restartApp()
    }

    interface Presenter {
        interface LoggoutCallback {
            fun onLoggoutSuccess()
        }

        fun loggoutApp()
        fun onDestroy()
    }

    interface Interactor {
        fun loggoutApp(callback: Presenter.LoggoutCallback)
        fun onDestroy()
    }
}