package com.project.condominioapp.condominioapp.ui.approvals

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.tabs.TabLayout
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.approvals.claim.ReplyClaimFragment
import com.project.condominioapp.condominioapp.ui.approvals.resident.AcceptResidentFragment
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity

private val TITLES = arrayOf(
        R.string.residents,
        R.string.claims
)

class ApprovalsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_approvals)

        setupToolbar(getString(R.string.approvals), true)

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: androidx.viewpager.widget.ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter

        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    class SectionsPagerAdapter(private val context: Context, fm: androidx.fragment.app.FragmentManager)
        : androidx.fragment.app.FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            when (position) {
                0 -> {
                    return AcceptResidentFragment.newInstance()
                }

                1 -> {
                    return ReplyClaimFragment.newInstance()
                }
            }

            return AcceptResidentFragment.newInstance()
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return context.resources.getString(TITLES[position])
        }

        override fun getCount(): Int {
            return TITLES.size
        }
    }
}