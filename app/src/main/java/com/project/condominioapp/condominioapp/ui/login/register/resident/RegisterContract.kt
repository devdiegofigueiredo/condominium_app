package com.project.condominioapp.condominioapp.ui.login.register.resident

import android.widget.EditText

interface RegisterContract {

    interface View {
        fun requestPhonePermission()
        fun requestFocusField(editText: EditText)
        fun showLoading()
        fun hideLoading()
        fun showMessage(message: String)
        fun showPendingAcceptDialog()
        fun showNameInvalidError()
        fun showEmailInvalidError()
        fun showPasswordInvalidError()
        fun showCondominiumIdInvalidError()
        fun showPhoneInvalidError()
        fun showApartmentInvalidError()
        fun showBlockInvalidError()
    }

    interface Presenter {
        interface CreateUserCallback {
            fun onCreateUserSuccess()
            fun onCreateUserError(message: String)
        }

        fun createUser(name: String,
                       email: String,
                       password: String,
                       condominiumId: String,
                       phone: String,
                       apartment: String,
                       block: String)

        fun isValidateFieldsToCreateUser(nameField: EditText,
                                         emailField: EditText,
                                         passwordField: EditText,
                                         condominiumId: EditText,
                                         phone: EditText,
                                         apartment: EditText,
                                         block: EditText): Boolean

        fun onDestroy()
    }

    interface Interactor {
        fun createUser(name: String, email: String, password: String, phone: String, condominiumId: String, apartment: String, block: String, callback: Presenter.CreateUserCallback)
        fun isValidEmail(email: String): Boolean
        fun onDestroy()
    }
}