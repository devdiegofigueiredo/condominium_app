package com.project.condominioapp.condominioapp.ui.settings

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.local.repository.CurrentUser
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.home.entities.UpdateFirebaseTokenRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SettingsInteractor(val context: Context) : SettingsContract.Interactor, CurrentUser.UserCallback {

    private lateinit var user: User
    private val compositeDisposable = CompositeDisposable()

    init {
        CurrentUser.user(this, context)
    }

    override fun onUserSuccess(user: User) {
        this.user = user
    }

    override fun loggoutApp(callback: SettingsContract.Presenter.LoggoutCallback) {
        deleteFirebaseToken()

        val dispose = Observable.fromCallable { loggoutAndClearCache() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { callback.onLoggoutSuccess() }

        compositeDisposable.add(dispose)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun deleteFirebaseToken() {
        val callCreateUser = BaseService.updateFirebaseToken(UpdateFirebaseTokenRequest(user.condominiumId, user.id, ""))
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {

                    }
                }, {
                })

        compositeDisposable.add(disposable)
    }

    private fun loggoutAndClearCache(): Boolean {
        return try {
            AppDatabase.instance(context)?.userDao()?.clear()
            FirebaseAuth.getInstance().signOut()
            true
        } catch (e: Exception) {
            false
        }
    }
}