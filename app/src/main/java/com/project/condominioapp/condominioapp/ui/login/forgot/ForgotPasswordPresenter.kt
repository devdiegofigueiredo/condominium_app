package com.project.condominioapp.condominioapp.ui.login.forgot

class ForgotPasswordPresenter(val view: ForgotPasswordContract.View,
                              val interactor: ForgotPasswordContract.Interactor) :
        ForgotPasswordContract.Presenter,
        ForgotPasswordContract.Presenter.RetrievePasswordCallback {

    override fun retrievePassword(email: String) {
        view.showLoading()
        interactor.retrievePassword(email, this)
    }

    override fun onPasswordSentSuccess(message: String) {
        view.hideLoading()
        view.showMessage(message)
    }

    override fun onPasswordSentError(message: String) {
        view.hideLoading()
        view.showMessage(message)
    }
}