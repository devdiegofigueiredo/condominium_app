package com.project.condominioapp.condominioapp.ui.login

class LoginPresenter(val view: LoginContract.View,
                     val interactor: LoginContract.Interactor) :
        LoginContract.Presenter,
        LoginContract.Presenter.LoginCallback {

    override fun login(email: String, password: String, condominiumId: String) {
        interactor.doFirebaseLogin(email, password, condominiumId, this)
        view.showLoading()
    }

    override fun onLoginSuccess() {
        view.startHomeScreen()
        view.hideLoading()
    }

    override fun onLoginPending() {
        view.showPendingAcceptDialog()
        view.hideLoading()
    }

    override fun onLoginError(message: String) {
        view.hideLoading()
        view.showErrorMessage(message)
    }
}