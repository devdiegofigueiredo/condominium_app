package com.project.condominioapp.condominioapp.ui.profile.residents

import com.project.condominioapp.condominioapp.ui.profile.entities.Resident

interface ResidentsContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun showErrorMessage(message: String)
        fun setupResidents(residents: List<Resident>)
        fun changeLoadingMessageToUpdate()
        fun closeScreen()
    }

    interface Presenter {
        interface ResidentsCallback {
            fun onResidentsSuccess(message: String)
            fun onResidentsError(message: String)
        }

        interface UserCallback {
            fun onUserSuccess(residents: List<Resident>)
            fun onUserError(message: String)
        }

        fun updateResidents(residents: List<Resident>)
    }

    interface Interactor {
        fun updateResidents(residents: List<Resident>, callback: Presenter.ResidentsCallback)
        fun getCurrentUser(callback: Presenter.UserCallback)
        fun onDestroy()
    }
}