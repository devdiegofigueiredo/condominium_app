package com.project.condominioapp.condominioapp.ui.profile.edit

import android.os.Bundle
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity
import kotlinx.android.synthetic.main.change_password_activity.*

class ChangePasswordActivity : BaseActivity(), ChangePasswordContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password_activity)

        setupToolbar(getString(R.string.change_password), true)

        val interactor = ChangePasswordInteractor(this)
        val presenter = ChangePasswordPresenter(this, interactor)

        loading = ProgressDialogUtil.getDialog(getString(R.string.wait_update_password), this)

        done.setOnClickListener { presenter.changePassword(current_password, new_password, confirm_password) }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item.takeIf { it?.itemId == android.R.id.home }?.apply { finish() }
        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showErrorField(field: EditText) {
        field.error = getString(R.string.input_this_field)
        field.requestFocus()
    }

    override fun closeScreen() {
        finish()
    }

    override fun showWrongPasswordMessage() =
            Toast.makeText(this, getString(R.string.error_password_and_confirmation), Toast.LENGTH_LONG).show()

    override fun showIncorrectSizeMessage(field: EditText) {
        field.error = getString(R.string.incorrect_size_password)
        field.requestFocus()
    }
}