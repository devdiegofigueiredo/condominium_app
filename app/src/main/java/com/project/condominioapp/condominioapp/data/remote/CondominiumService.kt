package com.project.condominioapp.condominioapp.data.remote

import com.project.condominioapp.condominioapp.data.datautil.Endpoints
import com.project.condominioapp.condominioapp.model.SimpleResponse
import com.project.condominioapp.condominioapp.ui.condominium.entities.*
import com.project.condominioapp.condominioapp.ui.login.register.entities.CreateCondominiumRequest
import io.reactivex.Observable
import retrofit2.http.*

interface CondominiumService {

    @POST(Endpoints.createCondominium)
    fun createCondominium(@Body body: CreateCondominiumRequest): Observable<SimpleResponse>

    @GET(Endpoints.condominium)
    fun condominium(@Query("condominiumId") condominium: String): Observable<CondominiumResponse>

    @PUT(Endpoints.updateCondominium)
    fun updateCondominium(@Body updateCondominiumRequest: EditCondominiumRequest): Observable<CondominiumResponse>

    @PUT(Endpoints.createSpace)
    fun createSpace(@Body body: CreateSpaceRequest): Observable<CreateSpaceResponse>

    @DELETE(Endpoints.deleteSpace)
    fun deleteSpace(@Query("spaceId") spaceId: String, @Query("condominiumId") condominiumId: String): Observable<SimpleResponse>

    @PUT(Endpoints.editSpace)
    fun editSpace(@Body body: EditSpaceRequest): Observable<EditSpaceResponse>

}