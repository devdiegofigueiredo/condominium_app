package com.project.condominioapp.condominioapp.ui.approvals.claim

import com.project.condominioapp.condominioapp.ui.claims.entities.Claim

interface ReplyClaimContract {

    interface View {
        fun addClaims(claims: List<Claim>)
        fun removeClaim(claimId: String)
        fun showLayoutLoading()
        fun hideLayoutLoading()
        fun showErrorMessage(message: String)
        fun onEmptyClaims()
        fun createErrorView(message: String)
    }

    interface Presenter {
        interface CallbackClaims {
            fun onClaimsSuccess(claims: List<Claim>)
            fun onClaimsEmpty()
            fun onClaimsError(message: String)
        }

        interface CallbackReply {
            fun onReplySuccess(message: String, claimId: String)
            fun onReplyError(message: String)
        }

        interface CallbackReplySolved {
            fun onReplySolvedSuccess(message: String, claimId: String)
            fun onReplySolvedError(message: String)
        }

        fun claimNotAnswered()
        fun replyClaim(claimId: String, reply: String)
        fun replySolvedClaim(claimId: String)
        fun onDestroy()
    }

    interface Interactor {
        fun claimsUnreplieds(callback: Presenter.CallbackClaims)
        fun replyClaim(claimId: String, reply: String, callback: Presenter.CallbackReply)
        fun replySolvedClaim(claimId: String, callback: Presenter.CallbackReplySolved)
        fun onDestroy()
    }
}