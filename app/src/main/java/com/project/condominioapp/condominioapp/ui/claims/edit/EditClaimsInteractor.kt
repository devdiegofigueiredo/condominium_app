package com.project.condominioapp.condominioapp.ui.claims.edit

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.ui.claims.entities.EditClaimRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EditClaimsInteractor(val context: Context) : EditClaimsContract.Interactor {

    override fun editClaim(claimId: String, title: String, message: String, callback: EditClaimsContract.Presenter.EditClaimCallback) {
        val editClaimRequest = EditClaimRequest(claimId, title, message)
        val request = BaseService.editClaim(editClaimRequest)
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        takeIf { this.statusCode == 200 }?.apply {
                            callback.onEditClaimSuccess("", this.claim)
                        }
                    } ?: kotlin.run {
                        callback.onEditClaimError(context.getString(R.string.error_edit_claim), context.getString(R.string.try_again))
                    }
                }, {
                    it.message?.apply { callback.onEditClaimError(this, context.getString(R.string.try_again)) }
                })
    }
}