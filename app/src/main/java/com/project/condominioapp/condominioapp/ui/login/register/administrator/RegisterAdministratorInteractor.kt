package com.project.condominioapp.condominioapp.ui.login.register.administrator

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.ui.login.register.entities.CreateCondominiumRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RegisterAdministratorInteractor(val context: Context) : RegisterAdministratorContract.Interactor {

    val compositeDisposable = CompositeDisposable()

    override fun createCondominium(registerAdministratorRequest: CreateCondominiumRequest,
                                   callback: RegisterAdministratorContract.Presenter.CreateCondominiumCallback) {

        val callCreateUser = BaseService.createCondominium(registerAdministratorRequest)
        val disposable = callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onCreateCondominiumSuccess(context.getString(R.string.condominium_created))
                        } else {
                            callback.onCreateCondominiumError(context.getString(R.string.error_try_again))
                        }
                    }
                }, {
                    callback.onCreateCondominiumError(context.getString(R.string.error_try_again))
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}