package com.project.condominioapp.condominioapp.ui.login.forgot

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.utils.EmailUtil
import com.project.condominioapp.condominioapp.utils.ProgressDialogUtil
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.toolbar.*

class ForgotPasswordActivity : AppCompatActivity(), ForgotPasswordContract.View {

    companion object {
        const val EXTRA_EMAIL = "extra_email"
    }

    private lateinit var toolbar: Toolbar
    private lateinit var presenter: ForgotPasswordPresenter
    private lateinit var email: EditText
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        toolbar = generic_toolbar
        toolbar.title = getString(R.string.forgot_password)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        progressDialog = ProgressDialogUtil.getDialog(this)

        email = et_email

        val bundle = intent.extras
        bundle?.getString(EXTRA_EMAIL)?.apply {
            email.setText(this)
        }

        val interactor = ForgotPasswordInteractor(this)
        presenter = ForgotPasswordPresenter(this, interactor)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_done, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }

            R.id.action_done -> {
                if (EmailUtil.isValidEmail(email.text.toString())) {
                    presenter.retrievePassword(email.text.toString())
                } else {
                    email.error = getString(R.string.input_valid_email)
                    email.requestFocus()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progressDialog.show()
    }

    override fun hideLoading() {
        progressDialog.dismiss()
    }
}