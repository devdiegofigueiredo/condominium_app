package com.project.condominioapp.condominioapp.ui.releases

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.releases.entities.Release

class ReleasesAdapter(private val releases: List<Release>) : androidx.recyclerview.widget.RecyclerView.Adapter<ReleasesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.releases_item, parent, false))

    override fun getItemCount(): Int = releases.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = releases[position].title
        holder.releaseds.text = releases[position].names
        holder.limitDate.text = "Data para liberação: " + releases[position].date
    }

    class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.title)
        val releaseds: TextView = itemView.findViewById(R.id.releaseds)
        val limitDate: TextView = itemView.findViewById(R.id.limit_date)
    }
}