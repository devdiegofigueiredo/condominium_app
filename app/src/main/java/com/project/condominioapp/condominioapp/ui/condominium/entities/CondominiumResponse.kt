package com.project.condominioapp.condominioapp.ui.condominium.entities

data class CondominiumResponse(val statusCode: Int, val condominium: Condominium, val error: String)