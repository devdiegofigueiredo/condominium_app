package com.project.condominioapp.condominioapp.ui.condominium

import com.project.condominioapp.condominioapp.ui.condominium.entities.Condominium

class CondominiumPresenter(val view: CondominiumContract.View,
                           val interactor: CondominiumContract.Interactor) :
        CondominiumContract.Presenter,
        CondominiumContract.Presenter.CondominiumCallback {

    override fun onCondominiumSuccess(condominium: Condominium) {
        view.setupCondominium(condominium)
        view.hideLoading()
    }

    override fun onCondominiumError(message: String) {
        view.showErrorMessage(message)
        view.hideLoading()
    }

    override fun condominium() {
        view.showLoading()
        interactor.condominium(this)
    }

    override fun onDestroy() {
        interactor.onDestroy()
    }

}