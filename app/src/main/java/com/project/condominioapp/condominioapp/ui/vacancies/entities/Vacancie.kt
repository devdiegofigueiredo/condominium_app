package com.project.condominioapp.condominioapp.ui.vacancies.entities

import com.google.gson.annotations.SerializedName
import com.project.condominioapp.condominioapp.model.User

data class Vacancie(@SerializedName("_id") val id: String,
                    val user: User,
                    val model: String,
                    val vacancieNumber: String,
                    val plate: String,
                    val apartmentNumber: String,
                    val apartmentBlock: String)