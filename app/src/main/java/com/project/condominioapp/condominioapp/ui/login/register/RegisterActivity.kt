package com.project.condominioapp.condominioapp.ui.login.register

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.tabs.TabLayout
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.ui.login.register.administrator.RegisterAdministratorFragment
import com.project.condominioapp.condominioapp.ui.login.register.resident.RegisterFragment
import com.project.condominioapp.condominioapp.utils.ui.BaseActivity

class RegisterActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register2)

        setupToolbar(getString(R.string.register), true)
        setupViewPager()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupViewPager() {
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: androidx.viewpager.widget.ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }

    private class SectionsPagerAdapter(private val context: Context, fm: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fm) {

        private val titles = arrayOf(R.string.resident, R.string.administrator)

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            when (position) {
                0 -> {
                    return RegisterFragment.newInstance()
                }

                1 -> {
                    return RegisterAdministratorFragment.newInstance()
                }
            }

            return RegisterFragment.newInstance()
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return context.resources.getString(titles[position])
        }

        override fun getCount(): Int {
            return titles.size
        }
    }
}