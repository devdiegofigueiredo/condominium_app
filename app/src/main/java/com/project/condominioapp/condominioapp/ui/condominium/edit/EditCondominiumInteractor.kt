package com.project.condominioapp.condominioapp.ui.condominium.edit

import android.content.Context
import com.project.condominioapp.condominioapp.R
import com.project.condominioapp.condominioapp.data.local.repository.CurrentUser
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.condominium.entities.EditCondominiumRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class EditCondominiumInteractor(val context: Context) : EditCondominiumContract.Interactor, CurrentUser.UserCallback {

    private val compositeDisposable = CompositeDisposable()
    private lateinit var user: User

    init {
        CurrentUser.user(this, context)
    }

    override fun onUserSuccess(user: User) {
        this.user = user
    }

    override fun editCondominium(editCondominium: EditCondominiumRequest, callback: EditCondominiumContract.Presenter.EditCondominiumCallback) {
        val request = BaseService.updateCondominium(editCondominium)
        val disposable = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.statusCode == 200) {
                            callback.onEditCondominiumSuccess(context.getString(R.string.condominium_updated_successfully), this.condominium)
                        } else {
                            callback.onEditCondominiumError(context.getString(R.string.error_update_condominium))
                        }
                    }
                }, {
                    it.message?.apply { callback.onEditCondominiumError(this) }
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}