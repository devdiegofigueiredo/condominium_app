package com.project.condominioapp.condominioapp.ui.claims.edit

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.claims.entities.Claim

class EditClaimsPresenter(val view: EditClaimsContract.View,
                          val interactor: EditClaimsContract.Interactor) :
        EditClaimsContract.Presenter,
        EditClaimsContract.Presenter.EditClaimCallback {

    override fun editClaim(claimId: String, title: EditText, message: EditText) {
        takeIf { isFieldsValidateds(title, message) }.apply {
            view.showLoading()
            interactor.editClaim(claimId, title.text.toString(), message.text.toString(), this@EditClaimsPresenter)
        }
    }

    override fun onEditClaimSuccess(message: String, claim: Claim) {
        view.closeScreen(claim)
        view.hideLoading()
    }

    override fun onEditClaimError(message: String, textButton: String) {
        view.hideLoading()
        view.showMessage(message)
    }


    private fun isFieldsValidateds(title: EditText, message: EditText): Boolean {
        title.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(title)
            return false
        }

        message.takeIf { it.text.isEmpty() }?.apply {
            view.showErrorField(message)
            return false
        }

        return true
    }
}