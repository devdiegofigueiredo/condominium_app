package com.project.condominioapp.condominioapp.data.local.repository

import android.content.Context
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.model.User
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CurrentUser {

    interface UserCallback {
        fun onUserSuccess(user: User)
    }

    companion object {

        private val compositeDisposable = CompositeDisposable()

        fun user(callback: UserCallback, context: Context) {
            val disposable = Single.fromCallable { getCurrentUser(context) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        it?.apply {
                            callback.onUserSuccess(it)
                        }
                    }, {

                    })

            compositeDisposable.add(disposable)
        }

        fun onDestroy() {
            compositeDisposable.dispose()
        }

        private fun getCurrentUser(context: Context): User? {
            try {
                val user = AppDatabase.instance(context)?.userDao()?.getUser()
                if (user != null) {
                    return user
                }
            } catch (e: Exception) {
            }
            return null
        }
    }
}