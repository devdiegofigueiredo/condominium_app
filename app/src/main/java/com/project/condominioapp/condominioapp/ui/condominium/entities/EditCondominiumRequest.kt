package com.project.condominioapp.condominioapp.ui.condominium.entities

data class EditCondominiumRequest(val name: String,
                                  val phone: String,
                                  val street: String,
                                  val city: String,
                                  val state: String,
                                  val postalCode: String,
                                  val condominiumId: String)