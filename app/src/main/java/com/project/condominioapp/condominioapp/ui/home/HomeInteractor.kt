package com.project.condominioapp.condominioapp.ui.home

import android.content.Context
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.project.condominioapp.condominioapp.data.local.dao.AppDatabase
import com.project.condominioapp.condominioapp.data.remote.BaseService
import com.project.condominioapp.condominioapp.model.User
import com.project.condominioapp.condominioapp.ui.home.entities.UpdateFirebaseTokenRequest
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class HomeInteractor(val context: Context) : HomeContract.Interactor {

    private val compositeDisposable = CompositeDisposable()

    override fun currentUser(callback: HomeContract.Presenter.UserCallback) {
        val disposable = Single.fromCallable { getCurrentUser() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.apply {
                        callback.onUserSuccess(it)
                        updateFirebaseToken(it)
                    }
                }, {
                })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun getCurrentUser(): User? {
        try {
            val user = AppDatabase.instance(context)?.userDao()?.getUser()
            if (user != null) {
                return user
            }
        } catch (e: Exception) {
        }
        return null
    }

    private fun updateFirebaseToken(user: User) {
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        return@OnCompleteListener
                    }

                    val token = task.result!!.token

                    val updateUserRequest = UpdateFirebaseTokenRequest(user.condominiumId, user.id, token)
                    val request = BaseService.updateFirebaseToken(updateUserRequest)
                    request.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ response ->
                                Log.v("FirebaseToken Success", response.statusCode.toString())
                            }, {
                                Log.v("FirebaseToken Error", it.message)
                            })
                })
    }
}