package com.project.condominioapp.condominioapp.ui.vacancies.new

import android.widget.EditText
import com.project.condominioapp.condominioapp.ui.vacancies.entities.Vacancie

interface NewVacancieContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun showErrorField(field: EditText)
        fun showMessage(message: String)
        fun closeScreen(vacancie: Vacancie, currentUserId: String)
    }

    interface Presenter {
        interface CreateVacancieCallback {
            fun onCreateVacancieSuccess(message: String, vacancie: Vacancie, currentUserId: String)
            fun onCreateVacancieError(message: String)
        }

        fun createVacancie(model: EditText, plate: EditText, vacancieNumber: EditText)
        fun onDestroy()
    }

    interface Interactor {
        fun createVacancie(model: String,
                           plate: String,
                           vacancieNumber: String,
                           callback: Presenter.CreateVacancieCallback)
        fun onDestroy()
    }

}